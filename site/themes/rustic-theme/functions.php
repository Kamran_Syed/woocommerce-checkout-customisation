<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN CTC ENQUEUE PLUGIN ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below
if ( !function_exists( 'chld_thm_cfg_plugin_css' ) ):
    function chld_thm_cfg_plugin_css() {
        wp_enqueue_style( 'chld_thm_cfg_plugins', trailingslashit( get_stylesheet_directory_uri() ) . 'ctc-plugins.css' );
    }
endif;
// using high priority so plugin custom styles load last. 
add_action( 'wp_print_styles', 'chld_thm_cfg_plugin_css', 9999 );

// END CTC ENQUEUE PLUGIN ACTION

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

        
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css' );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );

// END ENQUEUE PARENT ACTION

add_filter('woocommerce_add_to_cart_redirect', 'add_to_cart_redirect');
function add_to_cart_redirect() {
	global $woocommerce;
	$checkout_url = $woocommerce->cart->get_checkout_url();
	return $checkout_url;
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'cart_button_text' ); 
function cart_button_text() {
	return __( 'Order Now', 'woocommerce' );
}

add_action('wp_enqueue_scripts', 'child_styles', PHP_INT_MAX);
function child_styles()
{
    wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri().'/js/bootstrap.min.js', array('jquery'), NULL, true);
    wp_enqueue_script('jquery-ui-datepicker', false, array('jquery-ui-core','jquery'));
    wp_enqueue_style('bootstrap-css', get_stylesheet_directory_uri().'/css/bootstrap.min.css', false, NULL, 'all');
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    $fields['shipping']['personal_message'] = array(
        //'label'     => __('Personal Message', 'woocommerce'),
		'type'          => 'textarea',
		'placeholder'   => _x('Write a message here for the card to send with your posy', 'placeholder', 'woocommerce'),
		'required'  => false,
		'class'     => array('form-row-wide'),
		'clear'     => true
     );

    return $fields;
}

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
	
    if ( ! empty( $_POST['personal_message'] ) ) {
        update_post_meta( $order_id, '_shipping_personal_message', $_POST['personal_message'] );
    }
	
	if(isset($_POST['ddate'])){
		$mydate = current_time('d-m-Y');
		if($_POST['ddate'] == 'later') $mydate = $_POST['fddate'];
		
		$d = New DateTime($mydate);
		if($d->format('D') == 'Sat' || $d->format('D') == 'Sun'){
			throw new Exception('Sorry, we are closed on week ends');  
		}
		
		update_post_meta( $order_id, '_required_delivery_date', $mydate );
		
	}
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Message Card').':</strong></p><div style="background: #EFEFEF;padding:1em;"><textarea style="width:100%;" name="mymsg" >' . get_post_meta( $order->id, '_shipping_personal_message', true ) . '</textarea></div>';
	
	echo '<p><strong>'.__('Delivery Date').':</strong></p><div style="background: #EFEFEF;padding:1em;"><input type="text" name="ddate" placeholder="dd-mm-yyyy" value="' . get_post_meta( $order->id, '_required_delivery_date', true ) . '" /></div>';
}

function save_order_meta( $post_id, $post, $update ) {

    // If this isn't a 'shop_order' post, don't update it.
    if ( 'shop_order' != $post->post_type ) {
        return;
    }

    // - Update the post's metadata.

    if ( isset( $_REQUEST['mymsg'] ) ) {
        update_post_meta( $post_id, '_shipping_personal_message', $_REQUEST['mymsg']);
    }

    if ( isset( $_REQUEST['ddate'] ) ) {
        update_post_meta( $post_id, '_required_delivery_date', sanitize_text_field( $_REQUEST['ddate'] ) );
    }
}

add_action( 'save_post', 'save_order_meta', 10, 3 );


add_action( 'wp_ajax_aspk_add_to_cart', 'aspk_add_to_cart' );
add_action( 'wp_ajax_nopriv_aspk_add_to_cart', 'aspk_add_to_cart' );

function aspk_add_to_cart(){
	$pid = $_POST['pid'];
	if(empty($pid)) exit;
	
	WC()->cart->add_to_cart($pid);
	exit;
}

add_action( 'wp_ajax_aspk_remove_from_cart', 'aspk_remove_from_cart' );
add_action( 'wp_ajax_nopriv_aspk_remove_from_cart', 'aspk_remove_from_cart' );

function aspk_remove_from_cart(){
	$pid = $_POST['pid'];
	if(empty($pid)) exit;
	
	$cart = WC()->instance()->cart;
	$cart_id = $cart->generate_cart_id($pid);
	$cart_item_id = $cart->find_product_in_cart($cart_id);

	if($cart_item_id){
	   $cart->set_quantity($cart_item_id,0);
	}
	exit;
}

add_action( 'wp_ajax_aspk_view_order', 'aspk_view_order' );
add_action( 'wp_ajax_nopriv_aspk_view_order', 'aspk_view_order' );

function aspk_view_order(){
	if ( ! defined('WOOCOMMERCE_CART') ) {
		define( 'WOOCOMMERCE_CART', true );
	}
	WC()->instance()->cart->calculate_totals(); 
	ob_start();
	woocommerce_order_review();
	echo '<div style="display:none"  class="aspk_review_order_stub"></div>';
	?>
	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>
	<script>
		jQuery('.shipping').hide();
		jQuery('#payment').hide();
	</script>
	<?php
	echo ob_get_clean();
	exit;
}

add_action('wp_head','aspk_javascript');

function aspk_javascript() {
	?>
		<script>var ajaxurl = "<?php echo site_url();?>/wp-admin/admin-ajax.php"; </script>
		
		<!-- Facebook Pixel Code --> 
		<script> 
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n; 
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0; 
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, 
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1091220927636287'); 
		fbq('track', "PageView");</script> 
		<noscript><img height="1" width="1" style="display:none" 
		src="https://www.facebook.com/tr?id=1091220927636287&ev=PageView&noscript=1" 
		/></noscript> 
		<!-- End Facebook Pixel Code --> 
	<?php
}

add_action( 'wp_footer', 'aspk_footer', 99 );

function aspk_footer(){
	if(is_page('home')){
		$pids = get_out_of_stock_products();
		
		?>
		<script>
			jQuery( window ).load(function() {
				var dd_day = new Date();
				if(dd_day.getDay() == 0 || dd_day.getDay() == 6){
					jQuery('.rev-btn2').text('PRE-ORDER POSY');
					jQuery('#menu-item-1030 a').text('PRE-ORDER POSY');
				}else{
					var aspkzero = <?php echo json_encode($pids);?>;
					var aspkactions = jQuery('.rev-btn2').attr("href");
					var aspkidx = aspkactions.indexOf('add-to-cart=');
					var aspktgtstr = aspkactions.substring(aspkidx);
					var aspktgtstr2 = aspktgtstr.replace("add-to-cart=", "");
					var aspktgtstr3 = aspktgtstr2.replace("\"}]", "");

					if(aspkzero.indexOf(aspktgtstr3) > -1){
						jQuery('.rev-btn2').text('PRE-ORDER POSY');
						jQuery('#menu-item-1030 a').text('PRE-ORDER POSY');
					}
				}
			});
			
			jQuery( document ).ready(function() {
				jQuery('.nicdark_vc .vc_column_container').eq(1).find('.wpb_wrapper .vc_empty_space').eq(0).css('height','80px');
				jQuery('.nicdark_vc .vc_column_container').eq(2).find('.wpb_wrapper .vc_empty_space').eq(0).css('height','45px');
			});
		</script>
		<?php
	}
}

function get_out_of_stock_products(){
	global $wpdb;
	
	if (! class_exists( 'WooCommerce' ) ){
		return array();
	}
	
	$sql = "SELECT p.ID
			FROM {$wpdb->prefix}posts AS p, {$wpdb->prefix}postmeta AS m
			WHERE p.post_status =  'publish'
			AND (
			p.post_type =  'product'
			OR p.post_type =  'product_variation'
			)
			AND m.post_id = p.ID
			AND m.meta_key =  '_stock'
			AND m.meta_value < 1";
	
	return $wpdb->get_col($sql);
}

add_action( 'woocommerce_product_set_stock', 'aspk_product_set_stock');

function aspk_product_set_stock($p){
	global $woocommerce;
	
	$synced_products = array('POSY','POSYA','POSYD'); //product for which to sync inventory
	$sku = $p->get_sku();
	
	if(! in_array($sku, $synced_products)) return false;
	
	$qty = $p->get_stock_quantity();
	if($qty ==0 ) return;
	
	if($qty < 0){
		$qty = 0;
		wc_update_product_stock( $p->id, $qty );
	}
	
	foreach($synced_products as $sp){
		if($sp == $sku) continue;
		
		$pid = wc_get_product_id_by_sku( $sp );
		wc_update_product_stock( $pid, $qty );
		
	}
	
}

add_filter( 'manage_edit-shop_order_columns', 'woo_order_ddate_column' );

function woo_order_ddate_column( $columns ) {
	
	$newcols = array();
	
	foreach($columns as $k=>$v){
		$newcols[$k] = $v;
		
		if($k == 'order_date'){
			$newcols['ddate'] = __( 'Delivery Date', 'woocommerce' );
		}
	}
	
	return $newcols;
}

add_action( 'manage_shop_order_posts_custom_column', 'woo_custom_order_ddate_column');

function woo_custom_order_ddate_column( $column ) {
	global $post, $woocommerce, $the_order;
	
	if ( empty( $the_order ) || $the_order->id != $post->ID )
		$the_order = new WC_Order( $post->ID );
	
	if ( $column == 'ddate' ) {
		$mydate = get_post_meta( $the_order->id, '_required_delivery_date', true );
		
		if($mydate == current_time('d-m-Y')){
			echo '<span style="color:red">'.$mydate.'</span>';
		}else{
			echo $mydate;
		}
		 
	}
}

add_action( 'woocommerce_thankyou', 'aspk_woocommerce_thankyou');

function aspk_woocommerce_thankyou($order_id){
	?>
		<!-- Google Code for Purchases Conversion Page -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 920156556;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "NyCACKLo0GYQjPPhtgM";
		var google_conversion_value = 35.00;
		var google_conversion_currency = "AUD";
		var google_remarketing_only = false;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/920156556/?value=35.00&amp;currency_code=AUD&amp;label=NyCACKLo0GYQjPPhtgM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-76477007-1', 'auto');
		  ga('send', 'pageview');

		</script>
		
	<?php
		$order = new WC_Order($order_id);
		$revenue = $order->get_total();
		$tax = $order->get_total_tax();
		?>
		<script>
			ga('create', 'UA-76477007-1', 'auto');
			ga('require', 'ecommerce');
			ga('ecommerce:addTransaction', {
			  'id': '<?php echo $order_id;?>',
			  'revenue': '<?php echo $revenue;?>',
			  'shipping': 'Free',
			  'tax': '<?php echo $tax;?>'
			});
		
			<?php
			$items = $order->get_items();
			
			foreach($items as $item){
				?>
					ga('ecommerce:addItem', {
					  'id': '<?php echo $order_id;?>',
					  'name': '<?php echo $item[name];?>',
					  'price': '<?php echo round($item[line_total]/$item[qty] , 2);?>',
					  'quantity': '<?php echo $item[qty];?>'
					});
				<?php
			}
			?>
			ga('ecommerce:send');
			ga('ecommerce:clear');
		</script>
		<?php
		
}

add_action( 'woocommerce_review_order_after_order_total', 'aspk_woocommerce_review_order_after_order_total');

function aspk_woocommerce_review_order_after_order_total(){
	?>
		<tr><td></td><td id="tdmymsg">If you experience any issues making payment please contact Jenelle on <b>0419 199 107</b> to confirm your order.</td></tr>
		<script>
		jQuery( document ).ready(function() {
				jQuery('#tdmymsg').css('width','50%');
				jQuery('#tdmymsg').css('font-style','italic');
			});
		</script>
	<?php
}
