<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/*
    Plugin Name: Child Theme Configurator PRO
    Plugin URI: http://www.childthemeconfigurator.com/child-theme-configurator-pro/
    Description: Adds plugin stylesheets and other additional functionality to Child Theme Configurator
    Version: 2.1.2
    Author: Lilaea Media
    Author URI: http://www.lilaeamedia.com/
    Text Domain: chld_thm_cfg_plugins
    Domain Path: /lang
  
    This file and all accompanying files (C) 2014-2015 Lilaea Media LLC except where noted. See license for details.
*/

    defined( 'LF' ) or define( 'LF', "\n" );
    define( 'CHLD_THM_CFG_PLUGINS_VERSION', '2.1.2' );
    define( 'CHLD_THM_CFG_MIN_VERSION', '1.7.9' );
    define( 'CHLD_THM_CFG_PLUGINS_MENU', 'ctc-plugins' );
    define( 'CHLD_THM_CFG_PLUGINS_DIR', dirname( __FILE__ ) );
    define( 'CHLD_THM_CFG_PLUGINS_URL', plugin_dir_url( __FILE__ ) );
    define( 'CHLD_THM_CFG_PLUGINS_FILE', __FILE__ );
    
        
    if ( is_admin() ):
        // only initialize CTCP if in admin
        include_once( CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-plugins.php' );
    else:
        if ( isset( $_GET[ 'preview_ctc' ] ) && isset( $_GET[ 'preview_iframe' ] ) ):
            include_once( CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-preview.php' );
        endif;
    endif;
    
    // clean out options with uninstall
    register_uninstall_hook( __FILE__, 'chld_thm_cfg_plugins_uninstall' );
    
    function chld_thm_cfg_plugins_uninstall() {
        delete_option( 'chld_thm_cfg_options' );
        delete_option( 'chld_thm_cfg_options_plugins_configvars' );
        delete_option( 'chld_thm_cfg_options_plugins_dict_qs' );
        delete_option( 'chld_thm_cfg_options_plugins_dict_sel' );
        delete_option( 'chld_thm_cfg_options_plugins_dict_query' );
        delete_option( 'chld_thm_cfg_options_plugins_dict_rule' );
        delete_option( 'chld_thm_cfg_options_plugins_dict_val' );
        delete_option( 'chld_thm_cfg_options_plugins_dict_seq' );
        delete_option( 'chld_thm_cfg_options_plugins_sel_ndx' );
        delete_option( 'chld_thm_cfg_options_plugins_val_ndx' );
    }

    