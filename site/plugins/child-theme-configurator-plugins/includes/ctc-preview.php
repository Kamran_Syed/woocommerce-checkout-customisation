<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

    
    class ChildThemeConfiguratorProPreview {
        /**
         * Replaces core callback function for ob_start() to capture all links in the theme.
         */
        static function preview_filter( $content ) {
            
            return preg_replace_callback( "|(<a.*?href=([\"']))(.*?)([\"'].*?>)|", 
                'ChildThemeConfiguratorProPreview::preview_filter_callback', $content );
        }
        
        /**
         * Replaces core function to manipulate preview theme links in order to control and maintain location.
         */
        static function preview_filter_callback( $matches ) {
            // add preview parameters to all hrefs 
            if ( strpos( $matches[ 4 ], 'onclick' ) !== false )
                $matches[ 4 ] = preg_replace( '#onclick=([\'"]).*?(?<!\\\)\\1#i', '', $matches[ 4 ]);
            if ( ( false !== strpos( $matches[ 3 ], '/wp-admin/' ) )
                || ( false !== strpos( $matches[ 3 ], '://' ) && 0 !== strpos( $matches[ 3 ], home_url() ) )
                || ( false !== strpos( $matches[ 3 ], '/feed/') )
                || ( false !== strpos( $matches[ 3 ], '/trackback/' ) ) )
                return $matches[ 1 ] . "#$matches[2] onclick=$matches[2]return false;" . $matches[ 4 ];
        
            $stylesheet = isset( $_GET[ 'stylesheet' ] ) ? $_GET[ 'stylesheet' ] : '';
            $template   = isset( $_GET[ 'template' ] )   ? $_GET[ 'template' ]   : '';
            $nonce      = isset( $_GET[ 'preview_ctc' ] ) ? $_GET[ 'preview_ctc' ] : '';
            $link = add_query_arg( array( 
                'preview_ctc'       => $nonce,
                'template'          => $template, 
                'stylesheet'        => $stylesheet, 
                'preview_iframe'    => 1, 
                ), $matches[ 3 ] );
            if ( 0 === strpos( $link, 'preview_ctc' ) )
                $link = "?$link";
            return $matches[ 1 ] . esc_attr( $link ) . $matches[ 4 ];
        }
        
        // save menu arguments for later
        static function cache_menu_args( $args ) {
            if ( isset( $args[ 'theme_location' ] ) 
                && $args[ 'theme_location' ] ):
                $transient_key = 'ctcp_nav_menu_' . get_template() . '_' . $args[ 'theme_location' ];
                if ( ( $menu_args = get_transient( $transient_key ) ) && $menu_args == $args ):
                else:
                    set_transient( $transient_key, $args, 60 * 60 * 24 );
                endif;
            endif;
            return $args;
        }
        
        static function maybe_cache_menus() {
            // cache registered nav menus and their parameters
            if ( current_user_can( 'install_themes' ) ):
                add_filter( 'wp_nav_menu_args', 
                    'ChildThemeConfiguratorProPreview::cache_menu_args' );
                self::cache_menu_locations();
            endif;
        }
        
        /**
         * Stores registered nav menus in transient for use with all styles tab
         */
        static function cache_menu_locations() {
            $transient_key = 'ctcp_nav_menus_' . get_template();
            if ( $locations = get_transient( $transient_key ) ):
            else:
                $locations = get_registered_nav_menus();
                set_transient( $transient_key, $locations, 60 * 60 * 24 );
            endif;
        }
    }
    add_action ( 'init', 'ChildThemeConfiguratorProPreview::maybe_cache_menus' );
    // start output buffer with preview filter
    ob_start( 'ChildThemeConfiguratorProPreview::preview_filter' );
        
    