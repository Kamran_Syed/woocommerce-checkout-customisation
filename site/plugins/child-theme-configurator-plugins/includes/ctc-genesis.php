<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* 
 * This file and all accompanying files (C) 2014-2015 Lilaea Media LLC except where noted. See license for details.
 */
include_once CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-framework.php';

class ChildThemeConfiguratorGenesis extends ChildThemeConfiguratorFramework {
    
    function __construct() {
        $this->stylesheet = 'ctc-genesis.css';
        add_filter( 'chld_thm_cfg_enqueue_code_filter', array( $this, 'override_actions') );
        add_filter( 'chld_thm_cfg_parent_preview_args', array( $this, 'parent_preview_args' ) );
        add_action( 'chld_thm_cfg_enqueue_options',     array( $this, 'override_option' ) );
        add_action( 'chld_thm_cfg_stylesheet_handling', array( $this, 'stylesheet_handling' ) );
        add_action( 'chld_thm_cfg_existing_theme',      array( $this, 'move_original_stylesheet' ) );
    }
    
    // helper function to deref CTC class instance
    function ctc() {
        return ChildThemeConfigurator::ctc();
    }
        
    function override_option() {
?>      <div class="ctc-input-cell clear">&nbsp;</div>
      <div class="ctc-input-cell">
        <label>
          <input class="ctc_radio ctc-themeonly" id="ctc_parent_enqueue_genesis" name="ctc_parent_enqueue" type="radio" 
                value="genesis" <?php checked( 'genesis', $this->ctc()->css->enqueue ); ?>  />
          <?php _e( 'Genesis child theme handling', 'chld_thm_cfg' ); ?>
        </label>
        </strong> </div>
      <div class="ctc-input-cell howto sep"><?php _e( "This option enqueues a copy of the Genesis Child Theme stylesheet as if it were the parent and uses the custom style.css as the child stylesheet.", 'chld_thm_cfg' ); ?>
</div><?
    }
    
    function override_actions( $code ) {
        $code .= "
if ( !function_exists( 'chld_thm_cfg_genesis_meta' ) ):
    function chld_thm_cfg_genesis_meta() {
        if ( function_exists( 'genesis_enqueue_main_stylesheet' ) && ( \$priority = has_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet' ) ) ):
    	    remove_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', \$priority );
";
        if ( 'genesis' == $this->ctc()->css->enqueue ):
            $code .= "
            add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_genesis_child_parent' );
";
        else:
            $code .= "
            add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'  ) );
            add_theme_support( 'genesis-responsive-viewport' );
            add_theme_support( 'genesis-footer-widgets', 3 );
";
        endif;
        if ( in_array( $this->ctc()->css->enqueue, array( 'genesis', 'enqueue', 'none', 'import' ) ) )
            $code .= "
            add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet' );
        else:
            add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_child_css', 999 );
";
        $code .= "
        endif;
    }
endif;
add_action( 'genesis_meta', 'chld_thm_cfg_genesis_meta', 999 );
";
        if ( 'genesis' == $this->ctc()->css->enqueue )
            $code .= "
if ( !function_exists( 'chld_thm_cfg_genesis_child_parent' ) ):
    function chld_thm_cfg_genesis_child_parent() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_stylesheet_directory_uri() ) . 'ctc-genesis.css' ); 
    }
endif;
";
        if ( in_array( $this->ctc()->css->enqueue, array( 'genesis', 'enqueue', 'none', 'import' ) ) )
            $code .= "
if ( !function_exists( 'chld_thm_cfg_child_css' ) ):
    function chld_thm_cfg_child_css() {
        wp_enqueue_style( 'chld_thm_cfg_child', get_stylesheet_uri() ); 
    }
endif;
";  
        return $code;
    }
    
    function stylesheet_handling() {
        if ( 'genesis' == $this->ctc()->css->enqueue ):
            remove_action( 'chld_thm_cfg_parse_stylesheets', array( $this->ctc(), 'parse_parent_stylesheet' ) );
            add_action( 'chld_thm_cfg_parse_stylesheets', array( $this, 'parse_stylesheet' ) );
        endif;
    }
        
    function parent_preview_args( $args ) {
        if ( 'genesis' == $this->ctc()->css->enqueue )
            $args = array( 'child', $this->stylesheet );
        return $args;
    }

    // move original stylesheet to act as parent stylesheet if it does not already exist
    function move_original_stylesheet() {
        $origcss = $this->ctc()->css->get_child_target();
        if ( !file_exists( $origcss ) ) return;
        $regex = '#(Theme Name|Template):\s*(.+?)\n#i';
        $contents = preg_replace( $regex, '', @file_get_contents( $origcss ) );
        // if write is successful, original to be replaced by CTC
        if ( FALSE !== $this->ctc()->write_child_file( $this->stylesheet, $contents ) )
            $this->ctc()->delete_child_file( 'style', 'css' );
    }
}