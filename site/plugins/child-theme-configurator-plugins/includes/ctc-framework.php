<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* 
 * This file and all accompanying files (C) 2014-2015 Lilaea Media LLC except where noted. See license for details.
 */
class ChildThemeConfiguratorFramework {
    
    var $stylesheet;

    // helper function to deref CTC class instance
    function ctc() {
        return ChildThemeConfigurator::ctc();
    }
        
    // parse original stylesheet into config data as parent values
    function parse_stylesheet() {
        $this->ctc()->css->parse_css_file( 'child', $this->stylesheet, 'parnt' );
    }
        
}