<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

    
    class ChildThemeConfiguratorPlugins {
        
        static function ctcp() {
            global $chld_thm_cfg_plugins;
            if ( !isset( $chld_thm_cfg_plugins ) ):
                include_once( CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-pro.php' );
                $chld_thm_cfg_plugins = new ChildThemeConfiguratorPro( dirname( __FILE__ ) );
            endif;
            return $chld_thm_cfg_plugins;
        }
        
        static function init() {
	    	load_plugin_textdomain( 'chld_thm_cfg', FALSE, basename( CHLD_THM_CFG_PLUGINS_DIR ) . '/lang' );
            if ( !defined( 'CHLD_THM_CFG_VERSION' ) || version_compare( CHLD_THM_CFG_VERSION, CHLD_THM_CFG_MIN_VERSION, '<' ) ):
                if ( is_multisite() )
                    add_action( 'network_admin_notices', 'ChildThemeConfiguratorPlugins::install_warning' );
                else
                    add_action( 'admin_notices', 'ChildThemeConfiguratorPlugins::install_warning' );
                return FALSE; 
            endif;
            // clean up incorrect hooks from < 2.1.2
    		wp_clear_scheduled_hook( 'check_plugin_updates-ctc-plugins' );
    		delete_option( 'external_updates-ctc-plugins' );
            if ( ( $ukey = trim( self::ctcp()->options[ 'update_key' ] ) ) && !empty( $ukey ) && strlen( $ukey ) > 10 ):
                include_once( CHLD_THM_CFG_PLUGINS_DIR . '/includes/puc/plugin-update-checker.php' );
                new PluginUpdateChecker(
                    'http://www.lilaeamedia.com/updates/update.php?product=child-theme-configurator-plugins&key=' . $ukey,
                    CHLD_THM_CFG_PLUGINS_FILE
                );
            endif;
            // setup admin hooks
            add_action( 'wp_ajax_ctc_plugin',   'ChildThemeConfiguratorPlugins::save' );
            add_action( 'wp_ajax_ctc_plgqry',   'ChildThemeConfiguratorPlugins::query' );
            if ( is_multisite() )
                add_action( 'network_admin_menu',   'ChildThemeConfiguratorPlugins::network_admin' );
            else
                add_action( 'admin_menu',           'ChildThemeConfiguratorPlugins::admin' );
            // legacy plugin hook runs until defs option is removed
            add_action('wp_print_styles',       'ChildThemeConfiguratorPlugins::cfg_enqueue_styles', 999);
        }

        static function install_warning() {
?>
<div class="error">
  <p>
<?php _e( 'Child Theme Configurator PRO requires Child Theme Configurator ' . CHLD_THM_CFG_MIN_VERSION . ' or later. You can search and install it from Plugins > Add New or download it <a href="http://wordpress.org/plugins/child-theme-configurator/" target="_blank">here</a>.', 'chld_thm_cfg_plugins' ); ?>
  </p>
</div>
<?php
        }
        
        static function save() {
            ChildThemeConfigurator::ctc()->ajax_save_postdata( 'ctc_plugin' );
        }
        
        static function query() {
            ChildThemeConfigurator::ctc()->ajax_query_css( 'ctc_plugin' );
        }
        
        static function network_admin() {
            $hook = add_theme_page( 
                __( 'Child Theme Plugin Configurator', 'chld_thm_cfg_plugins' ), 
                __( 'Plugin Styles', 'chld_thm_cfg_plugins' ), 
                'install_themes', 
                CHLD_THM_CFG_PLUGINS_MENU, 
                'ChildThemeConfigurator::render' 
            );
            add_action( 'load-' . $hook, 'ChildThemeConfiguratorPlugins::page_init' );        
        }
        
        static function admin() {
            $hook = add_management_page(
                __( 'Child Theme Plugin Configurator', 'chld_thm_cfg_plugins' ), 
                __( 'Plugin Styles', 'chld_thm_cfg_plugins' ), 
                'install_themes', 
                CHLD_THM_CFG_PLUGINS_MENU, 
                'ChildThemeConfigurator::render' 
            );
            add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ChildThemeConfiguratorPlugins::action_links' );
            add_action( 'load-' . $hook, 'ChildThemeConfiguratorPlugins::page_init' );        
        }
        
        static function action_links( $actions ) {
            $actions[] = '<a href="' . admin_url( 'tools.php?page=' . CHLD_THM_CFG_PLUGINS_MENU ). '">' 
                . __( 'Plugin Styles', 'chld_thm_cfg_plugins' ) . '</a>' . LF;
            return $actions;
        }
        
        static function page_init() {
            self::ctcp()->set_pluginmode();
            ChildThemeConfigurator::page_init();
        }
        
        // legacy plugin stylesheet queue function
        static function cfg_enqueue_styles() {
            if ( $options = get_site_option( self::ctcp()->optionName ) ):
                $theme   = get_stylesheet();
                if ( isset( $options[ 'enqueues' ][ $theme ] ) ):
                    foreach( $options[ 'enqueues' ][ $theme ] as $slug => $target ):
                        wp_enqueue_style( $slug . '-ctc', get_stylesheet_directory_uri() . '/' . $target );
                    endforeach;
                endif;
            endif;
        }
        
        /**
         * Replaces core callback function for ob_start() to capture all links in the theme.
         */
        static function preview_filter( $content ) {
            return preg_replace_callback( "|(<a.*?href=([\"']))(.*?)([\"'].*?>)|", 
                'ChildThemeConfiguratorPlugins::preview_filter_callback', $content );
        }
        
        /**
         * Replaces core function to manipulate preview theme links in order to control and maintain location.
         */
        static function preview_filter_callback( $matches ) {
            // add preview parameters to all hrefs 
            if ( strpos( $matches[ 4 ], 'onclick' ) !== false )
                $matches[ 4 ] = preg_replace( '#onclick=([\'"]).*?(?<!\\\)\\1#i', '', $matches[ 4 ]);
            if ( ( false !== strpos( $matches[ 3 ], '/wp-admin/' ) )
                || ( false !== strpos( $matches[ 3 ], '://' ) && 0 !== strpos( $matches[ 3 ], home_url() ) )
                || ( false !== strpos( $matches[ 3 ], '/feed/') )
                || ( false !== strpos( $matches[ 3 ], '/trackback/' ) ) )
                return $matches[ 1 ] . "#$matches[2] onclick=$matches[2]return false;" . $matches[ 4 ];
        
            $stylesheet = isset( $_GET[ 'stylesheet' ] ) ? $_GET[ 'stylesheet' ] : '';
            $template   = isset( $_GET[ 'template' ] )   ? $_GET[ 'template' ]   : '';
        
            $link = add_query_arg( array( 
                'preview'           => 1, 
                'template'          => $template, 
                'stylesheet'        => $stylesheet, 
                'preview_iframe'    => 1, 
                'preview_ctcp'      => 1 
                ), $matches[ 3 ] );
            if ( 0 === strpos( $link, 'preview=1' ) )
                $link = "?$link";
            return $matches[ 1 ] . esc_attr( $link ) . $matches[ 4 ];
        }
        
        // save menu arguments for later
        static function cache_menu_args( $args ) {
            if ( isset( $args[ 'theme_location' ] ) 
                && $args[ 'theme_location' ] ):
                $transient_key = 'ctcp_nav_menu_' . get_template() . '_' . $args[ 'theme_location' ];
                if ( ( $menu_args = get_transient( $transient_key ) ) && $menu_args == $args ):
                else:
                    set_transient( $transient_key, $args, 60 * 60 * 24 );
                endif;
            endif;
            return $args;
        }
        
        static function maybe_cache_menus() {
            // cache registered nav menus and their parameters
            if ( current_user_can( 'install_themes' ) ):
                add_filter( 'wp_nav_menu_args', 
                    'ChildThemeConfiguratorPlugins::cache_menu_args' );
                self::cache_menu_locations();
            endif;
        }
        
        /**
         * Stores registered nav menus in transient for use with all styles tab
         */
        static function cache_menu_locations() {
            $transient_key = 'ctcp_nav_menus_' . get_template();
            if ( $locations = get_transient( $transient_key ) ):
            else:
                $locations = get_registered_nav_menus();
                set_transient( $transient_key, $locations, 60 * 60 * 24 );
            endif;
        }
    }
    
    add_action( 'plugins_loaded', 'ChildThemeConfiguratorPlugins::init' );
    