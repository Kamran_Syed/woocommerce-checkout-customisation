<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* 
 * This file and all accompanying files (C) 2014-2015 Lilaea Media LLC except where noted. See license for details.
 */
 
class ChildThemeUpgrade {
    
    var $def_map;
    var $options;
    var $plugins;
    var $optionName;
    var $addl_css;
    var $oldfiles;
    var $okfiles;
    
    function __construct() {
        $this->optionName   = 'chld_thm_cfg_plugins_options';
    }
   
    function ctcp() {
        return ChildThemeConfiguratorPlugins::ctcp();
    }
    
    function init() {
        return ( $this->options = get_site_option( $this->optionName ) 
            && isset( $this->options[ 'enqueues' ] ) 
                && count( $this->options[ 'enqueues' ] ) );
    }
    
    function build_def_index() {
        $this->def_map = array();
        if ( isset( $this->options[ 'defs' ] ) && is_array( $this->options[ 'defs' ] ) ):
            foreach ( $this->options[ 'defs' ] as $key => $def ):
                if ( 'plugin' == $def[ 'type' ] ):
                    $this->def_map[ $def[ 'slug' ] ] = $key;
                endif;
            endforeach;
        endif;
    }
    
    function migrate_plugin_stylesheets() {
        $this->build_def_index();
        $oldfiles   = array();
        $addl_css   = array();
        $regex      = '#(\@import.*?);#';
        $regex2      = '#(\@(import|charset).*?);#';
        $themeroot  = trailingslashit ( get_theme_root() );
        foreach ( $this->options[ 'enqueues' ] as $theme => $targets ):
            $styles = '';
            $imports = array();
            $hasfile = 0;
            $targetfile = $themeroot . trailingslashit( $theme ) . 'ctc-plugins.css';
            foreach( $targets as $slug => $target ):
                $oldfile = $themeroot . trailingslashit( $theme ) . $target;
                if ( !file_exists( $oldfile ) 
                    || !isset( $this->def_map[ $slug ] ) 
                        || !isset( $this->options[ 'defs' ][ $this->def_map[ $slug ] ] ) ):
                         continue;
                endif;
                $def = $this->options[ 'defs' ][ $this->def_map[ $slug ] ];
                $source = trailingslashit( $def[ 'parentdir' ] ) . $def[ 'source' ];
                $css = file_get_contents( $oldfile );
                // strip comments
                $css = preg_replace( '#\/\*.*?\*\/#s', '', $css );
                // space brace to ensure correct matching
                $css = preg_replace( '#(\{\s*)#', "$1\n", $css );
                // get all imports
                preg_match_all( $regex, $css, $matches );
                foreach ( $matches[ 1 ] as $import )
                    $imports[] = $import;
                // strip imports
                $css = preg_replace( $regex2, '', $css );
                $styles .= $css . LF;
                $this->oldfiles[] = $oldfile;
                $this->ctcp()->options[ 'addl_css' ][ $this->def_map[ $slug ] ] = 1;
                $hasfile++;
            endforeach;
            if ( $hasfile ):
                $styles = implode( ';' . LF, $imports ) . LF . $styles;
                if ( $error = $this->write_plugin_files( $targetfile, $styles ) ):
                    $this->ctcp()->errors[ $theme ][ basename( $targetfile ) ] = $error;
                else:
                    $this->okfiles[] = $oldfile;
                endif;
            endif;
        endforeach;
    }

    function write_plugin_files( $file, $contents ) {
        if ( !$this->ctcp()->ctc()->fs ):
            return 'Could not access filesystem.'; // return if no filesystem access
        endif;
        global $wp_filesystem;
        $file = $this->ctcp()->ctc()->fspath( $file );
        if ( $file ): //&& !$wp_filesystem->exists( $file ) ): // this is upgrade, existing file ok ... ?
            if ( FALSE === $wp_filesystem->put_contents( $file, $contents ) ):
                return 'Write failed.'; 
            endif;
        else:
            return 'File exists';
        endif;
        return FALSE;
    }
    
    function delete_options() {
        if ( isset( $_POST[ 'ctc_remove_stylesheets' ] ) && $this->ctcp()->ctc()->fs && count( $this->oldfiles ) ):
            global $wp_filesystem;
            foreach ( $this->oldfiles as $file ):
                $file = $this->ctcp()->ctc()->fspath( $file );
                if ( $file && $wp_filesystem->exists( $file ) ):
                    $wp_filesystem->delete( $file );
                else:
                endif;
            endforeach;
        endif;
        if ( empty( $this->ctcp()->errors ) ):
            delete_option( $this->optionName ); 
        endif;
    }
}