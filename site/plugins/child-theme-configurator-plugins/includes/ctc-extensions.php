<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* 
 * This file and all accompanying files (C) 2014-2015 Lilaea Media LLC except where noted. See license for details.
 */
 
class ChildThemeConfiguratorExtensions {
    var $frameworks = array(
        'genesis' => 'Genesis',
    );
    function __construct() {
        add_filter( 'chld_thm_cfg_backend', array( &$this, 'exclude_admin_scripts' ), 10, 2 );
    }
    function exclude_admin_scripts( $strings = array() ) {
        $strings = array_merge( $strings, array(
            '/chld[\-_]thm[\-_]cfg/',
        ) );
        return $strings;
    }
    
    function load_frameworks( $parent ) {
        // special handling for Framework themes
        if ( in_array( $parent, array_keys( $this->frameworks ) ) ):
            $include = CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-' . $parent . '.php';
            $class   = 'ChildThemeConfigurator' . $this->frameworks[ $parent ];
            if ( file_exists( $include ) && ! class_exists ( $class ) ):
                include_once( $include );
                new $class();
            endif;
        endif;
    }
    
}