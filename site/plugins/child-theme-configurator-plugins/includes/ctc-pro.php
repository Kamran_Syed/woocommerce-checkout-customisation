<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* 
 * This file and all accompanying files (C) 2014-2015 Lilaea Media LLC except where noted. See license for details.
 */
 
class ChildThemeConfiguratorPro {
    
    var $plugindir;
    var $options        = array();
    var $plugins;
    var $ctcu;          // upgrade class
    var $pluginmode     = FALSE;
    var $recent_count   = 50; // number of selectors to show
    var $ext;
    var $errors;
    var $menus          = array();
    var $preview_mods   = FALSE;


    function __construct() {
        $this->plugindir                            = dirname( CHLD_THM_CFG_PLUGINS_DIR );
        $options                                    = get_site_option( CHLD_THM_CFG_OPTIONS );
        $options = (array) $options;                // type mismatch from older version
        // prune any options from prior versions
        $this->options[ 'update_key' ]              = isset( $options[ 'update_key' ] ) ? $options[ 'update_key' ]  : '';
        $this->options[ 'addl_css' ]                = isset( $options[ 'addl_css' ] ) ? $options[ 'addl_css' ]  : array();
        include_once( CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-extensions.php' );
        $this->ext                                  = new ChildThemeConfiguratorExtensions();

        // filter hooks 
        add_filter( 'chld_thm_cfg_varname',         array( $this, 'set_upgrade_value' ), 10, 3 );
        add_filter( 'chld_thm_cfg_header',          array( $this, 'get_ctc_header' ) );
        add_filter( 'chld_thm_cfg_pc_panel',        array( $this, 'get_pc_panel' ) );
        add_filter( 'chld_thm_cfg_action',          array( $this, 'get_action' ) );
        add_filter( 'chld_thm_cfg_admin_page',      array( $this, 'get_admin_page' ) );
        add_filter( 'chld_thm_cfg_option',          array( $this, 'get_option_base' ) );
        add_filter( 'chld_thm_get_prop',            array( $this, 'get_prop' ), 10, 3 );
        add_filter( 'chld_thm_cfg_parnt',           array( $this, 'get_parent' ), 10, 2 );
        add_filter( 'chld_thm_cfg_child',           array( $this, 'get_child' ), 10, 2 );
        add_filter( 'chld_thm_cfg_target',          array( $this, 'get_target' ), 10, 2 );
        add_filter( 'chld_thm_cfg_target_uri',      array( $this, 'get_target_uri' ), 10, 2 );
        add_filter( 'chld_thm_cfg_css_header',      array( $this, 'get_css_header' ), 10, 2 );
        add_filter( 'chld_thm_cfg_update_msg',      array( $this, 'get_update_msg' ), 10, 2 );
        add_filter( 'chld_thm_cfg_localize_array',  array( $this, 'localize_array' ) );

        // action hooks
        add_action( 'chld_thm_cfg_pluginmode',      array( $this, 'set_pluginmode' ) );
        add_action( 'chld_thm_cfg_load',            array( $this, 'framework_hook' ) );
        add_action( 'chld_thm_cfg_preprocess',      array( $this, 'preprocess' ) );
        add_action( 'chld_thm_cfg_cache_updates',   array( $this, 'cache_updates' ), 10, 2 );
        add_action( 'chld_thm_cfg_update_qsid',     array( $this, 'update_qsid' ), 10, 2 );
        add_action( 'chld_thm_cfg_tabs',            array( $this, 'render_addl_tabs' ), 1, 4 );
        add_action( 'chld_thm_cfg_panels',          array( $this, 'render_addl_panels' ), 1, 4 );
        add_action( 'chld_thm_cfg_sidebar',         array( $this, 'render_sidebar' ) );
        add_action( 'chld_thm_cfg_forms',           array( $this, 'process_update_key_form' ), 10, 2 );
        add_action( 'chld_thm_cfg_files_tab',       array( $this, 'render_update_key_form' ), 10, 2 );
        add_action( 'chld_thm_cfg_addl_files',      array( $this, 'add_plugin_files'), 10, 2);
        add_action( 'admin_enqueue_scripts',        array( $this, 'enqueue_scripts' ), 999 );
    }
    // helper function to deref CTC class instance
    function ctc() {
        return ChildThemeConfigurator::ctc();
    }
        
    function css() {
        return $this->ctc()->css;
    }   
        
    function debug( $msg = NULL, $function = NULL ) {
        if ( $this->options[ 'is_debug' ] ) $this->debug .= $function . ': ' . $msg . LF;
    }
    
    function add_plugin_files( $obj ){
        // add ctc-plugins.css file
        if ( FALSE === $obj->write_child_file( 'ctc-plugins.css', $this->get_css_header( NULL, $obj->css ) ) )
            $this->ctc()->debug( 'Write to ctc-plugins.css failed.', __FUNCTION__ );
        $this->enqueue_plugin_css();
    }

    /**
     * sets active plugins property (array) by iterating plugins and checking if active
     */
    function get_active_plugins() {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        $active = array();
        foreach( get_plugins() as $handle => $data ):
            if ( is_plugin_active( $handle ) ) $active[ $handle ] = $data[ 'Name' ];
        endforeach;
        $this->ctc()->debug( 'Active plugins: ' . LF . print_r( $active, TRUE ), __FUNCTION__ );
        $this->plugins = $active;
    }
    
    function preprocess() {
        $this->ctc()->debug( 'Plugin Mode: ' . ( $this->is_pluginmode() ? 'plugin' : 'theme' ), __FUNCTION__ );
        $this->get_active_plugins();
        $upgrade_mode = 'check';
        if ( isset( $_POST[ 'ctc_upgrade' ] ) && $this->ctc()->validate_post( 'ctc_plugin' ) ):
            $upgrade_mode = isset( $_POST[ 'ctc_upgrade_migrate' ] ) ? 'migrate' : 'skip';
            $this->ctc()->debug( 'Upgrade mode: ' . $upgrade_mode, __FUNCTION__ );
        endif;        
        if ( $this->upgrade( $upgrade_mode ) ):
            if ( count( $this->errors ) && $this->ctc()->is_post && $this->ctc()->fs ):
                remove_action( 'admin_notices', array( $this->ctc(), 'config_notice' ) );
                add_action( 'admin_notices', array( $this, 'error_notice' ) ); 
                $this->ctc()->set_skip_form();
                $this->ctc()->debug( 'Errors detected: ' . LF . implode( LF, $this->errors ), __FUNCTION__ );
            elseif ( $this->ctc()->is_get ):	
                remove_action( 'admin_notices', array( $this->ctc(), 'config_notice' ) );
                add_action( 'admin_notices', array( $this, 'upgrade_notice' ) );                
                $this->ctc()->set_skip_form();
                $this->ctc()->debug( 'Upgrade required: ' . $upgrade_mode, __FUNCTION__ );
            endif;
        else:
            $this->ctc()->debug( 'No upgrade required.', __FUNCTION__ );
        endif;
            
        if ( $this->is_pluginmode() ):
            // populate config with current child theme data
            if ( $cfg = $this->get_theme_config() ):
                $this->ctc()->debug( 'Retrieved theme from existing child theme ' . LF . print_r( $cfg, TRUE ), __FUNCTION__ );
                $this->css()->set_prop( 'parnt',               $cfg[ 'parnt' ]         );
                $this->css()->set_prop( 'child',               $cfg[ 'child' ]         );
                $this->css()->set_prop( 'child_name',          $cfg[ 'child_name' ]    );
                if ( empty( $this->css()->child_author ) ):
                    $this->css()->set_prop( 'child_author',    $cfg[ 'child_author' ]  );
                endif;
                if ( empty( $this->css()->child_version ) ):
                    $this->css()->set_prop( 'child_version',   $cfg[ 'child_version' ] );
                endif;
                if ( empty( $this->css()->child_author ) ):
                    $this->css()->set_prop( 'child_author',    $cfg[ 'child_author' ]  );
                endif;
                if ( empty( $this->css()->child_version ) ):
                    $this->css()->set_prop( 'child_version',   $cfg[ 'child_version' ] );
                endif;
                $this->css()->set_prop( 'configtype', 'plugin' );
            else:
                $this->ctc()->debug( 'No child theme configured', __FUNCTION__ );
                // no child theme is configured, return notice
                $linktext = __( 'Click here to set up Child Themes', 'chld_thm_cfg' );
                $link = '<a href="' . ( is_multisite() ? 
                    network_admin_url( 'themes.php' ) : 
                        admin_url( 'tools.php' ) ) . '?page=' . CHLD_THM_CFG_MENU .'" 
                            title="' . $linktext . '">' . $linktext . '</a>';
                $this->ctc()->errors[] = 
                    sprintf( __( 'Plugin stylesheets cannot be set up until you configure a child theme. %s', 'chld_thm_cfg' ), $link );
                $this->ctc()->set_skip_form();
            endif;
        endif;
    }
    
    function upgrade( $action = 'check' ) {
        
        include_once( CHLD_THM_CFG_PLUGINS_DIR . '/includes/ctc-upgrade.php' );
        $this->ctcu = new ChildThemeUpgrade();
        if ( ! $this->ctcu->init() ) return FALSE;
        if ( 'check' == $action ) return TRUE;
        
        $this->ctc()->verify_creds();
        
        if ( 'migrate' == $action ):
            $this->ctcu->migrate_plugin_stylesheets();
        endif;
        
        if ( 'migrate' == $action && count( $this->errors ) ):
            return TRUE;
        endif;
        
        $this->ctcu->delete_options();
        $this->set_upgrade_values();
        return FALSE;
    }

    /**
     * If we are upgrading from legacy CTCP we need to get the config vars from the 
     * existing child theme and assign them to the new plugin config. If it was in plugin mode
     * then we need to create a new child theme CSS config object and reassign the config vars 
     * to the new object with an empty enqueue value to force a reload. Lastly we need to 
     * force a load on the new plugin CSS config object.
     */
    function set_upgrade_values() {
        if ( $cfg = $this->get_theme_config() ):
            $css = new ChildThemeConfiguratorCSS();
            $css->set_prop( 'parnt',            $cfg[ 'parnt' ]         );
            $css->set_prop( 'child',            $cfg[ 'child' ]         );
            $css->set_prop( 'child_author',     $cfg[ 'child_author' ]  );
            $css->set_prop( 'child_version',    $cfg[ 'child_version' ] );
            $css->set_prop( 'child_name',       $cfg[ 'child_name' ]    );

            // was this theme in plugin mode?
            if ( !empty( $cfg[ 'configtype' ] ) && 'theme' != $cfg[ 'configtype' ] ):
                $css->addl_css = $cfg[ 'addl_css' ];
                $css->set_prop( 'configtype', 'theme');
                $css->save_config( '' ); // override to save new theme css config object
            endif;
            $css->set_prop( 'configtype', 'plugin');
            $css->set_prop( 'enqueue', 'enqueue');
        endif;
        $this->ctc()->css = $css;

        // parse child stylesheet, backup or skip ( to reset )
        $this->css()->parse_css_file( 'child' );
        
        // parse additional stylesheets
        $this->css()->addl_css = array();
        foreach ( $this->options[ 'addl_css' ] as $file => $set):
            $this->css()->parse_css_file( 'parnt', $file );
            $this->css()->addl_css[] = $file;
        endforeach;
    
        // try to write new stylsheet. If it fails send alert.
        if ( FALSE === $this->css()->write_css() ):
            $this->ctc()->errors[] = __( 'Your stylesheet is not writable.', 'chld_thm_cfg' );
            add_action( 'admin_notices', array( $this, 'writable_notice' ) ); 	
            return FALSE;
        endif; 
        
        $this->enqueue_plugin_css();
        // save new object to WP options table
        $this->css()->save_config();
    }

    function get_theme_config() {
        if ( ( $cfg = get_site_option( CHLD_THM_CFG_OPTIONS . '_configvars' ) )
            && isset( $cfg[ 'child' ] )
                && $this->ctc()->check_theme_exists( $cfg[ 'child' ] )
                    && $this->ctc()->check_theme_exists( $cfg[ 'parnt' ] ) )
            return $cfg;
        return FALSE;
    }
    
    function get_css_header( $header, $css ) {
        if ( !$this->is_pluginmode() ) return $header;
        return '/*' . LF
            . 'Name: ' . 'Child Theme Configurator Plugin Custom Styles' . LF
            . 'Author: ' . $css->get_prop( 'author' ). LF
            . 'Version: ' . $css->get_prop( 'version' ) . LF
            . 'Updated: ' . current_time( 'mysql' ) . LF
            . '*/' . LF
            . '@charset "UTF-8";' . LF;
    }
        
    function get_ctc_header( $content ) {
        if ( $this->is_pluginmode() )
            return __( 'Child Theme Plugin Configurator', 'chld_thm_cfg_plugins' ) 
            . ' ' . __( 'version', 'chld_thm_cfg' ) . ' ' . CHLD_THM_CFG_PLUGINS_VERSION;
        else 
            return __( 'Child Theme Configurator Pro', 'chld_thm_cfg_plugins' ) 
            . ' ' . __( 'version', 'chld_thm_cfg' ) . ' ' . CHLD_THM_CFG_PLUGINS_VERSION . ' (CTC ' . CHLD_THM_CFG_VERSION . ')';
    }
    
    function get_parent( $uri, $file ) {
        return $this->get_uri( $uri, $file, 'source' );
    }
    
    function get_child( $uri, $file ) {
        return $this->get_uri( $uri, 'ctc-plugins.css', 'target' );
    }
        
    function get_target( $uri, $css ) {
        return $this->get_uri( $uri, 'ctc-plugins.css', 'target' );
    }

    function get_target_uri( $uri, $css ) {
        return $this->get_uri( $uri, 'ctc-plugins.css', 'target_uri' );
    }
        
    function get_uri( $uri, $file, $type ) {
        if ( !$this->is_pluginmode() ) return $uri;
        $child = $this->css()->get_prop( 'child' );
        $parnt = $this->css()->get_prop( 'parnt' );
        $contentdir = trailingslashit( $this->plugindir ) . $file;
        $contenturi = plugin_dir_url( trailingslashit( $this->plugindir ) . $file );
        switch ( $type ):
            case 'source':
                return $contentdir;
            case 'target':
                return trailingslashit( get_theme_root() ) . trailingslashit( $child ) . $file;
            case 'target_uri':
                return trailingslashit( get_theme_root_uri() ) . trailingslashit( $child ) . $file;
            default:
                return $uri;
        endswitch;
    }
        
    function get_update_msg( $msg, $ctc ) {
        if ( isset( $_GET[ 'updated' ] ) && 4 == $_GET[ 'updated' ] ) 
            return __( 'Update Key saved successfully.', 'chld_thm_cfg_plugins' );
        if ( !$this->is_pluginmode() ) return $msg;
        return sprintf( __( 'Plugin stylesheet for %s has been generated successfully.', 'chld_thm_cfg_plugins' ), 
           $ctc->css->get_prop( 'child_name' ) );
    }

    function render_update_key_form() {
?><div class="ctc-input-row clearfix" id="input_row_update_key">
<form id="ctc_update_key_form" method="post" action="">
    <?php 
	wp_nonce_field( 'ctc_update_key' ); ?>
      <div class="ctc-input-cell">
        <strong>
          <?php _e( 'Update Key', 'chld_thm_cfg_plugins' ); ?>
        </strong>
        <p class="howto">
          <?php _e( 'Enter the update key that was sent with your purchase to enable automatic notifications and downloads of future releases of CTC Pro.', 'chld_thm_cfg_plugins' ); ?>
        </p>
      </div>
      <div class="ctc-input-cell">
        <input class="ctc_text" id="ctpc_update_key" name="ctpc_update_key"  type="text" 
            value="<?php echo esc_attr( isset( $this->options[ 'update_key' ] ) ? $this->options[ 'update_key' ] :'' ); ?>" 
                placeholder="<?php _e( 'Update Key', 'chld_thm_cfg_plugins' ); ?>" autocomplete="off" />
      </div>
      <div class="ctc-input-cell">
        <input class="ctc_submit button button-primary" id="ctpc_save_update_key" name="ctpc_save_update_key"  type="submit" 
                value="<?php _e( 'Save', 'chld_thm_cfg_plugins' ); ?>" disabled />
      </div>
  </form>
    </div><?php 
    }
    
    function process_update_key_form( $ctc ) {
        if ( isset( $_POST[ 'ctpc_save_update_key' ] ) || isset( $_POST[ 'ctpc_is_debug' ] ) ):
            if ( $ctc->validate_post( 'ctc_update_key' ) ):
                if ( isset( $_POST[ 'ctpc_save_update_key' ] ) ):
                    $this->options[ 'update_key' ] = preg_replace( "/\W/", '', sanitize_text_field( $_POST[ 'ctpc_update_key' ] ) );
                endif;
                update_option( CHLD_THM_CFG_OPTIONS, $this->options );
                    $this->ctc()->debug( 'Updated options: ' . LF . print_r( $this->options, TRUE ), __FUNCTION__ );
            else:
                $this->ctc()->debug( 'Invalid nonce: ', __FUNCTION__ );
            endif;
            $ctc->update_redirect( '4&tab=file_options' );
        endif;
    }
    
    /**
     * returns css files for active plugins
     */
    function get_css_files(){
        $candidates = array();
        $this->ctc()->debug( 'Getting Plugin stylesheets from ' . $this->plugindir . ' ...', __FUNCTION__ );
        foreach ( $this->plugins as $handle => $name):
            $parentdir = dirname( $handle );
            $dir = trailingslashit( $this->plugindir ) . $parentdir;
            $this->ctc()->debug( 'Checking ' . $dir . ' ...', __FUNCTION__ );
            foreach ( $this->css()->recurse_directory( $dir ) as $filepath ):
                $file = plugin_basename( $filepath );
                $skip = FALSE;
                foreach ( apply_filters( 'chld_thm_cfg_backend', array() ) as $regex ):
                    if ( preg_match( $regex, $file ) ):
                        $skip = TRUE;
                        break;
                    endif;
                endforeach;
                if (!$skip):
                    $label = '<strong>' . $name . '</strong> (' . preg_replace( '/^' . preg_quote( $parentdir) . '/', '', $file ) . ')';
                    $candidates[ $file ] = $label;
                    $this->ctc()->debug( 'Added ' . $label, __FUNCTION__ );
                else:
                    $this->ctc()->debug( 'Skipped ' . $filepath, __FUNCTION__ );
                endif;
            endforeach;
        endforeach;
        if ( empty( $candidates ) )
            $this->ctc()->debug( 'No plugin stylesheets!', __FUNCTION__ );
        return $candidates; 
    }
        
    function render_addl_tabs( $ctc, $active_tab = NULL, $hidechild = '' ) {      
        remove_action( 'chld_thm_cfg_tabs', array( $this->ctc(), 'render_addl_tabs' ) );
        $child  = $ctc->css->get_prop( 'child' );
        $allowed = $ctc->themes[ 'child' ][ $child ][ 'allowed' ]; 
?><a id="all_styles" href="?page=<?php echo CHLD_THM_CFG_PLUGINS_MENU; ?>&amp;tab=all_styles" 
                    class="nav-tab<?php echo 'all_styles' == $active_tab ? ' nav-tab-active' : ''; ?>" <?php echo $hidechild; ?>>
<?php _e( 'All Styles', 'chld_thm_cfg_plugins' ); ?>
</a><?php if ( current_user_can( 'switch_themes' ) && ( !is_multisite() || $allowed ) ): ?>
<a id="live_preview" href="?page=<?php echo CHLD_THM_CFG_PLUGINS_MENU; ?>&amp;tab=live_preview" 
                    class="nav-tab<?php echo 'live_preview' == $active_tab ? ' nav-tab-active' : ''; ?>" <?php echo $hidechild; ?>>
<?php _e( 'Preview', 'chld_thm_cfg_plugins' ); ?>
</a><?php endif; ?>
<a id="recent_edits" class="ctc-recent-tab" href="#"><?php _e('Recent Edits', 'chld_thm_cfg'); ?></a>
<?php
    }
    
    function render_addl_panels( $ctc, $active_tab = NULL, $hidechild = '' ) {
        remove_action( 'chld_thm_cfg_panels', array( $this->ctc(), 'render_addl_panels' ) );
        $child  = $ctc->css->get_prop( 'child' );
        $parent = $ctc->css->get_prop( 'parnt' );
        $allowed = $ctc->themes[ 'child' ][ $child ][ 'allowed' ]; 
        $filter_input = '';        
        $filter_submit = '';        
        $page = apply_filters( 'chld_thm_cfg_admin_page', CHLD_THM_CFG_MENU );
        $linktext = __( 'Refresh all Selectors', 'chld_thm_cfg_plugins' );
        $link = '<a style="float:right" id="ctc_reload_selectors" href="' . ( is_multisite() ? 
            network_admin_url( 'themes.php' ) : 
                admin_url( 'tools.php' ) ) . '?page=' . $page . '&tab=all_styles" title="' . $linktext . '" class="button">' . $linktext . '</a>';
?><div id="all_styles_panel" class="ctc-option-panel<?php echo 'all_styles' == $active_tab ? ' ctc-option-panel-active' : ''; ?>" <?php echo $hidechild; ?> ><form id="all_styles_filter_form" action="" class="clearfix">
  <input name="ctc_all_styles_filter" id="ctc_all_styles_filter" value="" type="text" placeholder="<?php _e( 'Find Selectors by Text', 'chld_thm_cfg_pro' ); ?>" />
  <?php 
    if ( $locations = get_transient( 'ctcp_nav_menus_' . $parent ) ):
        $options    = array();
        foreach ( $locations as $locationid => $locationname ):
            $transient_key = 'ctcp_nav_menu_' . $parent . '_' . $locationid;
            //echo 'transient: ' . $transient_key . LF;
            $matchstr   = array();
            if ( ( $menuargs = get_transient( $transient_key ) ) ):
                if ( !empty( $menuargs[ 'menu_id' ] ) ) $matchstr[] = '#' . $menuargs[ 'menu_id' ];
                if ( !empty( $menuargs[ 'menu_class' ] ) )
                    foreach ( preg_split( "/\s+/", $menuargs[ 'menu_class' ] ) as $class ) $matchstr[] = '.' . $class;
                $options[]  = '<option value="' . implode( '|', $matchstr ) . '">' . $locationname . '</option>' . LF;
                //echo 'nav menu: ' . $locationid . LF;
                //print_r( $menuargs );
            endif;
        endforeach;
        if ( $options ): ?>
    <select id="ctc_all_styles_nav" name="ctc_all_styles_nav" style="margin-top:-2px">
    <option value=""><?php _e( 'Find Selectors by Nav Menu', 'chld_thm_cfg_plugins' ); ?></option>
    <?php echo implode( LF, $options ); ?>
    </select><?php 
        endif; 
    endif; ?>
  <input name="ctc_all_styles_submit" id="ctc_all_styles_submit" value="<?php _e( 'Go', 'chld_thm_cfg_plugins' ); ?>" type="submit" class="button-primary button"  /> <?php echo $link; ?>
  </form>
    <div class="ctc-three-col"><?php $this->render_all_selectors(); ?></div>
  </div><?php if ( !is_multisite() || $allowed ):
  ?><div id="live_preview_panel" class="ctc-option-panel<?php echo 'live_preview' == $active_tab ? ' ctc-option-panel-active' : ''; ?>" <?php echo $hidechild; ?> ><iframe src=""></iframe></div><?php endif;    
    }
    
    function render_all_selectors() {
        // turn the dictionaries into indexes (value => id into id => value):
        $selarr  = array_flip( $this->css()->dict_sel );
        $output = '<ul>' . LF;
        foreach ( $this->css()->sort_queries() as $query => $sort_order ):
            $has_selector = 0;
            $sel_output   = '<li>' . LF;
            $selectors = $this->css()->sel_ndx[ $this->css()->dict_query[ $query ] ];
            uasort( $selectors, array( $this->css(), 'cmp_seq' ) );
            $sel_output .=  '<strong>' . $query . '</strong>' . LF . '<ul>';
            foreach ( $selectors as $selid => $qsid ):
                $has_value = 0;
                $sel = $selarr[ $selid ];
                //if ( !empty( $this->css()->val_ndx[ $qsid ] ) ):
                    $has_selector = 1;

                    //$shorthand = array();
                    $sel_output .= '<li><a href="#" class="ctc-selector-edit" id="ctc_selector_edit_' . $qsid . '" >' . $sel . '</a></li>' . LF;
                //endif;
            endforeach;
            $sel_output .= '</ul></li>' . LF;
            if ( $has_selector ) $output .= $sel_output;
        endforeach;
        $output .= '</ul>' . LF;
        echo $output;
    }
    
    function render_sidebar() {
?><div class="ctc-recent-container"><div id="ctc_recent_selectors"></div></div><?php
    }

    function get_pc_panel( $path ) {
        if ( !$this->is_pluginmode() ) return $path;
        return trailingslashit( dirname( __FILE__ ) ) . 'parent-child.php';
    }
    
    function enqueue_plugin_css() {
        $marker = 'CTC ENQUEUE PLUGIN ACTION';
        $code = "// AUTO GENERATED - Do not modify or remove comment markers above or below
if ( !function_exists( 'chld_thm_cfg_plugin_css' ) ):
    function chld_thm_cfg_plugin_css() {
        wp_enqueue_style( 'chld_thm_cfg_plugins', trailingslashit( get_stylesheet_directory_uri() ) . 'ctc-plugins.css' );
    }
endif;
// using high priority so plugin custom styles load last. 
add_action( 'wp_print_styles', 'chld_thm_cfg_plugin_css', 9999 );
";
        $insertion = explode( "\n", $code );
        if ( $filename = $this->css()->is_file_ok( 
            $this->css()->get_child_target( 'functions.php' ), 'write' ) ):
            if ( FALSE !== $this->ctc()->insert_with_markers( $filename, $marker, $insertion ) ):
                $this->ctc()->debug( 'Enqueue action added', __FUNCTION__ );
                return;
            endif;
        endif;
        $this->ctc()->debug( 'Could not write enqueue action.', __FUNCTION__ );
    }
    
    function is_pluginmode() {
        return $this->pluginmode;
    }
    
    function set_pluginmode() {
        $this->pluginmode = TRUE;
        $this->ctc()->debug( 'Setting plugin mode.', __FUNCTION__ );
    }
    
    function get_option_base( $option ) {
        if ( $this->is_pluginmode() ) $option .= '_plugins';
        return $option;
    }
    
    function get_admin_page( $option ) {
        if ( $this->is_pluginmode() ) $option = CHLD_THM_CFG_PLUGINS_MENU;
        return $option;
    }

    function get_action( $option ) {
        if ( $this->is_pluginmode() ) $option = ( 'ctc_update' == $option ? 'ctc_plugin' : 'ctc_pdgqry' );
        return $option;
    }

    function get_prop( $null, $obj, $params ) {
        switch ( $obj ):
            case 'all_styles':
                ob_start();
                $this->render_all_selectors();
                $results = ob_get_contents();
                ob_end_clean();
                return $results;
        endswitch;
        return FALSE;
    }
    
    function denorm_recent() {
        $arr = array();
        foreach ( $this->css()->recent as $qsid ):
            $selarr = $this->css()->denorm_query_sel( $qsid );
            if (! empty( $selarr ) )
                $arr[] = array( $qsid => $selarr[ 'selector' ] );
        endforeach;
        return $arr;
    }

    function update_qsid( $qsid ) {
        // update recently modified selectors array
        while ( FALSE !== ( $key = array_search( $qsid, $this->css()->recent ) ) ) unset( $this->css()->recent[ $key ] );
        array_unshift( $this->css()->recent, $qsid );
        if ( count( $this->css()->recent ) > $this->recent_count ) array_pop( $this->css()->recent );
    }
    
    function cache_updates() {
        $this->ctc()->updates[] = array(
            'obj'   => 'recent',
            'key'   => '',
            'data'  => $this->denorm_recent()
        );
    }
    
    function upgrade_notice() {
    ?>
    <div class="update-nag">
        <p><?php _e( 'Child Theme Configurator Pro has detected an earlier version of the Plugin Stylesheet Extension. Click "Migrate" if you wish to migrate your original plugin stylesheets or "Skip" if you wish to continue with a fresh install.', 'chld_thm_cfg' ) ?>
        <form method="post" action="?page=<?php echo CHLD_THM_CFG_PLUGINS_MENU; ?>"><?php wp_nonce_field( 'ctc_plugin' ); ?>
        <input type="submit" value="<?php _e( 'Skip', 'chld_thm_cfg_plugins' ); ?>" name="ctc_upgrade_skip" > 
        <input type="submit" value="<?php _e( 'Migrate', 'chld_thm_cfg_plugins' ); ?>" name="ctc_upgrade_migrate" >
        <label><input type="checkbox" value="1" name="ctc_remove_stylesheets" /><?php _e( 'also remove original stylesheets', 'chld_thm_cfg_plugins' ); ?></label>
        <input type="hidden" name="ctc_upgrade" value="1" />
        </form></p>
    </div>
    <?php
    }
    function error_notice() {
    ?>
    <div class="update-nag">
        <p><?php _e( 'Child Theme Configurator Pro could not complete the migration. The following issues were detected:', 'chld_thm_cfg_plugins' ); ?>
        <ul><?php
        foreach ( $this->errors as $theme => $err )
            foreach ( $err as $file => $msg )
                echo '<li>' . $file . ' <strong>' . __( $msg, 'chld_thm_cfg_plugins' ) . '</strong></li>' . LF; ?>
        </ul><?php
        _e( 'Check your server for permission issues and click "Try again" to run the upgrade or "Finish" if you wish to continue with a fresh install.', 'chld_thm_cfg' ) ?>
        <form method="post" action="?page=<?php echo CHLD_THM_CFG_PLUGINS_MENU; ?>"><?php wp_nonce_field( 'ctc_plugin' ); ?>
        <input type="submit" value="<?php _e( 'Finish', 'chld_thm_cfg_plugins' ); ?>" name="ctc_upgrade_skip" > 
        <input type="submit" value="<?php _e( 'Try Again', 'chld_thm_cfg_plugins' ); ?>" name="ctc_upgrade_migrate" >
        <label><input type="checkbox" value="1" name="ctc_remove_stylesheets" /><?php _e( 'also remove original stylesheets', 'chld_thm_cfg_plugins' ); ?></label>
        <input type="hidden" name="ctc_upgrade" value="1" />
        </form></p>
    </div>
    <?php
    }
    
    function localize_array( $array ) {
        $array[ 'recent_txt' ]  = __( 'No recent edits.', 'chld_thm_cfg_plugins' );
        $array[ 'palette' ] = 1;
        return $array;
    }
    
    function enqueue_scripts() { //
        wp_enqueue_script( 'ctc-thm-cfg-pro', CHLD_THM_CFG_PLUGINS_URL . 'js/chld_thm_cfg_pro.min.js', array( 'chld-thm-cfg-admin' ), FALSE, TRUE );
    }
    
    function framework_hook() {
        // special handling for Framework themes
        $this->ext->load_frameworks( $this->ctc()->get_current_parent() );
    }

}