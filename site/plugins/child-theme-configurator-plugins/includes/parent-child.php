<?php  
if ( !defined( 'ABSPATH' ) ) exit;
// Parent/Child Panel
?>

<div id="parent_child_options_panel" class="ctc-option-panel<?php echo 'parent_child_options' == $active_tab ? ' ctc-option-panel-active' : ''; ?>">
  <form id="ctc_load_form" method="post" action="">
    <?php 
    wp_nonce_field( 'ctc_plugin' ); ?>
    <input class="ctc-hidden" id="ctc_theme_parnt" name="ctc_theme_parnt" type="hidden" value="<?php echo $this->ctc()->css->get_prop( 'parnt' ); ?>" />
    <input class="ctc-hidden" id="ctc_theme_child" name="ctc_theme_child" type="hidden" value="<?php echo $this->ctc()->css->get_prop( 'child' ); ?>" />
    <input class="ctc-hidden" id="ctc_action" name="ctc_action" type="hidden" value="plugin" />
    <input class="ctc-hidden" id="ctc_child_type" name="ctc_child_type" type="hidden" value="existing" />
    <div class="ctc-input-row clearfix ctc-themeonly-container" id="input_row_parnt">
      <div class="ctc-input-cell"> <strong>
        <?php _e( 'Parent Theme', 'chld_thm_cfg' ); ?>
        </strong> </div>
      <div class="ctc-input-cell">
        <?php echo esc_attr( $this->ctc()->themes[ 'parnt' ][ $this->ctc()->css->get_prop( 'parnt' ) ][ 'Name' ] ); ?>
      </div>
    </div>
    <div class="ctc-input-row clearfix ctc-themeonly-container" id="input_row_child">
      <div class="ctc-input-cell"> <strong>
        <?php _e( 'Child Theme', 'chld_thm_cfg' ); ?>
        </strong> </div>
      <div class="ctc-input-cell">
        <?php echo esc_attr( $this->ctc()->themes[ 'child' ][ $this->ctc()->css->get_prop( 'child' ) ][ 'Name' ] ); ?>
      </div>
    </div>
    <div class="ctc-input-row clearfix">
      <div class="ctc-input-cell"> <strong>
        <?php _e( 'Author', 'chld_thm_cfg' ); ?>
        </strong> </div>
      <div class="ctc-input-cell">
        <input class="ctc_text" id="ctc_child_author" name="ctc_child_author" type="text" 
                value="<?php echo esc_attr( $css->get_prop( 'author' ) ); ?>" placeholder="<?php _e( 'Author', 'chld_thm_cfg' ); ?>" autocomplete="off" />
      </div>
    </div>
    <div class="ctc-input-row clearfix">
      <div class="ctc-input-cell"> <strong>
        <?php _e( 'Version', 'chld_thm_cfg' ); ?>
        </strong> </div>
      <div class="ctc-input-cell">
        <input class="ctc_text" id="ctc_child_version" name="ctc_child_version" type="text" 
                value="<?php echo esc_attr( $css->get_prop( 'version' ) ); ?>" placeholder="<?php _e( 'Version', 'chld_thm_cfg' ); ?>" autocomplete="off" />
      </div>
    </div>
    <div class="ctc-input-row clearfix" id="ctc_stylesheet_files"><?php
        $stylesheets = ChildThemeConfiguratorPlugins::ctcp()->get_css_files();
        if ( count( $stylesheets ) ):?>
        <div class="ctc-input-cell ctc-section-toggle" id="ctc_additional_css_files"> <strong>
          <?php _e( 'Parse Plugin stylesheets:', 'chld_thm_cfg' ); ?>
          </strong> </div>
        <div class="ctc-input-cell-wide ctc-section-toggle-content" id="ctc_additional_css_files_content" style="display:block">
          <p style="margin-top:0">
            <?php _e( 'Select the plugin stylesheets you wish to customize below.', 'chld_thm_cfg' ); ?>
          </p><ul><?php 
            foreach ( $stylesheets as $stylesheet => $label ): ?>
            <li><label>
              <input class="ctc_checkbox" name="ctc_additional_css[]" type="checkbox" 
                value="<?php echo $stylesheet; ?>" />
              <?php echo $label; ?></label></li><?php 
            endforeach; ?>
        </ul></div><?php 
        endif; ?>
    </div>
    <div class="ctc-input-row clearfix">
      <div class="ctc-input-cell"> <strong>
        <?php _e( 'Backup current stylesheet', 'chld_thm_cfg' ); ?>
        </strong> </div>
      <div class="ctc-input-cell">
        <input class="ctc_checkbox" id="ctc_backup" name="ctc_backup" type="checkbox" 
                value="1" />
      </div>
      <div class="ctc-input-cell"> <strong>
        <?php _e( 'NOTE:', 'chld_thm_cfg' ); ?>
        </strong>
        <?php _e( 'This creates a copy of the current stylesheet before applying changes. You can remove old backup files using the Files tab.', 'chld_thm_cfg' ); ?>
      </div>
    </div>
    <div class="ctc-input-row clearfix">
      <div class="ctc-input-cell ctc-section-toggle" id="ctc_revert_css"> <strong>
        <?php _e( 'Reset/Restore from backup:', 'chld_thm_cfg' ); ?>
        </strong> </div>
      <div class="ctc-input-cell-wide ctc-section-toggle-content" id="ctc_revert_css_content">
        <label>
          <input class="ctc_checkbox" id="ctc_revert_none" name="ctc_revert" type="radio" 
                value="" checked="" />
          <?php _e( 'Leave unchanged', 'chld_thm_cfg' );?>
        </label>
        <br/>
        <label>
          <input class="ctc_checkbox" id="ctc_revert_all" name="ctc_revert" type="radio" 
                value="all" />
          <?php _e( 'Reset all', 'chld_thm_cfg' );?>
        </label>
        <div id="ctc_backup_files"><?php 
        foreach ( $this->ctc()->get_files( $this->ctc()->css->get_prop( 'child' ), 'pluginbackup' ) as $backup => $label ): ?>
          <label>
            <input class="ctc_checkbox" id="ctc_revert_<?php echo $backup; ?>" name="ctc_revert" type="radio" 
                value="<?php echo $backup; ?>" />
            <?php echo __( 'Restore backup from', 'chld_thm_cfg' ) . ' ' . $label; ?></label>
          <br/><?php
        endforeach; ?>
        </div>
      </div>
    </div>
    <div class="ctc-input-row clearfix">
      <div class="ctc-input-cell"> <strong>&nbsp;</strong> </div>
      <div class="ctc-input-cell">
        <input class="ctc_submit button button-primary" id="ctc_load_styles" name="ctc_load_styles"  type="submit" 
                value="<?php _e( 'Generate/Rebuild Child Theme Files', 'chld_thm_cfg' ); ?>" disabled />
      </div>
    </div>
  </form>
</div>