;
( function( $ ) {
    //console.log( 'initializing ctc pro script...' );
    try {
        $.extend($.chldthmcfg, {
            ctc_pro_init: function() {
                var self = this;
                $( '#all_styles_filter_form' ).on( 'submit', function( e ){
                    e.preventDefault();
                    var filter  = ( $( '#ctc_all_styles_filter' ).length ? $( '#ctc_all_styles_filter' ).val() : '' ).split('|'), 
                        nav     = ( $( '#ctc_all_styles_nav' ).length ? $( '#ctc_all_styles_nav' ).val() : '' ).split('|'),
                        targets = [],
                        navs    = [], 
                        regex,
                        navregex;
                    $.each( filter, function( ndx, el ){
                        targets.push( el.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&") );
                    } );
                    $.each( nav, function( ndx, el ){
                        navs.push( el.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&") );
                    } );
                    if ( targets.length ) regex = new RegExp( '(' + targets.join('|') + ')([\W]|$)', 'i' );
                    if ( navs.length ) navregex = new RegExp( '(' + navs.join('|') + ')([\W]|$)', 'i' );
                    //console.log( 'updating all styles (filter: ' + regex + ' and ' + navregex + ')' );
                    $( '#all_styles_panel .ctc-selector-edit' ).each( function( ndx, el ){
                        var sel = $( el ).text();
                        if ( sel.match( regex ) && sel.match( navregex ) ) $( el ).show();
                        else $( el ).hide();
                    } );
                    return false;
                } );
                $( '#recent_edits' ).on( 'click', function( e ){
                    e.preventDefault();
                    if ( $( '.ctc-recent-container' ).is( ':visible' ) ) {
                        $( '.ctc-recent-container' ).stop().slideUp();
                        $( '.ctc-option-panel' ).css( { 'width': '95%' } );
                    } else {
                        // move recent edits to outer wrapper
                        if ( !$('.ctc-recent-container').hasClass( 'moved' ) ) {
                            $( '.ctc-recent-container' ).addClass( 'moved' ).detach()
                                .appendTo('#ctc_option_panel_wrapper');
                        }
                        $( '.ctc-recent-container' ).stop().slideDown();
                        $( '.ctc-option-panel' ).css( { 'width': '80%' } );
                    }
                    return false;
                } );
                $( '#live_preview, #ctc_reload_selectors' ).on( 'click', self.reload_preview );
            },
            reload_preview: function(){
                var homeurl = ctcAjax.homeurl.replace( /^https?/, ctcAjax.ssl ? 'https' : 'http' ),
                    src     = homeurl + '&preview_iframe=1&template=' + ctcAjax.parnt + '&stylesheet=' + ctcAjax.child;
                //console.log( src );
                $( '#live_preview_panel iframe' ).first().attr( 'src', src );
            }
        } );
        $.extend($.chldthmcfg.update, {
            recent: function( res ) {
                var self = this;
                //console.log( res.data );
                //console.log( 'render_recent' );
                
                var html = '';
                if ( self.is_empty( res.data ) && !self.is_empty( self.getxt( 'recent' ) ) ) {
                    html += self.getxt( 'recent' );
                } else if ( self.is_empty( res.data ) ) {
                    return;
                } else {
                    html += '<ul>' + "\n";
                    $.each( res.data, function( ndx, el ) {
                        $.each( el, function ( key, value ) {
                            html += '<li><a href="#" class="ctc-selector-edit" id="ctc_selector_edit_' + key + '" >' + value + '</a></li>' + "\n";
                        } );
                    } );
                    html += '</ul>' + "\n";
                }
                $( '#ctc_recent_selectors' ).html( html );
            }
        } );
    } catch( exn ) {
        //console.log( exn.message );
    }
}( jQuery ) );
jQuery( document ).ready( function( $ ) {
    //console.log( 'initializing pro listeners' );
    $.chldthmcfg.ctc_pro_init();
    
});