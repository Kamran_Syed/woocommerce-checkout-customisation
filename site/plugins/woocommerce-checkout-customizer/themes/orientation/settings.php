<?php 
$settings = array(
    'name' => 'orientation',
    'options' => array(
        'stepsOrientation' => array(
            'default' => 'vertical'
        ),
        'stepsPosition' => array(
            'default' => 'left'
        ),
        'stepsContainerWidth' => array(
            'default' => 25
        ),
        'showStepsNumbers' => array(
            'default' => 1
        ),
        'bodyColorsDivider' => array(
            'default' => false
        ),
        'contentBackground' => array(
            'default' => '#ffffff'
        ),
        'contentBackgroundOpacity' => array(
            'default' => 10
        ),
        'contentBorderRadius' => array(
            'default' => 0
        ),
        
        'paddingTabsDivider' => array(
            'default' => false
        ),
        'tabsPaddingTop' => array(
            'default' => 5
        ),
        'tabsPaddingBottom' => array(
            'default' => 5
        ),
        
        'intervalTabsDivider' => array(
            'default' => false
        ),
        'tabsHorizontalInterval' => array(
            'default' => 5
        ),
        'tabsVerticalInterval' => array(
            'default' => 5
        ),
        
        'borderTabsDivider' => array(
            'default' => false
        ),
        'tabsBorderRadius' => array(
            'default' => 0
        ),
        
        'backgroundColorsTabsDivider' => array(
            'default' => false
        ),
        'doneTabsColor' => array(
            'default' => '#d5d1cd'
        ),
        'disabledTabsColor' => array(
            'default' => '#eeeeee'
        ),
        'activeTabsColor' => array(
            'default' => '#8c7156'
        ),
        'hoverTabsColor' => array(
            'default' => '#333333'
        ),
        'errorTabsColor' => array(
            'default' => '#ab2e2e'
        ),
        
        'fontColorsTabsDivider' => array(
            'default' => false
        ),
        'doneFontColor' => array(
            'default' => '#797a7b'
        ),
        'disabledFontColor' => array(
            'default' => '#c7c7c7'
        ),
        'activeFontColor' => array(
            'default' => '#ffffff'
        ),
        'hoverFontColor' => array(
            'default' => '#ffffff'
        ),
        'errorFontColor' => array(
            'default' => '#ffffff'
        ),
        
        'numbersFontColorDivider' => array(
            'default' => false
        ),
        'doneNumbersFontColor' => array(
            'default' => '#797a7b'
        ),
        'disabledNumbersFontColor' => array(
            'default' => '#c7c7c7'
        ),
        'activeNumbersFontColor' => array(
            'default' => '#ffffff'
        ),
        'hoverNumbersFontColor' => array(
            'default' => '#ffffff'
        ),
        'errorNumbersFontColor' => array(
            'default' => '#ffffff'
        ),
        
        
        'borderActionsButtonsDivider' => array(
            'default' => false
        ),
        'actionsButtonsBorderRadius' => array(
            'default' => 0
        ),
        
        'actionsButtonsColorDivider' => array(
            'default' => false
        ),
        'actionsButtonsColor' => array(
            'default' => '#8c7156'
        ),
        'hoverActionsButtonsColor' => array(
            'default' => '#333333'
        ),
        'disabledActionsButtonsColor' => array(
            'default' => '#eeeeee'
        ),
        'actionsButtonsFontColorDivider' => array(
            'default' => false
        ),
        'actionsButtonsFontColor' => array(
            'default' => '#ffffff'
        ),
        'hoverActionsButtonsFontColor' => array(
            'default' => '#ffffff'
        ),
        'disabledActionsButtonsFontColor' => array(
            'default' => 'c7c7c7'
        ),
        
        'fontSizeDivider' => array(
            'default' => false
        ),
        'tabsFontSize' => array(
            'default' => 14
        ),
        'numbersFontSize' => array(
            'default' => 18
        ),
        'actionsButtonsFontSize' => array(
            'default' => 14
        ),
    )
);

return $settings;