<?php 
$settings = array(
    'name' => 'green line',
    'options' => array(
        'bodyColorsDivider' => array(
            'default' => false
        ),
        'contentBackground' => array(
            'default' => '#ffffff'
        ),
        'contentBackgroundOpacity' => array(
            'default' => 10
        ),
        'backgroundColorsTabsDivider' => array(
            'default' => false,
            'caption' => __(
                'Lines Colors',
                $this->languageDomain
            ),
        ),
        'doneTabsColor' => array(
            'default' => '#9fd134'
        ),
        'disabledTabsColor' => array(
            'default' => '#c0c0c0'
        ),
        'activeTabsColor' => array(
            'default' => '#98a1d3'
        ),
        'hoverTabsColor' => array(
            'default' => '#98a1d3'
        ),
        'errorTabsColor' => array(
            'default' => '#FF0000'
        ),
        'fontColorsTabsDivider' => array(
            'default' => false,
            'caption' => __(
                'Font colors of step Titles',
                $this->languageDomain
            ),
        ),
        'disabledFontColor' => array(
            'default' => '#c0c0c0'
        ),
        'doneFontColor' => array(
            'default' => '#000000'
        ),
        'activeFontColor' => array(
            'default' => '#000000'
        ),
        'hoverFontColor' => array(
            'default' => '#000000'
        ),
        'errorFontColor' => array(
            'default' => '#ff0000'
        ),
        'actionsButtonsColorDivider' => array(
            'default' => false
        ),
        'actionsButtonsColor' => array(
            'default' => '#98a1d3'
        ),
        'hoverActionsButtonsColor' => array(
            'default' => '#9fd134'
        ),
        'disabledActionsButtonsColor' => array(
            'default' => '#c0c0c0'
        ),
        'actionsButtonsFontColorDivider' => array(
            'default' => false
        ),
        'actionsButtonsFontColor' => array(
            'default' => '#ffffff'
        ),
        'hoverActionsButtonsFontColor' => array(
            'default' => '#ffffff'
        ),
        'disabledActionsButtonsFontColor' => array(
            'default' => '#7d7d7d'
        ),
    )
);

return $settings;