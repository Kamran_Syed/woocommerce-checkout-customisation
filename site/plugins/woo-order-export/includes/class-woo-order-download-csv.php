<?php 
	$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
	require_once($root .'/wp-load.php');
$post_id = $_GET['order_key'];
if ( isset($post_id) && !empty($post_id)) {


	$meta_values = get_post_meta( $post_id );	

	global $woo_order_exp_model, $woocommerce;
		
	$model = $woo_order_exp_model;

	$symbol = $model -> woo_order_exp_currency_symbol();


	$symbol = utf8_decode($symbol);
	
	$order = new WC_Order($post_id);
	$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
	$excel_sheet = get_option("woo_order_excel_sheet_settings");



	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=".$meta_values['_billing_first_name'][0]."-order.csv;");
	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
	header("Pragma: no-cache"); // HTTP 1.0
	header("Expires: 0"); // Proxies
		
	$output = fopen("php://output", "w");


		$data_ary = ' No ,';
		$data_ary.=$model->woo_order_exp_csv_get_column('woo_order_id_enable');
 		$data_ary.=$model->woo_order_exp_csv_get_column('woo_order_status_title_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_date_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_modified_date_enable');

 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_user_name_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_first_name_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_last_name_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_company_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_address_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_second_bill_address_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_city_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_state_enable'); 		
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_country_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_post_code_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_phone_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_email_enable');
 		
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_first_name_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_last_name_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_company_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_address_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_city_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_state_enable');	 		
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_country_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_post_code_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_method_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_payment_method_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_payment_paypal_enable');

 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_shipping_charge_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_cart_discount_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_tax_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_shipping_tax_enable');
 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_total_enable');
 		
		$data_ary.= 'Card Message,';
		$data_ary.= 'Req Delivery Date';
 		
	 
		$product_export = get_option("woo_order_product_export_settings");
	 	if ($product_export == 1) {
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_product_name_enable');
		 	$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_sku_enable');
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_price_enable');
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_quantity_enable');
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_total_enable');
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_fee_enable');
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_total_enable');

		}else{	
			foreach ( $items as $key => $item ) {	

				$prod_list[] =  $item['product_id'];
				$product =get_product($item['product_id']);
				 
				$attributes = $product->get_attributes();

				$product_name = $item['name'];
				$data_ary.= (get_option('woo_order_product_name_enable') == 1 ) ? get_option('woo_order_product_name')." :-".$product_name." ," : '' ;
				$data_ary.= (get_option('woo_order_sku_enable') == 1 ) ?  get_option('woo_order_sku')." :-".$product_name." ," : '' ;
				$data_ary.= (get_option('woo_order_price_enable') == 1 ) ? get_option('woo_order_price').":-".$product_name." ," : '' ;

				

				if (!empty($attributes)) {
					foreach ($attributes as $key => $value) {														
						$data_ary.= " ".$key." ,";
					}
				}

				$data_ary.= (get_option('woo_order_total_enable') == 1 ) ? get_option('woo_order_total')." :-".$product_name." ," : '' ;
			}
			$type = $item['type'];
			if ($type != 'line_item') {
				$data_ary.= $product_name.",";
			}	
	 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_total_enable');
		}
		

			



	$data_array[] = explode(',', $data_ary);	
	
	outputCSV( $data_array );



	$counter = 1;
	

	$data = '';
	$get_data_array = '';
	$meta_values = get_post_meta( $post_id );	
	$order = new WC_Order($post_id);

		$data .= $counter.",";
		$data.= (get_option('woo_order_id_enable') == 1 ) ? $order->id." ," : '' ;
		$data.= (get_option('woo_order_status_title_enable') == 1 ) ? $order->status." ," : '' ;
		$data.= (get_option('woo_order_date_enable') == 1 ) ? date('Y-m-d' , strtotime($order->order_date) )." ," : '' ;
		$data.= (get_option('woo_order_modified_date_enable') == 1 ) ? date('Y-m-d' , strtotime($order->modified_date) )." ," : '' ;
		
		$customer_user = get_post_meta( $post_id, '_customer_user', true );
		if (!empty($customer_user) && $customer_user != 0 ) {
			$author_data     = get_user_by( 'id', $customer_user );
			$user_name = $author_data->user_login;																										
		}else{
			$user_name = '';
		}
			
		$data.= (get_option('woo_order_user_name_enable') == 1 ) ? $user_name." ," : '' ;
		$data.= (get_option('woo_order_bill_first_name_enable') == 1 ) ? $meta_values['_billing_first_name'][0]." ," : '' ;
		$data.= (get_option('woo_order_bill_last_name_enable') == 1 ) ? $meta_values['_billing_last_name'][0]." ," : '' ;
		$data.= (get_option('woo_order_bill_company_enable') == 1 ) ? $meta_values['_billing_company'][0]." ," : '' ;
		$search =  ',' ;
		$search = str_split($search);
		$bill_first_address =str_replace($search, "", $meta_values['_billing_address_1'][0]);
		$bill_second_address =str_replace($search, "", $meta_values['_billing_address_2'][0]);		
		$data.= (get_option('woo_order_bill_address_enable') == 1 ) ? $bill_first_address." ," : '' ; 
		$data.= (get_option('woo_order_second_bill_address_enable') == 1 ) ? $bill_second_address." ," : '' ;
		$data.= (get_option('woo_order_bill_city_enable') == 1 ) ? $meta_values['_billing_city'][0]." ," : '' ;
		$data.= (get_option('woo_order_bill_state_enable') == 1 ) ? $meta_values['_billing_state'][0]." ," : '' ;
		$data.= (get_option('woo_order_bill_country_enable') == 1 ) ? WC()->countries->countries[$meta_values['_billing_country'][0]]." ," : '' ;
		$data.= (get_option('woo_order_bill_post_code_enable') == 1 ) ? $meta_values['_billing_postcode'][0]." ," : '' ;
		$data.= (get_option('woo_order_bill_phone_enable') == 1 ) ? $meta_values['_billing_phone'][0]." ," : '' ;
		$data.= (get_option('woo_order_bill_email_enable') == 1 ) ? $meta_values['_billing_email'][0]." ," : '' ;

		$data.= (get_option('woo_order_ship_first_name_enable') == 1 ) ? $meta_values['_shipping_first_name'][0]." ," : '' ;
		$data.= (get_option('woo_order_ship_last_name_enable') == 1 ) ? $meta_values['_shipping_last_name'][0]." ," : '' ;
		$data.= (get_option('woo_order_ship_company_enable') == 1 ) ? $meta_values['_shipping_company'][0]." ," : '' ;
		$ship_first_address =str_replace($search, "", $meta_values['_shipping_address_1'][0]);
		$data.= (get_option('woo_order_ship_address_enable') == 1 ) ? $ship_first_address." ," : '' ;
		$data.= (get_option('woo_order_ship_city_enable') == 1 ) ? $meta_values['_shipping_city'][0]." ," : '' ;
		$data.= (get_option('woo_order_ship_state_enable') == 1 ) ? $meta_values['_shipping_state'][0]." ," : '' ;
		$data.= (get_option('woo_order_ship_country_enable') == 1 ) ? WC()->countries->countries[$meta_values['_shipping_country'][0]]." ," : '' ;
		$data.= (get_option('woo_order_ship_post_code_enable') == 1 ) ? $meta_values['_shipping_postcode'][0]." ," : '' ;
		
		$shipping_method = $order->get_shipping_methods();
		if (!empty($shipping_method)) {
			foreach ($shipping_method as $s_key => $s_value) {
				$ship_method = $s_value['name'];
			}
		}
		$ship_method = (!empty($ship_method)) ? $ship_method : '';

		$data.= (get_option('woo_order_ship_method_enable') == 1 ) ? $ship_method." ," : '' ;
		$data.= (get_option('woo_order_payment_method_enable') == 1 ) ? $meta_values['_payment_method'][0]." ," : '' ;

		if (isset($meta_values['_paypal_email'][0])) {
			$PayPal_Id = (!empty($meta_values['_paypal_email'][0])) ? $meta_values['_paypal_email'][0].',' : ',';
		}else $PayPal_Id = ',';
		
		$data.= (get_option('woo_order_payment_paypal_enable') == 1 ) ? $PayPal_Id : '' ;
		$data.= (get_option('woo_order_shipping_charge_enable') == 1 ) ? $symbol.$meta_values['_order_shipping'][0]." ," : '' ;
		$data.= (get_option('woo_order_cart_discount_enable') == 1 ) ? $symbol.$meta_values['_cart_discount'][0]." ," : '' ;
		$data.= (get_option('woo_order_order_tax_enable') == 1 ) ? $symbol.$meta_values['_order_tax'][0]." ," : '' ;
		$data.= (get_option('woo_order_shipping_tax_enable') == 1 ) ? $symbol.$meta_values['_order_shipping_tax'][0]." ," : '' ;
		$data.= (get_option('woo_order_order_total_enable') == 1 ) ? $symbol.$meta_values['_order_total'][0]." ," : '' ;

		$data.= $meta_values['_shipping_personal_message'][0]." ,";
		$data.= $meta_values['_required_delivery_date'][0]." ,";
					
		$data.= $model->woo_order_exp_csv_get_data_sheet($post_id);

		

		$get_data_array[] = explode(',', $data);	
	
		outputCSV( $get_data_array );
    
	



}else{
	$my_error = new WP_Error( 'error', 'Order Details not Found.', 'my best' );
	$error = $my_error->get_error_messages();
	echo $error[0];exit;
}

function outputCSV($data) {
	    $output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
}
?>