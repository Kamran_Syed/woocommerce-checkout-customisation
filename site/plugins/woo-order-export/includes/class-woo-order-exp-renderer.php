<?php 

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Renderer Class
 *
 * To handles some small HTML content for front end
 * 
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */
class Woo_Order_Exp_Renderer {
	var $model;	
	public function __construct() {
		
		global $woo_order_exp_model;
		
		$this->model = $woo_order_exp_model;		
		
	}	

}
?>