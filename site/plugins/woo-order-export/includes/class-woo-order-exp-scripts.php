<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Scripts Class
 *
 * Handles adding scripts functionality to the admin pages
 * as well as the front pages.
 *
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */
class Woo_Order_Exp_Scripts{
	
	public function __construct() {
		
	}
	
	/**
	 * Enqueue Styles for backend on needed page
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_admin_styles() {
		
		
		
			wp_register_style( 'woo-order-exp-admin-styles', WOO_ORDER_EXP_URL . 'includes/css/style-admin.css', array(), null);
			wp_enqueue_style( 'woo-order-exp-admin-styles' );

			wp_enqueue_style( 'jquery-ui-datepicker' );

			wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

		
	}
	
	/**
	 * Enqueue Scripts for backend on needed page
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_admin_scripts( $hook_suffix ) {
		
		global $wp_version;
		
		
		
		$newui = $wp_version >= '3.5' ? '1' : '0'; 
		
			wp_register_script( 'woo-order-exp-admin-scripts', WOO_ORDER_EXP_URL . 'includes/js/woo-order-exp-admin.js', array('jquery', 'jquery-ui-sortable' ) , null, true );
			wp_enqueue_script( 'woo-order-exp-admin-scripts' );			
					
			
			wp_localize_script( 'woo-order-exp-admin-scripts','WooOrderExp',array( 'new_media_ui'	=>	$newui ) );
		
			wp_enqueue_media();
			wp_enqueue_script( 'media-upload' );
			wp_enqueue_script( 'jquery');
			wp_enqueue_script( 'jquery-ui-core');
			wp_enqueue_script("jquery-ui-datepicker");

			
			wp_register_script( 'woo-order-choosen', WOO_ORDER_EXP_URL . 'includes/js/woo-order-chosen.jquery.js', array('jquery') , null, true );
			wp_enqueue_script( 'woo-order-choosen' );	
			wp_register_style( 'woo-order-choosen-css', WOO_ORDER_EXP_URL . 'includes/css/woo-order-chosen.css', array(), null);
			wp_enqueue_style( 'woo-order-choosen-css' );	
			
		
	}
	
	
	
	
		
	/**
	 * Display button in post / page container
	 *
	 * Handles to display button in post / page container
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_shortcode_display_button( $buttons ) {
	 
		array_push( $buttons, "|", "woo_order_exp_login" );
		return $buttons;
	}
	
	/**
	 * Include js for add button in post / page container
	 *
	 * Handles to include js for add button in post / page container
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_shortcode_button($plugin_array) {
	 
		$plugin_array['woo_order_exp_login'] = WOO_ORDER_EXP_URL . 'includes/js/woo-order-exp-shortcodes.js';
		return $plugin_array;
	}
	
	

		

	
	/**
	 * Adding Hooks
	 *
	 * Adding proper hoocks for the scripts.
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function add_hooks() {

		//add styles for back end
		add_action( 'admin_enqueue_scripts', array($this, 'woo_order_exp_admin_styles') );
		
		//add script to back side for order export
		add_action( 'admin_enqueue_scripts', array($this, 'woo_order_exp_admin_scripts') );
		
		
	}
}
?>