<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Admin Class
 *
 * Handles generic Admin functionality and AJAX requests.
 *
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */
class Woo_Order_Exp_Admin {
	
	var $model, $scripts;
	
	public function __construct() {	
	
		global $woo_order_exp_model, $woo_order_exp_scripts;
		
		$this->model = $woo_order_exp_model;
		$this->scripts = $woo_order_exp_scripts;
	}
	
	/**
	 *  Register All need admin menu page
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	
	public function woo_order_exp_admin_menu_pages(){
		
		$woo_order_export = add_submenu_page( 'woocommerce', __( 'WooCommerce Order Export', 'wooorderexp' ), __( 'Order Export', 'wooorderexp' ), 'manage_woocommerce', 'woo-order-export', array( $this, 'woo_order_export' ));
		//script for order export page
		
	}
	
	/**
	 * Add order export Page
	 * 
	 * Handles to load order export 
	 * page to show order export register data
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_export() {
		
		include_once( WOO_ORDER_EXP_ADMIN . '/forms/woo-commerce-order-export.php' );
		
	}
	
	/**
	 * Add PDF actions to the orders listing
	 */
	function woo_order_exp_add_listing_actions( $order ) {
		
		$listing_actions = array(
			
			/* 'excel'		=> array (
				'url'		=> WOO_ORDER_EXP_URL."includes/class-woo-order-export-xls.php?post_id=" . $order->id,
				'img'		=> WOO_ORDER_EXP_URL.'includes/images/excel.png',
				'alt'		=> esc_attr__( 'Excel Export', 'wpo_wcpdf' ),
			),
			'pdf'		=> array (
				'url'		=> WOO_ORDER_EXP_URL."includes/class-woo-order-export-pdf.php?post_id=" . $order->id,
				'img'		=> WOO_ORDER_EXP_URL.'includes/images/pdf.png',
				'alt'		=> esc_attr__( 'PDF Export', 'wpo_wcpdf' ),
			), */
			'csv'		=> array (
				'url'		=> WOO_ORDER_EXP_URL."includes/class-woo-order-export-csv.php?post_id=" . $order->id,
				'img'		=> WOO_ORDER_EXP_URL.'includes/images/csv.png',
				'alt'		=> esc_attr__( 'CSV Export', 'wpo_wcpdf' ),
			),
			
		);
		
		foreach ($listing_actions as $action => $data) {
			?>
			<a href="<?php echo $data['url']; ?>" class="button tips woo_order <?php echo $action; ?>" target="_blank" alt="<?php echo $data['alt']; ?>" data-tip="<?php echo $data['alt']; ?>">
				<img src="<?php echo $data['img']; ?>" alt="<?php echo $data['alt']; ?>" width="16">
			</a>
			<?php
		}
	}


	/**
	 * Add Attachment on order mail
	 *
	 * @package WooCommerce - Order Export
	 * @since 2.1.3
	 */
	public function woo_order_exp_send_file($attachments, $email_type = null, $order = null)
    {
    	$attached = get_option('woo_order_exp_attach_invoice');
		if (isset($order->id) && !empty($order->id) && $attached==1) {
			$attach_file = get_option("woo_order_excel_attached_file_type");	
			if ($attach_file == 1) {	
				$my_file = $this->model->woo_order_exp_send_invoice_excel($order->id);
			}elseif ($attach_file == 2) {
				$my_file = $this->model->woo_order_exp_send_invoice_csv($order->id);
			}
        	
        	$file = WOO_ORDER_EXP_SAVE_FILE.'/'.$my_file;
        	$attachments[] = $file;
			return $attachments;
        }
    }     	
	


	/**
	 * Add Link on order details page
	 *
	 * @package WooCommerce - Order Export
	 * @since 2.1.3
	 */
	public function woo_order_exp_customer_export_link($order)
    {
        $customer_invoice = get_option('woo_order_exp_buyer_invoice');
        if ($customer_invoice == 1) {
            $invoice_file = get_option("woo_order_excel_buyer_invoice_file_type");	
			if ($invoice_file == 1) {	
				$download_url = WOO_ORDER_EXP_URL."includes/class-woo-order-download-xls.php?order_key=" . $order->id;
				$export_image = WOO_ORDER_EXP_URL.'includes/images/excel.png';
				$export_text = get_option('woo_order_excel_buyer_invoice_file_title');
			}elseif ($invoice_file == 2) {
				$download_url = WOO_ORDER_EXP_URL."includes/class-woo-order-download-csv.php?order_key=" . $order->id;
				$export_image = WOO_ORDER_EXP_URL.'includes/images/csv.png';
				$export_text = get_option('woo_order_excel_buyer_invoice_file_title');
			}           
            $download_button = '<p style="padding: 15px 0;"><a  href="'.$download_url.'" >' .
                               '<img src="'.$export_image.'" alt="Invoice" width="20" height="20">' .
                               '<span style="padding-left: 10px;">'.$export_text.'</span>' .
                               '</a></p>';
            echo $download_button;            
        }

    }


	/**
	 * Adding Hooks
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function add_hooks() {
		
		//add admin menu pages
		add_action ( 'admin_menu', array($this,'woo_order_exp_admin_menu_pages' ));		

		add_action( 'woocommerce_admin_order_actions_end', array($this,'woo_order_exp_add_listing_actions') );

		//send invoice
		add_filter('woocommerce_email_attachments', array($this, 'woo_order_exp_send_file'), 10, 3);


		//Download link for user
		add_action('woocommerce_order_details_after_order_table', array($this, 'woo_order_exp_customer_export_link'));
	}
		
}
?>