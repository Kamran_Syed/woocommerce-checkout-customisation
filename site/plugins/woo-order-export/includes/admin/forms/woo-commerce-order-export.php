<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Admin Pages Class
 *
 * Handles generic Admin functionailties
 *
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */
global $woo_order_exp_model;
	
$model = $woo_order_exp_model;

?>
<div class="wrap woocommerce">
	
		<div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div><h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
			<?php
			    $current_tab     = empty( $_GET['tab'] ) ? 'export_order' : sanitize_title( $_GET['tab'] );
			    $tabs =array(
			    		'export_order'=> 'Export Order',
                        'pdf_settings'=>'PDF Settings',
  						'excel_setting'=>'Excel/CSV Settings',
  						'buyer_invoice'=>'Buyer Invoice',
  						'email_setting'=>'Emails', 						
  						
			    	);
				foreach ( $tabs as $name => $label )
					echo '<a href="' . admin_url( 'admin.php?page=woo-order-export&tab=' . $name ) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . $label . '</a>';

				
			?>
		</h2>
		<?php
			switch ($current_tab) {

				case 'export_order':
						require_once('settings/class-woo-order-exp-export.php');
					break;
					
				case 'pdf_settings':
						require_once('settings/class-woo-order-exp-pdf.php');
					break;

				case 'excel_setting':
						require_once('settings/class-woo-order-exp-excel.php');
					break;

				case 'buyer_invoice':
						require_once('settings/class-woo-order-exp-buyer-invoice.php');
					break;

				case 'email_setting':
						require_once('settings/class-woo-order-exp-emails.php');
					break;
					
				
				default:
					
					break;
			}
		?>
        
</div>