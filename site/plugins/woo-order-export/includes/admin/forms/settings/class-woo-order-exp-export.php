<?php 
ob_start();
	
 	global $woo_order_exp_model;	
	$model = $woo_order_exp_model;

	$html = '';
	$order_date_start = $order_date_end  = "";
	
	if(isset($_POST['wooorderexp_settings_save']) && !empty($_POST['wooorderexp_settings_save']) && $_POST['wooorderexp_settings_save'] == __( 'Export', 'wooorderexp' )) { //check click of reset button
		
		//date filter
		if (isset($_POST['order_date_start']) && $_POST['order_date_start'] != "") {
			$order_date_start = '&startdate='.$_POST['order_date_start'];
		}else{$order_date_start='';}	

		if (isset($_POST['order_date_end']) && $_POST['order_date_end'] != "") {
			$order_date_end = '&enddate='.$_POST['order_date_end'];
		}else{$order_date_end='';}	


		// export type
		if (isset($_POST['export_type']) && $_POST['export_type'] == "") {
			$errmsg['export_type']='Please Select Export Type';		
		}	
		


		$status = $_POST['order_export_status']; // Order status

		

		//for ptoduct
		if (isset($_POST['order_export_product_list']) && !empty( $_POST['order_export_product_list'] )) {
			
			$order_export_product_list = implode(',' , $_POST['order_export_product_list']);			

			$order_export_product_list = '&product_list='.$order_export_product_list;
		}else{
			$order_export_product_list = '&product_list=all';
		}

		
		//for category
		if (isset($_POST['order_export_product_cat_list']) && !empty( $_POST['order_export_product_cat_list'] )) {
			
			$order_export_product_cat_list = implode(',' , $_POST['order_export_product_cat_list']);			

			$order_export_product_cat_list = '&product_cat_list='.$order_export_product_cat_list;
		}else{
			$order_export_product_cat_list = '&product_cat_list=all';
		}


		//for Seller
		if (isset($_POST['order_export_product_seller_list']) && !empty( $_POST['order_export_product_seller_list'] )) {
			
			$order_export_product_seller_list = implode(',' , $_POST['order_export_product_seller_list']);			

			$order_export_product_seller_list = '&product_seller_list='.$order_export_product_seller_list;
		}else{
			$order_export_product_seller_list = '&product_seller_list=all';
		}

		//for order
		if (isset($_POST['order_export_order_list']) && !empty( $_POST['order_export_order_list'] )) {
			
			$order_export_order_list = implode(',' , $_POST['order_export_order_list']);			

			$order_export_order_list = '&order_list='.$order_export_order_list;
		}else{
			$order_export_order_list='';
		}

		//for Register Customer
		if (isset($_POST['order_export_register_user_list']) && !empty( $_POST['order_export_register_user_list'] )) {
			
			$order_export_register_user_list = implode(',' , $_POST['order_export_register_user_list']);			

			$order_export_register_user_list = '&signup_user_list='.$order_export_register_user_list;
		}else{
			$order_export_register_user_list='';
		}
		
		//for Billing Name
		if (isset($_POST['order_export_billing_name_list']) && !empty( $_POST['order_export_billing_name_list'] )) {
			
			$args = array(															        														        
		        'post_type'       => 'shop_order',
		        'post_status'     => array_keys( wc_get_order_statuses() ),														       
		        'suppress_filters' => false,   
				'posts_per_page' => -1
			);
			$posts_bill_name=get_posts($args);
			$bill_name_array = $_POST['order_export_billing_name_list'];
			foreach ($posts_bill_name as $b_key => $b_value) {
				$meta_values = get_post_meta( $b_value->ID );
				if (in_array($meta_values['_billing_email'][0], $bill_name_array)) {
					$orders_id[] = $b_value->ID;
				}				
			}			
			$orders_id = implode(',' , $orders_id);	
			$order_export_order_list = (empty($order_export_order_list)) ? '&order_list=' : $order_export_order_list.',';	
			$order_export_order_list = $order_export_order_list.$orders_id;			
		}
		

		//for Billing Email
		if (isset($_POST['order_export_email_list']) && !empty( $_POST['order_export_email_list'] )) {
			
			$args = array(															        														        
		        'post_type'       => 'shop_order',
		        'post_status'     => array_keys( wc_get_order_statuses() ),														       
		        'suppress_filters' => false,   
				'posts_per_page' => -1
			);
			$posts_bill_name=get_posts($args);
			$bill_name_array = $_POST['order_export_email_list'];
			foreach ($posts_bill_name as $b_key => $b_value) {
				$meta_values = get_post_meta( $b_value->ID );
				if (in_array($meta_values['_billing_email'][0], $bill_name_array)) {
					$orders_mail_id[] = $b_value->ID;
				}				
			}
			
			$orders_id = implode(',' , $orders_mail_id);	
			$order_export_order_list = (empty($order_export_order_list)) ? '&order_list=' : $order_export_order_list.',';	
			$order_export_order_list = $order_export_order_list.$orders_id;	

		}

		if (isset($_POST['export_type']) && $_POST['export_type'] == "excel") { 
			
			$path = WOO_ORDER_EXP_URL.'includes/admin/forms/woo-commerce-order-export-excel.php?status='.$status.$order_date_start.$order_date_end.$order_export_product_list.$order_export_product_cat_list.$order_export_product_seller_list.$order_export_order_list.$order_export_register_user_list;
			echo "<script type='text/javascript'>window.open('".$path."','_blank');</script>";

		}
		if (isset($_POST['export_type']) && $_POST['export_type'] == "csv") {
			
			$path = WOO_ORDER_EXP_URL.'includes/admin/forms/woo-commerce-order-export-csv.php?status='.$status.$order_date_start.$order_date_end.$order_export_product_list.$order_export_product_cat_list.$order_export_product_seller_list.$order_export_order_list.$order_export_register_user_list;
			echo "<script type='text/javascript'>window.open('".$path."','_blank');</script>";

		}	
		if (isset($_POST['export_type']) && $_POST['export_type'] == "pdf") {

			$path = WOO_ORDER_EXP_URL.'includes/admin/forms/woo-commerce-order-export-pdf.php?status='.$status.$order_date_start.$order_date_end.$order_export_product_list.$order_export_product_cat_list.$order_export_product_seller_list.$order_export_order_list.$order_export_register_user_list;
			echo "<script type='text/javascript'>window.open('".$path."','_blank');</script>";
		}	
		
		
	}

	
	$html .= '<div class="wrap">'.screen_icon('options-general');
	
	$html .= '<h2>'.__("Export Your Order's", 'wooorderexp').'</h2>';
	
	
	
	// beginning of the plugin options form
	$html .= '<form  method="post" action="" enctype="multipart/form-data">';

	echo $html;

	$html = '<!-- beginning of the settings meta box -->

				<div id="wpd-ws-settings" class="post-box-container">
				
					<div class="metabox-holder">	
				
						<div class="meta-box-sortables ui-sortable">
				
							<div id="settings" class="postbox">	
				
							
				
									<!-- settings box title -->
				
									<h3 class="hndle">
				
										<span style="vertical-align: top;">'. __( 'Export Page', 'wooorderexp' ).'</span>
				
									</h3>
				
									<div class="inside">';
	
							$html .= '	<table class="form-table wpd-ws-settings-box"> 
											<tbody>';
								
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Export Type:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="radio" name="export_type" value="excel" checked="checked">Excel
														<input type="radio" name="export_type" value="csv">CSV
														<input type="radio" name="export_type" value="pdf">PDF
													</td>
													<td>';
													if(isset($errmsg['export_type']) && !empty($errmsg['export_type'])) {
														$html .= '<div class="error-msg">'.$errmsg['export_type'].'</div>';
													}
										$html .='</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Selected Order Id:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><select  class="chosen-select" id="woo-order-products" name="order_export_order_list[]" multiple data-placeholder="Choose a Order id...">';

													$args = array(															        														        
												        'post_type'       => 'shop_order',
												        'post_status'     => array_keys( wc_get_order_statuses() ),														       
												        'suppress_filters' => false,   
														'posts_per_page' => -1
													);
													$posts=get_posts($args);													
													foreach ($posts as $key => $value) {
														$html .="<option value='".$value->ID."'>".$value->ID." - ".$value->post_title."</option>";
													}

										$html .='</select></br>
													<span class="description">'.__( 'If you select Order Id than only selected id can export.', 'wooorderexp' ).'</span>
										</td><td>';
													
										$html .='</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Products:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><select  class="chosen-select" id="woo-order-products" name="order_export_product_list[]" multiple data-placeholder="Choose a Product...">
													<option value="all" selected="selected">All</option>';

													$args = array(															        														        
												        'post_type'       => 'product',
												        'post_status'     => 'publish',															       
												        'suppress_filters' => false, 
														'posts_per_page' => -1  
													);
													$posts=get_posts($args);													
													foreach ($posts as $key => $value) {
														$html .="<option value='".$value->ID."'>".$value->post_title."</option>";
													}

										$html .='</select></td>
													<td>';
													
										$html .='</td>
												 </tr>';


										
									
										$html .='<tr>
												<th scope="row">
													<label><strong>'.__( 'Products Category:', 'wooorderexp' ).'</strong></label>
												</th>
												<td><select  class="chosen-select" id="woo-order-products" name="order_export_product_cat_list[]" multiple data-placeholder="Choose a Product...">
												<option value="all" selected="selected">All</option>';

												$prod_cat_args = array(
															  'taxonomy'     => 'product_cat', //woocommerce
															  'orderby'      => 'name',
															  'empty'        => 0
															);

												$woo_categories = get_categories( $prod_cat_args );	
												
												foreach ($woo_categories as $key => $value) {
													$html .="<option value='".$value->term_id."'>".$value->name."</option>";
												}

										$html .='</select></td>
													<td>';													
										$html .='</td>
												 </tr>';

													$html .='<tr>
												<th scope="row">
													<label><strong>'.__( 'Products Seller / Author :', 'wooorderexp' ).'</strong></label>
												</th>
												<td><select  class="chosen-select" id="woo-order-products" name="order_export_product_seller_list[]" multiple data-placeholder="Choose a Seller...">
												<option value="all" selected="selected">All</option>';

												$args = array(															        														        
												        'post_type'       => 'product',
												        'post_status'     => 'publish',															       
												        'suppress_filters' => false, 
														'posts_per_page' => -1  
													);
												$posts_author=get_posts($args);
												$author_array = array();
												foreach ($posts_author as $key => $value) {
													if (!in_array($value->post_author, $author_array)) {

														$author     = get_user_by( 'id', $value->post_author );

														$html .="<option value='".$value->post_author."'>".$author->display_name."</option>";
														$author_array[] = $value->post_author;
													}
													
												}

										$html .='</select></td>
													<td>';
										$html .='</td>
												 </tr>';


										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Registerd Customer:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><select  class="chosen-select" id="woo-order-products" name="order_export_register_user_list[]" multiple data-placeholder="Choose a Customer...">';

													$args = array(															        														        
												        'post_type'       => 'shop_order',
												        'post_status'     => array_keys( wc_get_order_statuses() ),														       
												        'suppress_filters' => false,   
														'posts_per_page' => -1
													);
													$posts_reg_cust=get_posts($args);
													

													$reg_cust_array = array();
													foreach ($posts_reg_cust as $r_key => $r_value) {
														
														$customer_user = get_post_meta( $r_value->ID, '_customer_user', true );
														if (!empty($customer_user) && $customer_user != 0 ) {
															$author_data     = get_user_by( 'id', $customer_user );
															$customer_name = $author_data->display_name;

															if (!in_array($customer_user, $reg_cust_array)) {
															
																$html .="<option value='".$customer_user."'>".$customer_name."</option>";
																$reg_cust_array[] = $customer_user;
															}
														}
														
													}
											

										$html .='</select></br>
													<span class="description">'.__( 'If you select Register Customers name than only selected Customer orders can export.', 'wooorderexp' ).'</span>
										</td><td>';

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Customer Billing Name:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><select  class="chosen-select" id="woo-order-products" name="order_export_billing_name_list[]" multiple data-placeholder="Choose a Billing Name...">';

													$args = array(															        														        
												        'post_type'       => 'shop_order',
												        'post_status'     => array_keys( wc_get_order_statuses() ),														       
												        'suppress_filters' => false,   
														'posts_per_page' => -1
													);
													$posts_bill_name=get_posts($args);
													

													$bill_name_array = array();
													foreach ($posts_bill_name as $b_key => $b_value) {
														$meta_values = get_post_meta( $b_value->ID );
														if (!in_array($meta_values['_billing_email'][0], $bill_name_array)) {
															$html .="<option value='".$meta_values['_billing_email'][0]."'>".$meta_values['_billing_first_name'][0]." ".$meta_values['_billing_last_name'][0]."</option>";
															$bill_name_array[] = $meta_values['_billing_email'][0];
														}
														
													}
											

										$html .='</select></br>
													<span class="description">'.__( 'If you select  Customers Billing name than only selected Customer orders can export.', 'wooorderexp' ).'</span>
										</td><td>';

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Customer Email:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><select  class="chosen-select" id="woo-order-products" name="order_export_email_list[]" multiple data-placeholder="Choose a Email...">';

													$args = array(															        														        
												        'post_type'       => 'shop_order',
												        'post_status'     => array_keys( wc_get_order_statuses() ),														       
												        'suppress_filters' => false,   
														'posts_per_page' => -1
													);
													$posts_bill_name=get_posts($args);
													

													$bill_name_array = array();
													foreach ($posts_bill_name as $b_key => $b_value) {
														$meta_values = get_post_meta( $b_value->ID );
														if (!in_array($meta_values['_billing_email'][0], $bill_name_array)) {
															$html .="<option value='".$meta_values['_billing_email'][0]."'>".$meta_values['_billing_email'][0]."</option>";
															$bill_name_array[] = $meta_values['_billing_email'][0];
														}
														
													}
											

										$html .='</select></br>
													<span class="description">'.__( 'If you select  Customers Email than only selected Email orders can export.', 'wooorderexp' ).'</span>
										</td><td>';
										
										
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Select Order Status :', 'wooorderexp' ).'</strong></label>
													</th>
													<td><select name="order_export_status">
															<option value="all">All</option>
															<option value="wc-cancelled">Cancelled</option>
															<option value="wc-completed">Completed</option>
															<option value="wc-failed">Failed</option>
															<option value="wc-on-hold">On Hold</option>
															<option value="wc-pending">Pending</option>
															<option value="wc-processing">Processing</option>															
															<option value="wc-refunded">Refunded</option>														
															
														</select>
													</td>
													<td>';
										$html .='</td>
												 </tr>';
													
										
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Select Date :', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="date" id="order_date_start" name="order_date_start" value="" placeholder="From Order Date" class="order-date-start" />
													<input type="date" id="order_date_end" name="order_date_end" value="" placeholder="To Order Date" class="order-date-end" />
													</td>';
													
										$html .='</tr>';
										
										$html .= '<tr>
													<td colspan="2">
														<input type="submit" class="button-primary wpd-ws-settings-save" name="wooorderexp_settings_save" class="" value="'.__( 'Export', 'wooorderexp' ).'" />
													</td>
												</tr>';
										$html .= '<tr>
													<td colspan="2">
														<span>Note : If File does not downlod than allow popup from your browser settings.</span>									
													</td>
												</tr>';
										
							$html .= '		</tbody>
										</table>';	
							
	$html .= '					</div><!-- .inside -->
					
							</div><!-- #settings -->
				
						</div><!-- .meta-box-sortables ui-sortable -->
				
					</div><!-- .metabox-holder -->
				
				</div><!-- #wps-settings-general -->
				
				<!-- end of the settings meta box -->';
	
	$html .= '</form>';
	
	$html .= '</div><!-- .wrap -->';
	
	echo $html;
	
?>