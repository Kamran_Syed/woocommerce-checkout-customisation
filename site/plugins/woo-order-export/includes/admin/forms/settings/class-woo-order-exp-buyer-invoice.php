<?php  	
	$html = '';	
	//all settings will reset as per default
	if(isset($_POST['wooorderexp_reset_settings']) && !empty($_POST['wooorderexp_reset_settings']) && $_POST['wooorderexp_reset_settings'] == __( 'Reset To Deafault', 'wooorderexp' )) { //check click of reset button
		
		update_option('woo_order_exp_buyer_invoice',0);
		update_option('woo_order_excel_buyer_invoice_file_type',1);
		update_option('woo_order_excel_buyer_invoice_file_title','Download Order Report');
		$html .= '<div class="updated" id="message">
					<p><strong>'.__("All Settings Reset Successfully.",'wooorderexp').'</strong></p>
				</div>';
		
	}
	
	//all settings will Save
	if(isset($_POST['wooorderexp_settings_save']) && !empty($_POST['wooorderexp_settings_save']) && $_POST['wooorderexp_settings_save'] == __( 'Save Changes', 'wooorderexp' )) { 
		
		if (isset($_POST['woo_order_exp_buyer_invoice']))  update_option('woo_order_exp_buyer_invoice',$_POST['woo_order_exp_buyer_invoice']); else update_option('woo_order_exp_buyer_invoice',0);
		
		update_option('woo_order_excel_buyer_invoice_file_type',$_POST['woo_order_excel_buyer_invoice_file_type']);
		
		update_option('woo_order_excel_buyer_invoice_file_title',$_POST['woo_order_excel_buyer_invoice_file_title']);

		
		$html = '<div class="updated" id="message">
					<p><strong>'.__("Changes Saved Successfully.",'wooorderexp').'</strong></p>
				</div>';
	}
	
	$excel_sheet = ''; $csv_sheet='';
	$attach_file = get_option("woo_order_excel_buyer_invoice_file_type");	
	if ($attach_file == 1) {	
		$excel_sheet = "checked='checked'";
	}elseif ($attach_file == 2) {
		$csv_sheet = "checked='checked'";
	}
	$woo_order_excel_buyer_invoice_file_title = get_option('woo_order_excel_buyer_invoice_file_title');
	
	$html .= '<div class="wrap">'.screen_icon('options-general');
	
	$html .= '<h2>'.__('Buyer Invoice Settings', 'wooorderexp').'</h2>';
	
	$html .= '<div class="wpd-ws-reset-setting">
				<form method="post" action="">
					<input type="submit" class="button-primary" name="wooorderexp_reset_settings" value="'.__( 'Reset To Deafault', 'wooorderexp' ).'" />
				</form>
			</div>';
	
	// beginning of the plugin options form
	$html .= '<form  method="post" action="" enctype="multipart/form-data">';

	echo $html;

	
	$html = '<!-- beginning of the settings meta box -->

				<div id="wpd-ws-settings" class="post-box-container">
				
					<div class="metabox-holder">	
				
						<div class="meta-box-sortables ui-sortable">
				
							<div id="settings" class="postbox">	
				
											
									<!-- settings box title -->
				
									<h3 class="hndle">
				
										<span style="vertical-align: top;">'. __( 'Allow buyer to download Excel & CSV Reports', 'wooorderexp' ).'</span>
				
									</h3>
				
									<div class="inside">';
	
							$html .= '	<table class="form-table wpd-ws-settings-box"> 
											<tbody>';

										$buyer_invoice = get_option('woo_order_exp_buyer_invoice');
										$buyer_invoice_check = ($buyer_invoice == 1) ? 'checked="checked"' : '';
										$html .='<tr>
												<th scope="row">														
													<label><strong>'.__( 'Allow Invoice', 'wooorderexp' ).'</strong></label>
												</th>
												<td>
													<input type="checkbox"  name="woo_order_exp_buyer_invoice" '.$buyer_invoice_check.' value="1" />
													</br>
													<span class="description">'.__( 'If you select Allow Invoice than Buyer(Customer) will be able to download invoices(CSV & Excel) from order details page.', 'wooorderexp' ).'</span>
												</td>
											 </tr>';
											 
										$html .='<tr>
												<th scope="row">														
													<label><strong>'.__( 'File(Invoice) Type', 'wooorderexp' ).'</strong></label>
												</th>
												<td>
													<input type="radio"  name="woo_order_excel_buyer_invoice_file_type" '.$excel_sheet.' value="1" />Excel
													<input type="radio"  name="woo_order_excel_buyer_invoice_file_type" '.$csv_sheet.' value="2" />CSV
													</br>
													<span class="description">'.__( 'Buyer(Customer) will be able to download invoices on selected files type only from the order details page.', 'wooorderexp' ).'</span>
												</td>												
											 </tr>';

										$html .='<tr>
												<th scope="row">														
													<label><strong>'.__( 'Download Title', 'wooorderexp' ).'</strong></label>
												</th>
												<td>
													<input type="text"  name="woo_order_excel_buyer_invoice_file_title"  value="'.$woo_order_excel_buyer_invoice_file_title.'" />
													
													</br>
													<span class="description">'.__( 'File title for download text on order details page.', 'wooorderexp' ).'</span>
												</td>												
											 </tr>';

																			
										$html .= '<tr>
													<td colspan="2">
														<input type="submit" class="button-primary" name="wooorderexp_settings_save" class="" value="'.__( 'Save Changes', 'wooorderexp' ).'" />
													</td>
												</tr>';
										
										
							$html .= '		</tbody>
										</table>';	
							
	$html .= '					</div><!-- .inside -->
					
							</div><!-- #settings -->
				
						</div><!-- .meta-box-sortables ui-sortable -->
				
					</div><!-- .metabox-holder -->
				
				</div><!-- #wps-settings-general -->
				
				<!-- end of the settings meta box -->';
	
	$html .= '</form>';
	
	$html .= '</div><!-- .wrap -->';
	
	echo $html;	
?>