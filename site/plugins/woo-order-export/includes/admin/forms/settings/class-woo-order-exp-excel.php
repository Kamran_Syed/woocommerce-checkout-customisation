<?php  	
	$html = '';	
	//all settings will reset as per default
	if(isset($_POST['wooorderexp_reset_settings']) && !empty($_POST['wooorderexp_reset_settings']) && $_POST['wooorderexp_reset_settings'] == __( 'Reset To Deafault', 'wooorderexp' )) { //check click of reset button
		
		$model->woo_order_exp_excel_default();
		
		$html .= '<div class="updated" id="message">
					<p><strong>'.__("All Settings Reset Successfully.",'wooorderexp').'</strong></p>
				</div>';
		
	}
	
	//all settings will Save
	if(isset($_POST['wooorderexp_settings_save']) && !empty($_POST['wooorderexp_settings_save']) && $_POST['wooorderexp_settings_save'] == __( 'Save Changes', 'wooorderexp' )) { 
		
		$model->woo_order_exp_excel_save_settings($_POST); 
		
		if (isset($_POST['woo_order_excel_setings_enable']))  update_option('woo_order_excel_setings_enable',$_POST['woo_order_excel_setings_enable']); else update_option('woo_order_excel_setings_enable',0);
		update_option('woo_order_product_export_settings',$_POST['woo_order_product_export_settings']);
		update_option('woo_order_excel_diff_title_settings',$_POST['woo_order_excel_diff_title_settings']);
		
		if (isset($_POST['woo_order_id_enable'])) update_option('woo_order_id_enable',$_POST['woo_order_id_enable']); else update_option('woo_order_id_enable',0);
		if (isset($_POST['woo_order_status_title_enable'])) update_option('woo_order_status_title_enable',$_POST['woo_order_status_title_enable']); else update_option('woo_order_status_title_enable',0);
		if (isset($_POST['woo_order_date_enable'])) update_option('woo_order_date_enable',$_POST['woo_order_date_enable']); else update_option('woo_order_date_enable',0);
		if (isset($_POST['woo_order_modified_date_enable'])) update_option('woo_order_modified_date_enable',$_POST['woo_order_modified_date_enable']); else update_option('woo_order_modified_date_enable',0); 
		if (isset($_POST['woo_order_payment_method_enable'])) update_option('woo_order_payment_method_enable',$_POST['woo_order_payment_method_enable']); else update_option('woo_order_payment_method_enable',0);
		if (isset($_POST['woo_order_payment_paypal_enable'])) update_option('woo_order_payment_paypal_enable',$_POST['woo_order_payment_paypal_enable']); else update_option('woo_order_payment_paypal_enable',0);
		if (isset($_POST['woo_order_shipping_charge_enable'])) update_option('woo_order_shipping_charge_enable',$_POST['woo_order_shipping_charge_enable']); else update_option('woo_order_shipping_charge_enable',0);
		if (isset($_POST['woo_order_cart_discount_enable'])) update_option('woo_order_cart_discount_enable',$_POST['woo_order_cart_discount_enable']); else update_option('woo_order_cart_discount_enable',0);
		if (isset($_POST['woo_order_order_tax_enable'])) update_option('woo_order_order_tax_enable',$_POST['woo_order_order_tax_enable']); else update_option('woo_order_order_tax_enable',0);
		if (isset($_POST['woo_order_shipping_tax_enable'])) update_option('woo_order_shipping_tax_enable',$_POST['woo_order_shipping_tax_enable']); else update_option('woo_order_shipping_tax_enable',0);
		if (isset($_POST['woo_order_sku_enable'])) update_option('woo_order_sku_enable',$_POST['woo_order_sku_enable']); else update_option('woo_order_sku_enable',0);
		if (isset($_POST['woo_order_product_name_enable'])) update_option('woo_order_product_name_enable',$_POST['woo_order_product_name_enable']); else update_option('woo_order_product_name_enable',0);
		if (isset($_POST['woo_order_price_enable'])) update_option('woo_order_price_enable',$_POST['woo_order_price_enable']); else update_option('woo_order_price_enable',0);
		if (isset($_POST['woo_order_quantity_enable'])) update_option('woo_order_quantity_enable',$_POST['woo_order_quantity_enable']); else update_option('woo_order_quantity_enable',0);
		if (isset($_POST['woo_order_total_enable'])) update_option('woo_order_total_enable',$_POST['woo_order_total_enable']); else update_option('woo_order_total_enable',0);
		if (isset($_POST['woo_order_fee_enable'])) update_option('woo_order_fee_enable',$_POST['woo_order_fee_enable']); else update_option('woo_order_fee_enable',0);
		if (isset($_POST['woo_order_order_total_enable'])) update_option('woo_order_order_total_enable',$_POST['woo_order_order_total_enable']); else update_option('woo_order_order_total_enable',0);

		//customer default excel/csv
		if (isset($_POST['woo_order_user_name_enable'])) update_option('woo_order_user_name_enable',$_POST['woo_order_user_name_enable']); else update_option('woo_order_user_name_enable',0);
		if (isset($_POST['woo_order_bill_first_name_enable'])) update_option('woo_order_bill_first_name_enable',$_POST['woo_order_bill_first_name_enable']); else update_option('woo_order_bill_first_name_enable',0);
		if (isset($_POST['woo_order_bill_last_name_enable'])) update_option('woo_order_bill_last_name_enable',$_POST['woo_order_bill_last_name_enable']); else update_option('woo_order_bill_last_name_enable',0);
		if (isset($_POST['woo_order_bill_company_enable'])) update_option('woo_order_bill_company_enable',$_POST['woo_order_bill_company_enable']); else update_option('woo_order_bill_company_enable',0);
		if (isset($_POST['woo_order_bill_address_enable'])) update_option('woo_order_bill_address_enable',$_POST['woo_order_bill_address_enable']); else update_option('woo_order_bill_address_enable',0);
		if (isset($_POST['woo_order_second_bill_address_enable'])) update_option('woo_order_second_bill_address_enable',$_POST['woo_order_second_bill_address_enable']); else update_option('woo_order_second_bill_address_enable',0);
		if (isset($_POST['woo_order_bill_city_enable'])) update_option('woo_order_bill_city_enable',$_POST['woo_order_bill_city_enable']); else update_option('woo_order_bill_city_enable',0);
		if (isset($_POST['woo_order_bill_state_enable'])) update_option('woo_order_bill_state_enable',$_POST['woo_order_bill_state_enable']); else update_option('woo_order_bill_state_enable',0);
		if (isset($_POST['woo_order_bill_country_enable'])) update_option('woo_order_bill_country_enable',$_POST['woo_order_bill_country_enable']); else update_option('woo_order_bill_country_enable',0);
		if (isset($_POST['woo_order_bill_post_code_enable'])) update_option('woo_order_bill_post_code_enable',$_POST['woo_order_bill_post_code_enable']); else update_option('woo_order_bill_post_code_enable',0);
		if (isset($_POST['woo_order_bill_phone_enable'])) update_option('woo_order_bill_phone_enable',$_POST['woo_order_bill_phone_enable']); else update_option('woo_order_bill_phone_enable',0);
		if (isset($_POST['woo_order_bill_email_enable'])) update_option('woo_order_bill_email_enable',$_POST['woo_order_bill_email_enable']); else update_option('woo_order_bill_email_enable',0);

		if (isset($_POST['woo_order_ship_first_name_enable'])) update_option('woo_order_ship_first_name_enable',$_POST['woo_order_ship_first_name_enable']); else update_option('woo_order_ship_first_name_enable',0);
		if (isset($_POST['woo_order_ship_last_name_enable'])) update_option('woo_order_ship_last_name_enable',$_POST['woo_order_ship_last_name_enable']); else update_option('woo_order_ship_last_name_enable',0);
		if (isset($_POST['woo_order_ship_company_enable'])) update_option('woo_order_ship_company_enable',$_POST['woo_order_ship_company_enable']); else update_option('woo_order_ship_company_enable',0);
		if (isset($_POST['woo_order_ship_address_enable'])) update_option('woo_order_ship_address_enable',$_POST['woo_order_ship_address_enable']); else update_option('woo_order_ship_address_enable',0);
		if (isset($_POST['woo_order_ship_city_enable'])) update_option('woo_order_ship_city_enable',$_POST['woo_order_ship_city_enable']); else update_option('woo_order_ship_city_enable',0);
		if (isset($_POST['woo_order_ship_state_enable'])) update_option('woo_order_ship_state_enable',$_POST['woo_order_ship_state_enable']); else update_option('woo_order_ship_state_enable',0);
		if (isset($_POST['woo_order_ship_country_enable'])) update_option('woo_order_ship_country_enable',$_POST['woo_order_ship_country_enable']); else update_option('woo_order_ship_country_enable',0);
		if (isset($_POST['woo_order_ship_post_code_enable'])) update_option('woo_order_ship_post_code_enable',$_POST['woo_order_ship_post_code_enable']); else update_option('woo_order_ship_post_code_enable',0);
		if (isset($_POST['woo_order_ship_method_enable'])) update_option('woo_order_ship_method_enable',$_POST['woo_order_ship_method_enable']); else update_option('woo_order_ship_method_enable',0);
		
		$html = '<div class="updated" id="message">
					<p><strong>'.__("Changes Saved Successfully.",'wooorderexp').'</strong></p>
				</div>';
	}
	
	$same_sheet = ''; $diff_sheet='';
	$excel_sheet = get_option("woo_order_excel_sheet_settings");	
	if ($excel_sheet == 1) {
		$same_sheet = "checked='checked'";
	}else{
		$diff_sheet = "checked='checked'";
	}

	$product_export = get_option("woo_order_product_export_settings");	
	if ($product_export == 1) {
		$prod_same_sheet = "checked='checked'";
		$prod_diff_sheet ='';
	}else{
		$prod_diff_sheet = "checked='checked'";
		$prod_same_sheet ='';
	}
	
	$diff_title_settings = get_option("woo_order_excel_diff_title_settings");	
	if ($diff_title_settings == 1) {
		$diff_sheet_one_title = "checked='checked'";
		$diff_sheet_all_title="";		
	}else{
		$diff_sheet_all_title = "checked='checked'";
		$diff_sheet_one_title="";		
	}

	$html .= '<div class="wrap">'.screen_icon('options-general');
	
	$html .= '<h2>'.__('Set Your Template Settings For Excel/CSV', 'wooorderexp').'</h2>';
	
	$html .= '<div class="wpd-ws-reset-setting">
				<form method="post" action="">
					<input type="submit" class="button-primary" name="wooorderexp_reset_settings" value="'.__( 'Reset To Deafault', 'wooorderexp' ).'" />
				</form>
			</div>';
	
	// beginning of the plugin options form
	$html .= '<form  method="post" action="" enctype="multipart/form-data">';

	echo $html;

	
	$html = '<!-- beginning of the settings meta box -->

				<div id="wpd-ws-settings" class="post-box-container">
				
					<div class="metabox-holder">	
				
						<div class="meta-box-sortables ui-sortable">
				
							<div id="settings" class="postbox">	
				
											
									<!-- settings box title -->
				
									<h3 class="hndle">
				
										<span style="vertical-align: top;">'. __( 'Excel/CSV Template Settings Page', 'wooorderexp' ).'</span>
				
									</h3>
				
									<div class="inside">';
	
							$html .= '	<table class="form-table wpd-ws-settings-box"> 
											<tbody>';

										$html .='<tr>
												<th scope="row">														
													<label><strong>'.__( 'Excel Settings', 'wooorderexp' ).'</strong></label>
												</th>
												<td>
													<input type="radio"  name="woo_order_excel_sheet_settings" '.$same_sheet.' value="1" />Same Sheet </br>
													<input type="radio"  name="woo_order_excel_sheet_settings" '.$diff_sheet.' value="2" />Different Sheet
												</td>
											 </tr>';
											 
										$html .='<tr>
												<th scope="row">														
													<label><strong>'.__( 'Title Settings For Different Sheet', 'wooorderexp' ).'</strong></label>
												</th>
												<td>
													<input type="radio"  name="woo_order_excel_diff_title_settings" '.$diff_sheet_one_title.' value="1" />One Title For All Order</br>
													<input type="radio"  name="woo_order_excel_diff_title_settings" '.$diff_sheet_all_title.' value="2" />Every Order Have Title
												</td>
											 </tr>';

										$html .='<tr>
												<th scope="row">														
													<label><strong>'.__( 'Product Export Settings', 'wooorderexp' ).'</strong></label>
													</br><span class="description">('.__( 'For Single Order Export', 'wooorderexp' ).')</span>
												</th>
												<td>
													<input type="radio"  name="woo_order_product_export_settings" '.$prod_same_sheet.' value="1" />Same Column </br>
													<input type="radio"  name="woo_order_product_export_settings" '.$prod_diff_sheet.' value="2" />Different Column
												</td>
											 </tr>';


										$html .='<tr>
													<th colspan="2"><h2>'.__( 'Order Information', 'wooorderexp' ).'</h2>
													</th>													
												 </tr>';		
										
										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="all_check" name="all_check" value="1" >
														<label><strong>'.__( 'Select All', 'wooorderexp' ).'</strong></label>
													</th>
												 </tr>';	

										$html .='<tr>
												<th scope="row">
													<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_id_enable').'  name="woo_order_id_enable" value="1" >
													<label><strong>'.__( 'Order ID', 'wooorderexp' ).'</strong></label>
												</th>
												<td>
													<input type="text"  name="woo_order_id"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_id')).'" />
												</td>
											 </tr>';			 

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_status_title_enable').'  name="woo_order_status_title_enable" value="1" >
														<label><strong>'.__( 'Order Status', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_status_title"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_status_title')).'" />
													</td>
												 </tr>';						
													
										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_date_enable').'  name="woo_order_date_enable" value="1" >
														<label><strong>'.__( 'Order Date', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_date"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_date')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_modified_date_enable').'  name="woo_order_modified_date_enable" value="1" >
														<label><strong>'.__( 'Order Modified Date', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_modified_date"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_modified_date')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_payment_method_enable').'  name="woo_order_payment_method_enable" value="1" >
														<label><strong>'.__( 'Pyment Method', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_payment_method"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_payment_method')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_payment_paypal_enable').'  name="woo_order_payment_paypal_enable" value="1" >
														<label><strong>'.__( 'Paypal Payment', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_payment_paypal"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_payment_paypal')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_shipping_charge_enable').'  name="woo_order_shipping_charge_enable" value="1" >
														<label><strong>'.__( 'Shipping Charge', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_shipping_charge"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_shipping_charge')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_cart_discount_enable').'  name="woo_order_cart_discount_enable" value="1" >
														<label><strong>'.__( 'Cart Discount', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_cart_discount"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_cart_discount')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_order_tax_enable').'  name="woo_order_order_tax_enable" value="1" >
														<label><strong>'.__( 'Order Tax', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_order_tax"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_order_tax')).'" />
													</td>
												 </tr>';
										
										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_shipping_tax_enable').'  name="woo_order_shipping_tax_enable" value="1" >
														<label><strong>'.__( 'Shipping Tax', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_shipping_tax"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_shipping_tax')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_sku_enable').'  name="woo_order_sku_enable" value="1" >
														<label><strong>'.__( 'SKU', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_sku"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_sku')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_product_name_enable').'  name="woo_order_product_name_enable" value="1" >
														<label><strong>'.__( 'Product Name', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_product_name"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_product_name')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_price_enable').'  name="woo_order_price_enable" value="1" >
														<label><strong>'.__( 'Product Price', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_price"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_price')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_quantity_enable').'  name="woo_order_quantity_enable" value="1" >
														<label><strong>'.__( 'Quantity', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_quantity"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_quantity')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_total_enable').'  name="woo_order_total_enable" value="1" >
														<label><strong>'.__( 'Total', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_total"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_total')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_fee_enable').'  name="woo_order_fee_enable" value="1" >
														<label><strong>'.__( 'Order Fee', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_fee"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_fee')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_order_total_enable').'  name="woo_order_order_total_enable" value="1" >
														<label><strong>'.__( 'Order Total', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_order_total"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_order_total')).'" />
													</td>
												 </tr>';





										$html .='<tr>
													<th colspan="2"><h2>'.__( 'Billing Information', 'wooorderexp' ).'</h2>
													</th>													
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_user_name_enable').'  name="woo_order_user_name_enable" value="1" >
														<label><strong>'.__( 'User Name', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_user_name"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_user_name')).'" />
													</td>
												 </tr>';	

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_first_name_enable').'  name="woo_order_bill_first_name_enable" value="1" >
														<label><strong>'.__( 'Billing First Name', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_first_name"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_first_name')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_last_name_enable').'  name="woo_order_bill_last_name_enable" value="1" >
														<label><strong>'.__( 'Billing Last Name', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_last_name"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_last_name')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_company_enable').'  name="woo_order_bill_company_enable" value="1" >
														<label><strong>'.__( 'Billing Company', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_company"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_company')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_address_enable').'  name="woo_order_bill_address_enable" value="1" >
														<label><strong>'.__( 'Billing Address', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_address"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_address')).'" />
													</td>
												 </tr>';


										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_second_bill_address_enable').'  name="woo_order_second_bill_address_enable" value="1" >
														<label><strong>'.__( 'Billing Second Address', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_second_bill_address"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_second_bill_address')).'" />
													</td>
												 </tr>';


										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_city_enable').'  name="woo_order_bill_city_enable" value="1" >
														<label><strong>'.__( 'Billing City', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_city"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_city')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_state_enable').'  name="woo_order_bill_state_enable" value="1" >
														<label><strong>'.__( 'Billing State', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_state"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_state')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_country_enable').'  name="woo_order_bill_country_enable" value="1" >
														<label><strong>'.__( 'Billing Country', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_country"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_country')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_post_code_enable').'  name="woo_order_bill_post_code_enable" value="1" >
														<label><strong>'.__( 'Billing Postcode', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_post_code"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_post_code')).'" />
													</td>
												 </tr>';
													
										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_phone_enable').'  name="woo_order_bill_phone_enable" value="1" >
														<label><strong>'.__( 'Billing Phone', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_phone"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_phone')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_bill_email_enable').'  name="woo_order_bill_email_enable" value="1" >
														<label><strong>'.__( 'Billing E-Mail', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_bill_email"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_bill_email')).'" />
													</td>
												 </tr>';


										$html .='<tr>
													<th colspan="2"><h2>'.__( 'Shipping Information', 'wooorderexp' ).'</h2>
													</th>													
												 </tr>';
										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_first_name_enable').'  name="woo_order_ship_first_name_enable" value="1" >
														<label><strong>'.__( 'Shipping First Name', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_first_name"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_first_name')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_last_name_enable').'  name="woo_order_ship_last_name_enable" value="1" >
														<label><strong>'.__( 'Shipping Last Name', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_last_name"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_last_name')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_company_enable').'  name="woo_order_ship_company_enable" value="1" >
														<label><strong>'.__( 'Shipping Company', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_company"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_company')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_address_enable').'  name="woo_order_ship_address_enable" value="1" >
														<label><strong>'.__( 'Shipping Address', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_address"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_address')).'" />
													</td>
												 </tr>';
										

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_city_enable').'  name="woo_order_ship_city_enable" value="1" >
														<label><strong>'.__( 'Shipping City', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_city"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_city')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_state_enable').'  name="woo_order_ship_state_enable" value="1" >
														<label><strong>'.__( 'Shipping State', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_state"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_state')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable" '.$model->checked_checkbox('woo_order_ship_country_enable').'  name="woo_order_ship_country_enable" value="1" >
														<label><strong>'.__( 'Shipping Country', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_country"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_country')).'" />
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable"  '.$model->checked_checkbox('woo_order_ship_post_code_enable').'  name="woo_order_ship_post_code_enable" value="1" >
														<label><strong>'.__( 'Shipping Postcode', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_post_code"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_post_code')).'" />
													</td>
												 </tr>';
										$html .='<tr>
													<th scope="row">
														<input type="checkbox" class="check_enable"  '.$model->checked_checkbox('woo_order_ship_method_enable').'  name="woo_order_ship_method_enable" value="1" >
														<label><strong>'.__( 'Shipping Method', 'wooorderexp' ).'</strong></label>
													</th>
													<td>
														<input type="text"  name="woo_order_ship_method"  size="39" value="'.$model->woo_order_exp_escape_attr($model->woo_order_exp_get_data('woo_order_ship_method')).'" />
													</td>
												 </tr>';

										

										$html .= '<tr>
													<td colspan="2">
														<input type="submit" class="button-primary" name="wooorderexp_settings_save" class="" value="'.__( 'Save Changes', 'wooorderexp' ).'" />
													</td>
												</tr>';
										
										
							$html .= '		</tbody>
										</table>';	
							
	$html .= '					</div><!-- .inside -->
					
							</div><!-- #settings -->
				
						</div><!-- .meta-box-sortables ui-sortable -->
				
					</div><!-- .metabox-holder -->
				
				</div><!-- #wps-settings-general -->
				
				<!-- end of the settings meta box -->';
	
	$html .= '</form>';
	
	$html .= '</div><!-- .wrap -->';
	
	echo $html;	
?>