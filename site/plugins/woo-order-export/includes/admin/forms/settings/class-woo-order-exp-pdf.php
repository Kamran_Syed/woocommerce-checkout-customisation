<?php 
 	
	$html = '';
	
	//all settings will reset as per default
	if(isset($_POST['wooorderexp_reset_settings']) && !empty($_POST['wooorderexp_reset_settings']) && $_POST['wooorderexp_reset_settings'] == __( 'Reset To Deafault', 'wooorderexp' )) { //check click of reset button
		
		update_option('woo_order_export_pdf_enable','');
		update_option('woo_order_export_pdf_shopname','');
		update_option('woo_order_export_pdf_address','');	
		update_option('woo_order_export_pdf_logo',''); // set default settingswoo_order_export_pdf_title
		update_option('woo_order_export_pdf_title','Invoice Details');
		update_option('woo_order_export_pdf_footer','');
		update_option('woo_order_export_page_enable','N');
		update_option('woo_order_export_footer_display','');
		update_option('woo_order_export_store_title','Store Details');
		update_option('woo_order_export_buyer_bill_title_enable','1');
		update_option('woo_order_export_buyer_bill_title','Bill Information');
		update_option('woo_order_export_buyer_shipp_title_enable','1');
		update_option('woo_order_export_buyer_shipp_title','Shipping Information');
		update_option('woo_order_export_pdf_logo_width','');
		update_option('woo_order_export_pdf_logo_height','');


		$html .= '<div class="updated" id="message">
					<p><strong>'.__("All Settings Reset Successfully.",'wooorderexp').'</strong></p>
				</div>';
		
	}
	
	//Save settings
	if(isset($_POST['wooorderexp_settings_save']) && !empty($_POST['wooorderexp_settings_save']) && $_POST['wooorderexp_settings_save'] == __( 'Save Changes', 'wooorderexp' )) { 
				
		update_option('woo_order_export_pdf_enable',isset($_POST['woo_order_export_pdf_enable']) ? $_POST['woo_order_export_pdf_enable'] : "");
		update_option('woo_order_export_pdf_shopname',isset($_POST['woo_order_export_pdf_shopname']) ? $_POST['woo_order_export_pdf_shopname'] : "");
		update_option('woo_order_export_pdf_address',isset($_POST['woo_order_export_pdf_address']) ? $_POST['woo_order_export_pdf_address'] : "");		
		update_option('woo_order_export_pdf_logo',isset($_POST['woo_order_export_pdf_logo']) ? $_POST['woo_order_export_pdf_logo'] : "");
		update_option('woo_order_export_pdf_title',isset($_POST['woo_order_export_pdf_title']) ? $_POST['woo_order_export_pdf_title'] : "");
		update_option('woo_order_export_pdf_footer',isset($_POST['woo_order_export_pdf_footer']) ? $_POST['woo_order_export_pdf_footer'] : "");
		update_option('woo_order_export_page_enable',isset($_POST['woo_order_export_page_enable']) ? $_POST['woo_order_export_page_enable'] : "");
		update_option('woo_order_export_footer_display',isset($_POST['woo_order_export_footer_display']) ? $_POST['woo_order_export_footer_display'] : "");
		update_option('woo_order_export_store_title',isset($_POST['woo_order_export_store_title']) ? $_POST['woo_order_export_store_title'] : "");
		update_option('woo_order_export_buyer_bill_title_enable',isset($_POST['woo_order_export_buyer_bill_title_enable']) ? $_POST['woo_order_export_buyer_bill_title_enable'] : "");
		update_option('woo_order_export_buyer_bill_title',isset($_POST['woo_order_export_buyer_bill_title']) ? $_POST['woo_order_export_buyer_bill_title'] : "");
		update_option('woo_order_export_buyer_shipp_title_enable',isset($_POST['woo_order_export_buyer_shipp_title_enable']) ? $_POST['woo_order_export_buyer_shipp_title_enable'] : "");
		update_option('woo_order_export_buyer_shipp_title',isset($_POST['woo_order_export_buyer_shipp_title']) ? $_POST['woo_order_export_buyer_shipp_title'] : "");
		update_option('woo_order_export_pdf_logo_width',isset($_POST['woo_order_export_pdf_logo_width']) ? $_POST['woo_order_export_pdf_logo_width'] : "");
		update_option('woo_order_export_pdf_logo_height',isset($_POST['woo_order_export_pdf_logo_height']) ? $_POST['woo_order_export_pdf_logo_height'] : "");

		$html .= '<div class="updated" id="message">
					<p><strong>'.__("Changes Saved Successfully.",'wooorderexp').'</strong></p>
				</div>';
		
	}

	
	$html .= '<div class="wrap">'.screen_icon('options-general');
	
	$html .= '<h2>'.__('Set Your Template Settings For PDF', 'wooorderexp').'</h2>';
	
	$html .= '<div class="wpd-ws-reset-setting">
				<form method="post" action="">
					<input type="submit" class="button-primary" name="wooorderexp_reset_settings" value="'.__( 'Reset To Deafault', 'wooorderexp' ).'" />
				</form>
			</div>';
	
	// beginning of the plugin options form
	$html .= '<form  method="post" action="" enctype="multipart/form-data">';

	echo $html;

	$woo_order_export_pdf_enable = get_option('woo_order_export_pdf_enable');

	$woo_order_export_pdf_shopname = get_option('woo_order_export_pdf_shopname');

	$woo_order_export_pdf_address = get_option('woo_order_export_pdf_address');	

	$woo_order_export_pdf_logo = get_option('woo_order_export_pdf_logo');

	$woo_order_export_pdf_title = get_option('woo_order_export_pdf_title');

	$woo_order_export_pdf_footer = get_option('woo_order_export_pdf_footer');

	$woo_order_export_page_enable = get_option('woo_order_export_page_enable');

	$woo_order_export_footer_display = get_option('woo_order_export_footer_display');

	$woo_order_export_store_title = get_option('woo_order_export_store_title');

	$woo_order_export_buyer_bill_title_enable = get_option('woo_order_export_buyer_bill_title_enable');

	$woo_order_export_buyer_bill_title = get_option('woo_order_export_buyer_bill_title');

	$woo_order_export_buyer_shipp_title_enable = get_option('woo_order_export_buyer_shipp_title_enable');

	$woo_order_export_buyer_shipp_title = get_option('woo_order_export_buyer_shipp_title');

	$woo_order_export_pdf_logo_width = get_option('woo_order_export_pdf_logo_width');

	$woo_order_export_pdf_logo_height = get_option('woo_order_export_pdf_logo_height');

	if(!empty($woo_order_export_pdf_logo)) { //check connect button image
		$show_img_connect = ' <img src="'.$woo_order_export_pdf_logo.'" alt="'.__('Store Logo','wooorderexp').'" />';
	} else {
		$show_img_connect = '';
	}

	$checked = "";$checkedf='';	$checkedship='';$checkedbill='';
		
	if(isset($woo_order_export_pdf_enable) && $woo_order_export_pdf_enable == 1){
		$checked="checked='checked'";
	}

	if(isset($woo_order_export_footer_display) && $woo_order_export_footer_display == 1){
		$checkedf="checked='checked'";
	}

	if(isset($woo_order_export_buyer_bill_title_enable) && $woo_order_export_buyer_bill_title_enable == 1){
		$checkedbill="checked='checked'";
	}
	if(isset($woo_order_export_buyer_shipp_title_enable) && $woo_order_export_buyer_shipp_title_enable == 1){
		$checkedship="checked='checked'";
	}
	$checkedl = ''; $checkedr = '';  $checkedn ='';
	if(isset($woo_order_export_page_enable) && $woo_order_export_page_enable == 'L'){
		$checkedl="checked='checked'";
	}elseif (isset($woo_order_export_page_enable) && $woo_order_export_page_enable == 'R') {
		$checkedr="checked='checked'";
	}else{
		$checkedn="checked='checked'";
	}



	$html = '<!-- beginning of the settings meta box -->

				<div id="wpd-ws-settings" class="post-box-container">
				
					<div class="metabox-holder">	
				
						<div class="meta-box-sortables ui-sortable">
				
							<div id="settings" class="postbox">	
				
												
									<!-- settings box title -->
				
									<h3 class="hndle">
				
										<span style="vertical-align: top;">'. __( 'PDF Template Settings Page', 'wooorderexp' ).'</span>
				
									</h3>
				
									<div class="inside">';
	
							$html .= '	<table class="form-table wpd-ws-settings-box"> 
											<tbody>';
								
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'PDF Title:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text"  name="woo_order_export_pdf_title" size="39" value="'.$woo_order_export_pdf_title.'"  ><br />
														<span class="description">'.__( 'Fiil Text Box For Title on PDF Page', 'wooorderexp' ).'</span>													
													</td>
												 </tr>';

										$html .='<tr>
											<th scope="row"><input type="checkbox" '.$checkedbill.'  name="woo_order_export_buyer_bill_title_enable" value="1" >
												<label><strong>'.__( 'Title For Buyer Billing:', 'wooorderexp' ).'</strong></label>
											</th>
											<td><input type="text"  name="woo_order_export_buyer_bill_title" size="39" value="'.$woo_order_export_buyer_bill_title.'"  ><br />
												<span class="description">'.__( 'Check Box,If you want to Display your own Title For Buyer Billing Details on PDF Page', 'wooorderexp' ).'</span>												
											</td>
										 </tr>';

										$html .='<tr>
													<th scope="row"><input type="checkbox" '.$checkedship.'  name="woo_order_export_buyer_shipp_title_enable" value="1" >
														<label><strong>'.__( 'Title For Buyer Shipping:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text"  name="woo_order_export_buyer_shipp_title" size="39" value="'.$woo_order_export_buyer_shipp_title.'"  ><br />
														<span class="description">'.__( 'Check Box,If you want to Display your own Title For Buyer Shipping Details on PDF Page', 'wooorderexp' ).'</span>												
													</td>
												 </tr>';	


										
										
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Display Store Information:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="checkbox" '.$checked.'  name="woo_order_export_pdf_enable" value="1" ><br />
														<span class="description">'.__( 'Check this box, if you want to print Store/Seller information on PDF page.', 'wooorderexp' ).'</span>
													</td>
												 </tr>';

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Title For Store:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text"  name="woo_order_export_store_title" size="39" value="'.$woo_order_export_store_title.'"  ><br />
														<span class="description">'.__( 'If you want to use your own Title For Store/Seller on PDF', 'wooorderexp' ).'</span>												
													</td>
												 </tr>';
													
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Store Name:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text"  name="woo_order_export_pdf_shopname"  size="39" value="'.$model->woo_order_exp_escape_attr($woo_order_export_pdf_shopname).'" /><br />
														<span class="description">'.__( 'Enter Your Store Name.', 'wooorderexp' ).'</span>
													</td>
												 </tr>';
													
										
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Store Address:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><textarea name="woo_order_export_pdf_address" size="39" >'.$model->woo_order_exp_escape_attr($woo_order_export_pdf_address).'</textarea><br />
														<span class="description">'.__( 'Enter Your Store Address.', 'wooorderexp' ).'</span>
													</td>
												 </tr>';								
										
										
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Store Logo:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text" id="woo_order_export_pdf_logo" name="woo_order_export_pdf_logo" value="'.$woo_order_export_pdf_logo.'" size="39" />
														<input type="button" class="button-secondary wooorderexp-img-uploader" name="wooorderexp_img" value="'.__( 'Upload', 'wooorderexp' ).'"><br />
														<span class="description">'.__( 'If you want to use your Store Logo, upload one here.', 'wooorderexp' ).'</span>
														<div id="wooorderexp_store_logo_preview">'.$show_img_connect.'</div>
													</td>

												 </tr>';
										
										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Image Size :', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text" name="woo_order_export_pdf_logo_width" placeholder="Width" value="'.$woo_order_export_pdf_logo_width.'" size="7" />
														&#10008; <input type="text" name="woo_order_export_pdf_logo_height" placeholder="Hieght" value="'.$woo_order_export_pdf_logo_height.'" size="7" /><br />
														<span class="description">'.__( 'Set Your Image Size In Pixel.', 'wooorderexp' ).'</span>
														
													</td>

												 </tr>';
										

										


										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Display Footer:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="checkbox" '.$checkedf.'  name="woo_order_export_footer_display" value="1" ><br />
														<span class="description">'.__( 'Check this box, if you want to print your information in Footer on PDF page.', 'wooorderexp' ).'</span>
													</td>
												 </tr>';			 

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Footer :', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="text"  name="woo_order_export_pdf_footer" size="39" value="'.$woo_order_export_pdf_footer.'"  ><br />
														<span class="description">'.__( 'If you want to use Footer on PDF', 'wooorderexp' ).'</span>
														
													
													</td>
												 </tr>';											 

										$html .='<tr>
													<th scope="row">
														<label><strong>'.__( 'Display Page No:', 'wooorderexp' ).'</strong></label>
													</th>
													<td><input type="radio" '.$checkedl.' name="woo_order_export_page_enable" value="L" >Left Side<br />
														<input type="radio" '.$checkedr.' name="woo_order_export_page_enable" value="R" >Right Side<br />
														<input type="radio" '.$checkedn.' name="woo_order_export_page_enable" value="N" >None<br />
														<span class="description">'.__( 'Select If You want display Page Number On Footer', 'wooorderexp' ).'</span>
													</td>
												 </tr>';

								
										
										$html .= '<tr>
													<td colspan="2">
														<input type="submit" class="button-primary wooorderexp-img-uploader" name="wooorderexp_settings_save" class="" value="'.__( 'Save Changes', 'wooorderexp' ).'" />
													</td>
												</tr>';
										
										
							$html .= '		</tbody>
										</table>';	
							
	$html .= '					</div><!-- .inside -->
					
							</div><!-- #settings -->
				
						</div><!-- .meta-box-sortables ui-sortable -->
				
					</div><!-- .metabox-holder -->
				
				</div><!-- #wps-settings-general -->
				
				<!-- end of the settings meta box -->';
	
	$html .= '</form>';
	
	$html .= '</div><!-- .wrap -->';
	
	echo $html;	
?>