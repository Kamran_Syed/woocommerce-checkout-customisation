<?php 
	
	$root = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
	require_once($root .'/wp-load.php');
	
	if ( current_user_can( 'manage_options' ) ) {
		global $woo_order_exp_model;
		$model = $woo_order_exp_model;
		$symbol = get_woocommerce_currency_symbol();
		$excel_sheet = get_option("woo_order_excel_sheet_settings");//Display In Settings

		$status = (isset($_GET['status'])) ? $_GET['status'] : '' ;
		$startdate = (isset($_GET['startdate'])) ? $_GET['startdate'] : '' ;
		$enddate = (isset($_GET['enddate'])) ? $_GET['enddate'] : '' ;
		$order_export_product_list = (isset($_GET['product_list'])) ? $_GET['product_list'] : '' ;
		$reg_user_list = (isset($_GET['signup_user_list']) && !empty($_GET['signup_user_list'])) ? $_GET['signup_user_list'] : '';
			
		$orders = $model->woo_order_exp_export_filter($status,$startdate,$enddate,$order_export_product_list,$reg_user_list);
		
		$data = "<?xml version='1.0'?>
				<?mso-application progid='Excel.Sheet'?>
				<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>";
				$data.="<Styles>
				  	<Style ss:ID='Default' ss:Name='Normal'>
				   	<Alignment ss:Vertical='Bottom'/>
				   	<Borders/>
				   	<Font/>
				   	<Interior/>
				   	<NumberFormat/>
				   	<Protection/>
				  	</Style>
				  	<Style ss:ID='s21'>
				   	<Alignment ss:Horizontal='Center' ss:Vertical='Bottom'/>
				   	<Font x:Family='Swiss' ss:Bold='1'/>
				    </Style>
				   	<Style ss:ID='s22'>
				   	<Alignment ss:Horizontal='Center' ss:Vertical='Bottom'/>
				   	</Style>
				 	</Styles>";

				 				$data.= "<Worksheet ss:Name='Order Information'><Table>";
										$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"120\"/>
												 <Column ss:AutoFitWidth=\"0\" ss:Width=\"130\"/>";
										$data.= "<Row>";
										$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='10'><Data ss:Type='String'>Order Information </Data></Cell>	</Row>";

										$data.= "<Row>";
										$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>	";
										 		
										 		$data.=$model->woo_order_exp_excel_get_column('woo_order_id_enable');
										 		$data.=$model->woo_order_exp_excel_get_column('woo_order_status_title_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_date_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_modified_date_enable');
										 		
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_user_name_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_first_name_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_last_name_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_company_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_address_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_second_bill_address_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_city_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_state_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_country_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_post_code_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_phone_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_bill_email_enable');
										 		
										 		
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_first_name_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_last_name_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_company_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_address_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_city_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_state_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_country_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_post_code_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_ship_method_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_payment_method_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_payment_paypal_enable');
										 		
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_shipping_charge_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_cart_discount_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_order_tax_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_shipping_tax_enable');
										 		$data.= $model->woo_order_exp_excel_get_column('woo_order_order_total_enable');
										 		
												$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>Card Message</Data></Cell>";
												$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>Required Delivery Date</Data></Cell>";
										 		
										 		//display / export in same sheet
												if ($excel_sheet == 1) {
													$data.= $model->woo_order_exp_excel_get_column('woo_order_product_name_enable');
													$data.= $model->woo_order_exp_excel_get_column('woo_order_sku_enable');
													$data.= $model->woo_order_exp_excel_get_column('woo_order_price_enable');
													$data.= $model->woo_order_exp_excel_get_column('woo_order_quantity_enable');
													$data.= $model->woo_order_exp_excel_get_column('woo_order_total_enable');
													$data.= $model->woo_order_exp_excel_get_column('woo_order_fee_enable');
													$data.= $model->woo_order_exp_excel_get_column('woo_order_order_total_enable');
													
												}//display / export in same sheet

										
										$data .= "</Row>";
										$counter = 1;
										
										foreach ($orders as $key => $value) {
											$meta_values = get_post_meta( $value );	
											$order = new WC_Order($value);											
											$data .= "<Row>";											
												$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='Number'>".$counter."</Data></Cell>";
												
												$data.= (get_option('woo_order_id_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->id."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_status_title_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->status."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_date_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".date('Y-m-d' , strtotime($order->order_date) )."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_modified_date_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".date('Y-m-d' , strtotime($order->modified_date) )."</Data></Cell>" : '' ;
												
												$customer_user = get_post_meta( $value, '_customer_user', true );
												if (!empty($customer_user) && $customer_user != 0 ) {
													$author_data     = get_user_by( 'id', $customer_user );
													$user_name = $author_data->user_login;																										
												}else{
													$user_name = '';
												}
													
												$data.= (get_option('woo_order_user_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$user_name."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_first_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_first_name'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_last_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_last_name'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_company_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_company'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_address_1'][0]."</Data></Cell>" : '' ; 
												$data.= (get_option('woo_order_second_bill_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_address_2'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_city_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_city'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_state_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_state'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_country_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".WC()->countries->countries[$meta_values['_billing_country'][0]]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_post_code_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_postcode'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_phone_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_phone'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_bill_email_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_email'][0]."</Data></Cell>" : '' ;

												$data.= (get_option('woo_order_ship_first_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_first_name'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_last_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_last_name'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_company_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_company'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_address_1'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_city_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_city'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_state_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_state'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_country_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".WC()->countries->countries[$meta_values['_shipping_country'][0]]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_ship_post_code_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_postcode'][0]."</Data></Cell>" : '' ;
												$shipping_method = $order->get_shipping_methods();
												if (!empty($shipping_method)) {
													foreach ($shipping_method as $s_key => $s_value) {
														$ship_method = $s_value['name'];
													}
												}
												$ship_method = (!empty($ship_method)) ? $ship_method : '';
												$data.= (get_option('woo_order_ship_method_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$ship_method."</Data></Cell>" : '' ;

												
												
												$data.= (get_option('woo_order_payment_method_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_payment_method'][0]."</Data></Cell>" : '' ;
												if (isset($meta_values['_paypal_email'][0])) {
													$PayPal_Id = (!empty($meta_values['_paypal_email'][0])) ? $meta_values['_paypal_email'][0] : '';
												}else $PayPal_Id = '';
												
												
												$data.= (get_option('woo_order_payment_paypal_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$PayPal_Id."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_shipping_charge_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_shipping'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_cart_discount_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_cart_discount'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_order_tax_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_tax'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_shipping_tax_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_shipping_tax'][0]."</Data></Cell>" : '' ;
												$data.= (get_option('woo_order_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_total'][0]."</Data></Cell>" : '' ;
												
												$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_personal_message'][0]."</Data></Cell>";
												$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_required_delivery_date'][0]."</Data></Cell>";
												
												//display / export in same sheet											
												
												if ($excel_sheet == 1) {
													
													$data.= $model->woo_order_exp_excel_get_data_sheet($value);

												}	//display / export in same sheet

										
		

											$data.= "</Row>";
											$counter++;
										}

									$data .= "</Table></Worksheet>";


									//display / export Different Sheet	
									if ($excel_sheet == 2) {

											$data.= "<Worksheet ss:Name='Order Details' ><Table>";
											$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"120\"/>
													 <Column ss:AutoFitWidth=\"0\" ss:Width=\"130\"/>";
											$data.= "<Row>";
											$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='5'><Data ss:Type='String'>Order Details</Data></Cell>	</Row>";
											
											//One title for all record start
											$diff_title_settings = get_option("woo_order_excel_diff_title_settings");	
											if ($diff_title_settings == 1) {
												$data.= "<Row>";
												$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>";
												$data.= $model->woo_order_exp_excel_get_column('woo_order_id_enable');
												$data.= $model->woo_order_exp_excel_get_column('woo_order_product_name_enable');
												$data.= $model->woo_order_exp_excel_get_column('woo_order_sku_enable');
												$data.= $model->woo_order_exp_excel_get_column('woo_order_price_enable');
												$data.= $model->woo_order_exp_excel_get_column('woo_order_quantity_enable');												
													
												$id_skip = (get_option('woo_order_id_enable')==1) ? 1 : 0;
												$pn_skip = (get_option('woo_order_product_name_enable')==1) ? 1 : 0;
												$sku_skip = (get_option('woo_order_sku_enable')==1) ? 1 : 0;
												$pr_skip = (get_option('woo_order_price_enable')==1) ? 1 : 0;
												$qty_skip = (get_option('woo_order_quantity_enable')==1) ? 1 : 0;
												$skip = $id_skip + $pn_skip + $sku_skip + $pr_skip + $qty_skip;
												
												$order = new WC_Order($value);
	
												$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));			
													
												foreach ( $items as $key => $item ) {	
													$count_att = 0;
													$product =get_product($item['product_id']);											 
													$attributes = $product->get_attributes();
													if (!empty($attributes)) {
														
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$key."</Data></Cell>";
															$count_att++;
														}
													}										
												}		
												$data.= $model->woo_order_exp_excel_get_column('woo_order_total_enable');													
												$data .= "</Row>";
												
													
											}//One title for all record end




											$counter = 1;
											foreach ($orders as $key => $value) {
											
												$order = new WC_Order($value);
												$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));			
												$grand_total = 0;
												
												//Every order have title Start
												if ($diff_title_settings == 2) {
														$data.= "<Row>";
														$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>";
														$data.= $model->woo_order_exp_excel_get_column('woo_order_id_enable');
														$data.= $model->woo_order_exp_excel_get_column('woo_order_product_name_enable');
														$data.= $model->woo_order_exp_excel_get_column('woo_order_sku_enable');
														$data.= $model->woo_order_exp_excel_get_column('woo_order_price_enable');
														$data.= $model->woo_order_exp_excel_get_column('woo_order_quantity_enable');			
														
														$id_skip = (get_option('woo_order_id_enable')==1) ? 1 : 0;
														$pn_skip = (get_option('woo_order_product_name_enable')==1) ? 1 : 0;
														$sku_skip = (get_option('woo_order_sku_enable')==1) ? 1 : 0;
														$pr_skip = (get_option('woo_order_price_enable')==1) ? 1 : 0;
														$qty_skip = (get_option('woo_order_quantity_enable')==1) ? 1 : 0;
														$skip = $id_skip + $pn_skip + $sku_skip + $pr_skip + $qty_skip;

														$order = new WC_Order($value);			
														$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));			
															
														foreach ( $items as $key => $item ) {	
															$count_att = 0;
															$product =get_product($item['product_id']);											 
															$attributes = $product->get_attributes();
															if (!empty($attributes)) {
																
																foreach ($attributes as $key => $value) {														
																	$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$key."</Data></Cell>";
																	$count_att++;
																}
															}										
														}			
														$data.= $model->woo_order_exp_excel_get_column('woo_order_total_enable');	
														$data .= "</Row>";														
												}//Every order have title end
												

												//data start export sigle order
												foreach ( $items as $item ) {											
													
												    $product_name = $item['name'];
												    $product_info = $item['item_meta'];
												    $total=$product_info['_line_total'][0];
												    $product =get_product($item['product_id']);	
			    									$sku = $product->get_sku();
			    									$SKU = ( !empty($sku) ) ? $sku : '';
			    									
												    $type = $item['type'];
												    if ($type == 'line_item') {
												    	$quantity=$product_info['_qty'][0];
												    	$price = $total / $quantity;
												    	$data .= "<Row>"; 
													    $data.= "<Cell  ss:StyleID='s22'><Data ss:Type='Number'>".$counter."</Data></Cell>";
														$data.= (get_option('woo_order_id_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->id."</Data></Cell>" : '' ;
														$data.= (get_option('woo_order_product_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$product_name."</Data></Cell>" : '' ;
														$data.= (get_option('woo_order_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$SKU."</Data></Cell>" : '' ;
														$data.= (get_option('woo_order_price_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>" : '' ;
														$data.= (get_option('woo_order_quantity_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$quantity."</Data></Cell>" : '' ;

														$product =get_product($item['product_id']);											 
														$attributes = $product->get_attributes();
														if (!empty($attributes)) {
															foreach ($attributes as $key => $value) {														
																$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$value['value']."</Data></Cell>";
															}
														}else{
															for ($i=0; $i < $count_att; $i++) { 
																$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'></Data></Cell>";
																
															}
														}	
														$data.= (get_option('woo_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$total."</Data></Cell>" : '' ;
														
														$data.= "<Cell  ss:StyleID='s22'></Cell>
																</Row>";  

												    }else{ //Order Fee

												    	$price = $total;
														$merge = $count_att + $skip;
												    	$fee_enable = get_option('woo_order_fee_enable');
												    	if ($fee_enable == 1) {
												    		$data .= "<Row >"; 													    
															$data.= "<Cell  ss:StyleID='s22' ss:MergeAcross='".$merge."'><Data ss:Type='String'>".$product_name."</Data></Cell>																										
															<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>
															</Row>"; 
												    	}									    	

												    }//Order Fee End
												    
												    $grand_total =  $grand_total + $total;
													if (end($item) ) {	}
												    
												}//data end export sigle order
													
													$counter++;
													$merge = $count_att + $skip;
													
													$order_total_enable = get_option('woo_order_order_total_enable');
													if ($order_total_enable == 1) {
														$data .= "<Row>";
														$data.= "<Cell  ss:StyleID='s21' ss:MergeAcross='".$merge."'><Data ss:Type='String'>".get_option('woo_order_order_total')."</Data></Cell>																										
														<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$symbol.$grand_total."</Data></Cell>
														</Row>";
													}
															
													$blank_row =  $count_att + $skip + 1;
													$data .= "<Row><Cell  ss:StyleID='s21' ss:MergeAcross='".$blank_row."'><Data ss:Type='String'></Data></Cell></Row>"; 
											}		
														
													

											$data .= "</Table></Worksheet>";		

									}//display / export Different Sheet	

										

						$data.="</Workbook>";	
					
	header("Content-type: application/octet-stream");

	header("Content-Disposition: attachment; filename=".$status."-order.xls;");

	header("Content-Type: application/ms-excel");

	header("Pragma: no-cache");

	header("Expires: 0");
		
	echo $data; 	
}else{
	$my_error = new WP_Error( 'error', 'Please login with administrator role.', 'my best' );
	$error = $my_error->get_error_messages();
	echo $error[0];exit;
	
}
?>