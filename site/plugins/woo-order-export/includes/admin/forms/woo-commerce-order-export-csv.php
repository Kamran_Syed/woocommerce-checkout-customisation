<?php 
	$root = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
	require_once($root .'/wp-load.php');

	if ( current_user_can( 'manage_options' ) ) {

		global $woo_order_exp_model;
		$model = $woo_order_exp_model;			
		$symbol = $model -> woo_order_exp_currency_symbol();
		$symbol = utf8_decode($symbol);		
		$status = (isset($_GET['status'])) ? $_GET['status'] : '' ;
		$startdate = (isset($_GET['startdate'])) ? $_GET['startdate'] : '' ;
		$enddate = (isset($_GET['enddate'])) ? $_GET['enddate'] : '' ;
		$order_export_product_list = (isset($_GET['product_list'])) ? $_GET['product_list'] : '' ;
		$reg_user_list = (isset($_GET['signup_user_list']) && !empty($_GET['signup_user_list'])) ? $_GET['signup_user_list'] : '';
		$orders = $model->woo_order_exp_export_filter($status,$startdate,$enddate,$order_export_product_list,$reg_user_list);

	    header("Content-type: text/csv; charset=utf-8; encoding=utf-8 " );
		header("Content-Disposition: attachment; filename=".$status."-order.csv");
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
		
		$data_ary = ' No ,';

	
		 		$data_ary.=$model->woo_order_exp_csv_get_column('woo_order_id_enable');
		 		$data_ary.=$model->woo_order_exp_csv_get_column('woo_order_status_title_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_date_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_modified_date_enable');

 				$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_user_name_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_first_name_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_last_name_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_company_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_address_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_second_bill_address_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_city_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_state_enable'); 		
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_country_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_post_code_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_phone_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_bill_email_enable');
		 		
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_first_name_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_last_name_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_company_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_address_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_city_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_state_enable');	 		
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_country_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_post_code_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_ship_method_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_payment_method_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_payment_paypal_enable');

		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_shipping_charge_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_cart_discount_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_tax_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_shipping_tax_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_total_enable');
		 		
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_product_name_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_sku_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_price_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_quantity_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_total_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_fee_enable');
		 		$data_ary.= $model->woo_order_exp_csv_get_column('woo_order_order_total_enable');
		 		$data_ary.= 'Card Message,';
		 		$data_ary.= 'Req Delivery Date';
		 		

		$data_array[] = explode(',', $data_ary);	
		
		outputCSV( $data_array );



		$counter = 1;
		foreach ($orders as $key => $value) {

			$data = '';
			$get_data_array = '';
			$meta_values = get_post_meta( $value );	
			$order = new WC_Order($value);
		
					$data .= $counter." ,";					
					$data.= (get_option('woo_order_id_enable') == 1 ) ? $order->id." ," : '' ;
					$data.= (get_option('woo_order_status_title_enable') == 1 ) ? $order->status." ," : '' ;
					$data.= (get_option('woo_order_date_enable') == 1 ) ? date('Y-m-d' , strtotime($order->order_date) )." ," : '' ;
					$data.= (get_option('woo_order_modified_date_enable') == 1 ) ? date('Y-m-d' , strtotime($order->modified_date) )." ," : '' ;
					
					$customer_user = get_post_meta( $value, '_customer_user', true );
					if (!empty($customer_user) && $customer_user != 0 ) {
						$author_data     = get_user_by( 'id', $customer_user );
						$user_name = $author_data->user_login;																										
					}else{
						$user_name = '';
					}
						
					$data.= (get_option('woo_order_user_name_enable') == 1 ) ? $user_name." ," : '' ;
					$data.= (get_option('woo_order_bill_first_name_enable') == 1 ) ? $meta_values['_billing_first_name'][0]." ," : '' ;
					$data.= (get_option('woo_order_bill_last_name_enable') == 1 ) ? $meta_values['_billing_last_name'][0]." ," : '' ;
					$data.= (get_option('woo_order_bill_company_enable') == 1 ) ? $meta_values['_billing_company'][0]." ," : '' ;
					$search =  ',' ;
					$search = str_split($search);
					$bill_first_address =str_replace($search, "", $meta_values['_billing_address_1'][0]);
					$bill_second_address =str_replace($search, "", $meta_values['_billing_address_2'][0]);		
					$data.= (get_option('woo_order_bill_address_enable') == 1 ) ? $bill_first_address." ," : '' ; 
					$data.= (get_option('woo_order_second_bill_address_enable') == 1 ) ? $bill_second_address." ," : '' ;
					$data.= (get_option('woo_order_bill_city_enable') == 1 ) ? $meta_values['_billing_city'][0]." ," : '' ;
					$data.= (get_option('woo_order_bill_state_enable') == 1 ) ? $meta_values['_billing_state'][0]." ," : '' ;
					$data.= (get_option('woo_order_bill_country_enable') == 1 ) ? WC()->countries->countries[$meta_values['_billing_country'][0]]." ," : '' ;
					$data.= (get_option('woo_order_bill_post_code_enable') == 1 ) ? $meta_values['_billing_postcode'][0]." ," : '' ;
					$data.= (get_option('woo_order_bill_phone_enable') == 1 ) ? $meta_values['_billing_phone'][0]." ," : '' ;
					$data.= (get_option('woo_order_bill_email_enable') == 1 ) ? $meta_values['_billing_email'][0]." ," : '' ;

					$data.= (get_option('woo_order_ship_first_name_enable') == 1 ) ? $meta_values['_shipping_first_name'][0]." ," : '' ;
					$data.= (get_option('woo_order_ship_last_name_enable') == 1 ) ? $meta_values['_shipping_last_name'][0]." ," : '' ;
					$data.= (get_option('woo_order_ship_company_enable') == 1 ) ? $meta_values['_shipping_company'][0]." ," : '' ;
					$ship_first_address =str_replace($search, "", $meta_values['_shipping_address_1'][0]);
					$data.= (get_option('woo_order_ship_address_enable') == 1 ) ? $ship_first_address." ," : '' ;
					$data.= (get_option('woo_order_ship_city_enable') == 1 ) ? $meta_values['_shipping_city'][0]." ," : '' ;
					$data.= (get_option('woo_order_ship_state_enable') == 1 ) ? $meta_values['_shipping_state'][0]." ," : '' ;
					$data.= (get_option('woo_order_ship_country_enable') == 1 ) ? WC()->countries->countries[$meta_values['_shipping_country'][0]]." ," : '' ;
					$data.= (get_option('woo_order_ship_post_code_enable') == 1 ) ? $meta_values['_shipping_postcode'][0]." ," : '' ;
					
					$shipping_method = $order->get_shipping_methods();
					if (!empty($shipping_method)) {
						foreach ($shipping_method as $s_key => $s_value) {
							$ship_method = $s_value['name'];
						}
					}
					$ship_method = (!empty($ship_method)) ? $ship_method : '';

					$data.= (get_option('woo_order_ship_method_enable') == 1 ) ? $ship_method." ," : '' ;
					$data.= (get_option('woo_order_payment_method_enable') == 1 ) ? $meta_values['_payment_method'][0]." ," : '' ;

					if (isset($meta_values['_paypal_email'][0])) {
						$PayPal_Id = (!empty($meta_values['_paypal_email'][0])) ? $meta_values['_paypal_email'][0].',' : ',';
					}else $PayPal_Id = ',';
					
					$data.= (get_option('woo_order_payment_paypal_enable') == 1 ) ? $PayPal_Id : '' ;
					$data.= (get_option('woo_order_shipping_charge_enable') == 1 ) ? $symbol.$meta_values['_order_shipping'][0]." ," : '' ;
					$data.= (get_option('woo_order_cart_discount_enable') == 1 ) ? $symbol.$meta_values['_cart_discount'][0]." ," : '' ;
					$data.= (get_option('woo_order_order_tax_enable') == 1 ) ? $symbol.$meta_values['_order_tax'][0]." ," : '' ;
					$data.= (get_option('woo_order_shipping_tax_enable') == 1 ) ? $symbol.$meta_values['_order_shipping_tax'][0]." ," : '' ;
					$data.= (get_option('woo_order_order_total_enable') == 1 ) ? $symbol.$meta_values['_order_total'][0]." ," : '' ;

					$data.= $meta_values['_shipping_personal_message'][0]." ,";
					$data.= $meta_values['_required_delivery_date'][0]." ,";

			$data.= $model->woo_order_exp_csv_get_data_sheet_multi_order($value);

			

			$get_data_array[] = explode(',', $data);	
		
			outputCSV( $get_data_array );

	    	$counter++;
		}
		


	
}else {
	$my_error = new WP_Error( 'error', 'Please login with administrator role.', 'my best' );
	$error = $my_error->get_error_messages();
	echo $error[0];exit;
}
function outputCSV($data) {
	    $output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}
?>