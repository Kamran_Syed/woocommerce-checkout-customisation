<?php 

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Model Class
 *
 * Handles generic plugin functionality.
 *
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */
class Woo_Order_Exp_Model {
	
	public function __construct() {
	
	}
	
	/**
	 * Escape Tags & Slashes
	 *
	 * Handles escapping the slashes and tags
	 *
	 * @package  WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_escape_attr($data){
		return esc_attr(stripslashes($data));
	}
	
	/**
	 * Strip Slashes From Array
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_escape_slashes_deep($data = array(),$flag=false){
			
		if($flag != true) {
			$data = $this->woo_order_exp_nohtml_kses($data);
		}
		$data = stripslashes_deep($data);
		return $data;
	}
	
	/**
	 * Strip Html Tags 
	 * 
	 * It will sanitize text input (strip html tags, and escape characters)
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	public function woo_order_exp_nohtml_kses($data = array()) {
		
		if ( is_array($data) ) {
			
			$data = array_map(array($this,'woo_order_exp_nohtml_kses'), $data);
			
		} elseif ( is_string( $data ) ) {
			
			$data = wp_filter_nohtml_kses($data);
		}
		
		return $data;
	}	
	
	/**
	 * Convert Object To Array
	 *
	 * Converting Object Type Data To Array Type
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	public function woo_order_exp_object_to_array($result)
	{
	    $array = array();
	    foreach ($result as $key=>$value)
	    {	
	        if (is_object($value))
	        {
	            $array[$key]=$this->woo_order_exp_object_to_array($value);
	        } else {
	        	$array[$key]=$value;
	        }
	    }
	    return $array;
	}	


	
	
	/**
	 *
	 * export data settings in Excell or csv for checkbox
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	public function checked_checkbox( $field )
	{
			$field_check=get_option($field);
			$checked = '';
			if(isset($field_check) && $field_check == 1){
				$checked="checked='checked'";
			}

	    	return $checked;	
	}	

	/**
	 *
	 * Get data from database for Excell or csv 
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	public function woo_order_exp_get_data( $field )
	{
			$field_check=get_option($field);
	    	return $field_check;	
	}	

	/**
	 *
	 * Update database  for Excell or csv settings
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	function woo_order_exp_excel_save_settings($field){		
		foreach ($field as $key => $value) {
			update_option($key,$value);
		}
	}

	/**
	 *
	 * Get Title For Excel for Excell 
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	 
	 function before ($thiss, $inthat)
    {
        return substr($inthat, 0, strpos($inthat, $thiss));
    }
	
	function woo_order_exp_excel_get_column($field){
			$field_check=get_option($field);
			$title_field = $this->before ('_enable', $field);				
			if ($field_check == 1) {				
				$data = "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".get_option($title_field)."</Data></Cell>";
			}else{
				$data ="";
			}
					
	    	return $data;	
	}

	/**
	 *
	 * Get Title For Excel for Excell 
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	function woo_order_exp_csv_get_column($field){
			$field_check=get_option($field);
			$title_field = $this->before ('_enable', $field);				
			if ($field_check == 1) {				
				$data = get_option($title_field). ',' ;
			}else{
				$data ="";
			}
					
	    	return $data;	
	}
	


	/**
	 *
	 * Get data in one excel sheet
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	function woo_order_exp_excel_get_data_sheet($value){
			
			$symbol = get_woocommerce_currency_symbol();

			$data='';

			$order = new WC_Order($value);

			$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));			

			$grand_total = 0; $product_array = ''; 	$price_array = '';
			$quantity_array = ''; $total_array = ''; $fee_array = '';

			foreach ( $items as $item ) {

			    $product_name = $item['name'];

			    $product_info = $item['item_meta'];

			    $total=$product_info['_line_total'][0];

			    $quantity=$product_info['_qty'][0];

			    $product =get_product($item['product_id']);	
			    $sku = $product->get_sku();
			    $SKU[] = $sku;
			    $type = $item['type'];
			    if ($type == 'line_item') {
			    		
			    	$price = $total / $quantity;

			    	$product_array[] = $product_name;

					$price_array[] = $symbol.$price;

					$quantity_array[] = $quantity;

					$total_array[] = $symbol.$total;


			    }else{

			    	$fee_array[] = $product_name." - ".$symbol.$total;

			    }	 

			    $grand_total =  $grand_total + $total;			    
			   
			    
				if (end($item) ) {}

			    
			}
			$SKU = implode(",", $SKU);
			$SKU = ( !empty($SKU) ) ? $SKU : '';
			$product_name = implode(",", $product_array);

			$price = implode(",", $price_array);

			$quantity = implode(",", $quantity_array);

			$total = implode(",", $total_array);
			

			$fee = (!empty($fee_array)) ? implode(",",$fee_array ) : 0 ;
				
			$data.= (get_option('woo_order_product_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$product_name."</Data></Cell>" : '' ;
			$data.= (get_option('woo_order_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$SKU."</Data></Cell>" : '' ;
			$data.= (get_option('woo_order_price_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$price."</Data></Cell>" : '' ;
			$data.= (get_option('woo_order_quantity_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$quantity."</Data></Cell>" : '' ;
			$data.= (get_option('woo_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$total."</Data></Cell>" : '' ;
			$data.= (get_option('woo_order_fee_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$fee."</Data></Cell>" : '' ;
			$data.= (get_option('woo_order_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$grand_total."</Data></Cell>" : '' ;
		
    	return $data;	
	}

	/**
	 *
	 * Get data in one CSV sheet in single order
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	function woo_order_exp_csv_get_data_sheet($value){
		
		$data='';
		
		$symbol = $this -> woo_order_exp_currency_symbol();

		$symbol = utf8_decode($symbol);

		$order = new WC_Order($value);

		$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
		$grand_total = 0;	
		$product_export = get_option("woo_order_product_export_settings");
	 	if ($product_export == 1) {

			 $product_array = ''; 	$price_array = '';
			$quantity_array = ''; $total_array = ''; $fee_array = '';

			foreach ( $items as $item ) {

			    $product_name = $item['name'];

			    $product_info = $item['item_meta'];

			    $total=$product_info['_line_total'][0];

			    $product =get_product($item['product_id']);	
			    $sku = $product->get_sku();
			    $sku = (!empty($sku)) ? $sku : '-';
			    $SKU[] = $sku;

			    $type = $item['type'];
			    if ($type == 'line_item') {
			    	
			    	$quantity=$product_info['_qty'][0];

			    	$price = $total / $quantity;

			    	$product_array[] = $product_name;

					$price_array[] = $symbol.$price;

					$quantity_array[] = $quantity;

					$total_array[] = $symbol.$total;


			    }else{

			    	$fee_array[] = $product_name." - ".$symbol.$total;

			    }	 

			    $grand_total =  $grand_total + $total;			    
			   
			    
				if (end($item) ) {}

			    
			}
			$SKU = implode("|", $SKU);
			$SKU = ( !empty($SKU) ) ? $SKU : '';

			$product_name = implode("|", $product_array);

			$price = implode("|", $price_array);

			$quantity = implode("|", $quantity_array);

			$total = implode("|", $total_array);
			
			
			$fee = (!empty($fee_array)) ? implode(',',$fee_array ) : $symbol.'0' ;

			$data.= (get_option('woo_order_product_name_enable') == 1 ) ? $product_name." ," : '' ;
			$data.= (get_option('woo_order_sku_enable') == 1 ) ? $SKU." ," : '' ;
			$data.= (get_option('woo_order_price_enable') == 1 ) ? $price." ," : '' ;
			$data.= (get_option('woo_order_quantity_enable') == 1 ) ? $quantity." ," : '' ;
			$data.= (get_option('woo_order_total_enable') == 1 ) ? $total." ," : '' ;
			$data.= (get_option('woo_order_fee_enable') == 1 ) ? $fee." ," : '' ;
			$data.= (get_option('woo_order_order_total_enable') == 1 ) ? $symbol.$grand_total." ," : '' ;
		}else{	
		
			foreach ( $items as $key => $item ) {	

				$prod_list[] =  $item['product_id'];
				$product =get_product($item['product_id']);
				 
				$attributes = $product->get_attributes();

				$product_name = $item['name'];

				$product =get_product($item['product_id']);	
			    $sku = $product->get_sku();
			    $SKU = (!empty($sku)) ? $sku : '-';			   
				
				$product_info = $item['item_meta'];

				$total=$product_info['_line_total'][0];

				$quantity=$product_info['_qty'][0];

				$price = $total / $quantity;

				$data.= (get_option('woo_order_product_name_enable') == 1 ) ? "Quantity :-".$quantity." ," : '' ;
				$data.= (get_option('woo_order_sku_enable') == 1 ) ?  $SKU." ," : '' ;
				$data.= (get_option('woo_order_price_enable') == 1 ) ? $symbol.$price." ," : '' ;

				if (!empty($attributes)) {
					foreach ($attributes as $key => $value) {														
						$data.= $value['value'].",";
					}
				}


				$data.= (get_option('woo_order_total_enable') == 1 ) ? $symbol.$total." ," : '' ;
				
				$grand_total =  $grand_total + $total;
			}	
			$type = $item['type'];
			if ($type != 'line_item') {
				$data.= $symbol.$total.",";
			}
		
			$data.= (get_option('woo_order_order_total_enable') == 1 ) ? $symbol.$grand_total." ," : '' ;

		}	
			
					
    	return $data;	
	}
	
	
	/**
	 *
	 * Get data in one CSV sheet in Multiple order
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	function woo_order_exp_csv_get_data_sheet_multi_order($value){
		
		$data='';
		
		$symbol = $this -> woo_order_exp_currency_symbol();

		$symbol = utf8_decode($symbol);

		$order = new WC_Order($value);

		$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
		$grand_total = 0;	
		

			$product_array = ''; 	$price_array = '';
			$quantity_array = ''; $total_array = ''; $fee_array = '';

			foreach ( $items as $item ) {

			    $product_name = $item['name'];

			    $product_info = $item['item_meta'];

			    $total=$product_info['_line_total'][0];

			    $product =get_product($item['product_id']);	
			    $sku = $product->get_sku();
			    $sku = (!empty($sku)) ? $sku : '-';
			    $SKU[] = $sku;

			    $type = $item['type'];
			    if ($type == 'line_item') {
			    	
			    	$quantity=$product_info['_qty'][0];

			    	$price = $total / $quantity;

			    	$product_array[] = $product_name;

					$price_array[] = $symbol.$price;

					$quantity_array[] = $quantity;

					$total_array[] = $symbol.$total;


			    }else{

			    	$fee_array[] = $product_name." - ".$symbol.$total;

			    }	 

			    $grand_total =  $grand_total + $total;			    
			   
			    
				if (end($item) ) {}

			    
			}

			$SKU = implode("|", $SKU);
			$SKU = ( !empty($SKU) ) ? $SKU : '';

			$product_name = implode("|", $product_array);

			

			$price = implode("|", $price_array);

			$quantity = implode("|", $quantity_array);

			$total = implode("|", $total_array);
			
			
			$fee = (!empty($fee_array)) ? implode(',',$fee_array ) : $symbol.'0' ;

			
			$data.= (get_option('woo_order_product_name_enable') == 1 ) ? $product_name." ," : '' ;
			$data.= (get_option('woo_order_sku_enable') == 1 ) ? $SKU." ," : '' ;
			$data.= (get_option('woo_order_price_enable') == 1 ) ? $price." ," : '' ;
			$data.= (get_option('woo_order_quantity_enable') == 1 ) ? $quantity." ," : '' ;
			$data.= (get_option('woo_order_total_enable') == 1 ) ? $total." ," : '' ;
			$data.= (get_option('woo_order_fee_enable') == 1 ) ? $fee." ," : '' ;
			$data.= (get_option('woo_order_order_total_enable') == 1 ) ? $symbol.$grand_total." ," : '' ;
					
    	return $data;	
	}
	
	/**
	 *
	 * Get Currency Symbol
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	function woo_order_exp_currency_symbol( ) {
		
		$currency = get_woocommerce_currency();
		
	
		switch ( $currency ) {
			case 'BRL' :
				$currency_symbol = 'R$';
				break;
			case 'BGN' :
				$currency_symbol = 'лв.';
				break;
			case 'AUD' :
			case 'CAD' :
			case 'CLP' :
			case 'MXN' :
			case 'NZD' :
			case 'HKD' :
			case 'SGD' :
			case 'USD' :
				$currency_symbol = '$';
				break;
			case 'EUR' :
				$currency_symbol = '€';
				break;
			case 'CNY' :
			case 'RMB' :
			case 'JPY' :
				$currency_symbol = '¥';
				break;
			case 'RUB' :
				$currency_symbol = 'руб.';
				break;
			case 'KRW' : $currency_symbol = '₩'; break;
			case 'TRY' : $currency_symbol = 'TL'; break;
			case 'NOK' : $currency_symbol = 'kr'; break;
			case 'ZAR' : $currency_symbol = 'R'; break;
			case 'CZK' : $currency_symbol = 'Kč'; break;
			case 'MYR' : $currency_symbol = 'RM'; break;
			case 'DKK' : $currency_symbol = 'kr'; break;
			case 'HUF' : $currency_symbol = 'Ft'; break;
			case 'IDR' : $currency_symbol = 'Rp'; break;
			case 'INR' : $currency_symbol = 'Rs.'; break;
			case 'ISK' : $currency_symbol = 'Kr.'; break;
			case 'ILS' : $currency_symbol = '₪'; break;
			case 'PHP' : $currency_symbol = '₱'; break;
			case 'PLN' : $currency_symbol = 'zł'; break;
			case 'SEK' : $currency_symbol = 'kr'; break;
			case 'CHF' : $currency_symbol = 'CHF'; break;
			case 'TWD' : $currency_symbol = 'NT$'; break;
			case 'THB' : $currency_symbol = '฿'; break;
			case 'GBP' : $currency_symbol = '£'; break;
			case 'RON' : $currency_symbol = 'lei'; break;
			case 'VND' : $currency_symbol = '₫'; break;
			case 'NGN' : $currency_symbol = '₦'; break;
			default    : $currency_symbol = ''; break;
		}
	
		return  $currency_symbol;
	}


	/**
	 *
	 * Export Filter for data export
	 * 
	 * @package WooCommerce - Order Export
	 * @since 2.1.2
	 * 
	 */
	public function woo_order_exp_export_filter($status,$startdate,$enddate,$order_export_product_list,$reg_user_list)
	{	
		global $woo_order_exp_model;
		$model = $woo_order_exp_model;	

		if (isset($order_export_product_list) && $order_export_product_list == "all") {
			$order_export_product_list_desable = 1;
		}else{
			$order_export_product_list = explode(',', $order_export_product_list);
			$order_export_product_list_desable =0;
		}

		$order_export_product_cat_list = (isset($_GET['product_cat_list'])) ? $_GET['product_cat_list'] : '' ;
		
		if (isset($order_export_product_cat_list) && $order_export_product_cat_list == "all") {
			$order_export_product_cat_list_desable = 1;
		}else{
			$order_export_product_cat_list = explode(',', $order_export_product_cat_list);
			$order_export_product_cat_list_desable =0;
		}


		$order_export_product_seller_list = (isset($_GET['product_seller_list'])) ? $_GET['product_seller_list'] : '' ;
		
		if (isset($order_export_product_seller_list) && $order_export_product_seller_list == "all") {
			$order_export_product_seller_list_desable = 1;
		}else{
			$order_export_product_seller_list = explode(',', $order_export_product_seller_list);
			$order_export_product_seller_list_desable =0;
		}

		
		

			$orders=array();
			
		    if ($status == 'all') {
			 	$args = array(
					        'numberposts'     => -1,
					        'meta_key'        => '_customer_user',
					        'post_type'       => 'shop_order',
					        'post_status'     => array_keys( wc_get_order_statuses() ),
					        'date_query' => array(
											        'after' =>  $startdate,
											        'before' => $enddate
											    ),
					        'suppress_filters' => false,   
							);
			 			
		    }else{
		    	$args = array(
					        'numberposts'     => -1,
					        'meta_key'        => '_customer_user',
					        'post_type'       => 'shop_order',
					        'post_status'     => $status,

					        'date_query' => array(
											        'after' =>  $startdate,
											        'before' => $enddate
											    ),
					        'suppress_filters' => false, 
					        
					       
		    				);
		    } 			
			//seller vise 
		    if (isset($order_export_product_seller_list_desable) && $order_export_product_seller_list_desable == 0) {
		    	$posts = array();
		    	$new_posts=get_posts($args);	    	
		    	foreach ($new_posts as $key => $value) {
		    		if (in_array($value->post_author , $order_export_product_seller_list) ) {
		    			$posts[] = $value;
		    		}
		    	}

		    }else{
		    	$posts=get_posts($args);  
		    }
		        	    	    	

		    //get the post ids as product ids
		    if (isset($order_export_product_list_desable) && $order_export_product_list_desable == 0) {
		    	$orders_array=wp_list_pluck( $posts, 'ID' );
		    
			    foreach ($orders_array as $key => $value) {
			    	$order = new WC_Order($value);
					$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
					foreach ( $items as $item ) {
						$orders_list[$value][] = $item['product_id'];
					}

			    }

			    foreach ($orders_list as $key => $value) {
					foreach ($value as $p_key => $p_value) {
						if (in_array($p_value, $order_export_product_list)) {
							$orders[]=$key;
						}
					}	
			    }
		    }else{
		    	$orders=wp_list_pluck( $posts, 'ID' );
		    }

	 	
		    //category vise
		    if (isset($order_export_product_cat_list_desable) && $order_export_product_cat_list_desable == 0) {	    	
		    	
			    foreach ($orders as $key => $value) {

			    	$order = new WC_Order($value);

					$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
					
					foreach ( $items as $item ) {
						$orders_list[$value] = $item['product_id'];
					}

			    }
			    
			    foreach ($orders_list as $key => $value) {

			    	$cat= get_the_terms( $value, 'product_cat' );
			    	
			    	if (is_array($cat)) { 
			    		foreach ($cat as $cats) {
				    		if (in_array($cats->term_id, $order_export_product_cat_list)) {
					    		$orders_new[]=$key;
					    	}
				    	}
			    	}
			    	
			    	
			    }	

			    $orders =$orders_new;

		    }
		 	


		 //Register user Filter
		if (isset($reg_user_list) && !empty($reg_user_list)) {			
			$reg_user_list = explode(',', $reg_user_list);
			foreach ($orders as $r_key => $r_value) {														
				$customer_user = get_post_meta( $r_value, '_customer_user', true );
				if (!empty($customer_user) && $customer_user != 0 ) {					
					if (in_array($customer_user, $reg_user_list)) {					
						$reg_orders[]=$r_value;
					}
				}				
			}
			$orders =$reg_orders;
		}//Register user Filter

		/*echo "<pre>";
			echo print_r($orders);exit;*/
		//Order ID
		if (isset($_GET['order_list']) && !empty($_GET['order_list']) ) {

	    	$orders = explode(',', $_GET['order_list']);
	    }
		    
		$orders = array_unique($orders);

		return $orders;
	}

	/**
	 *
	 * Send  Excel Invoice To Customer and Admin
	 * 
	 * @package WooCommerce - Order Export
	 * @since 2.1.3
	 * 
	 */	
	public function woo_order_exp_send_invoice_excel($post_id='')
	{
		global $product,$woocommerce;	
		ob_start();		

		$meta_values = get_post_meta( $post_id );	
		$order = new WC_Order($post_id);
		$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
		
		$symbol = $this->woo_order_exp_currency_symbol();
		
		$excel_sheet = get_option("woo_order_excel_sheet_settings");//Display In Settings
		$counter = 1;
	 	
		$data = "<?xml version='1.0'?>
				<?mso-application progid='Excel.Sheet'?>
				<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>";
				$data.="<Styles>
				  	<Style ss:ID='Default' ss:Name='Normal'>
				   	<Alignment ss:Vertical='Bottom'/>
				   	<Borders/>
				   	<Font/>
				   	<Interior/>
				   	<NumberFormat/>
				   	<Protection/>
				  	</Style>
				  	<Style ss:ID='s21'>
				   	<Alignment ss:Horizontal='Center' ss:Vertical='Bottom'/>
				   	<Font x:Family='Swiss' ss:Bold='1'/>
				    </Style>
				   	<Style ss:ID='s22'>
				   	<Alignment ss:Horizontal='Center' ss:Vertical='Bottom'/>
				   	</Style>
				 	</Styles>";
			 				$data.= "<Worksheet ss:Name='Order Information'><Table>";
									$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"150\"/>
											 <Column ss:AutoFitWidth=\"0\" ss:Width=\"160\"/>";
									$data.= "<Row>";
									$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='10'><Data ss:Type='String'>Order Information </Data></Cell>	</Row>";
									$data.= "<Row>";
									$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>	";
										$data.=$this->woo_order_exp_excel_get_column('woo_order_id_enable');
								 		$data.=$this->woo_order_exp_excel_get_column('woo_order_status_title_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_date_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_modified_date_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_user_name_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_first_name_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_last_name_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_company_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_address_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_second_bill_address_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_city_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_state_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_country_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_post_code_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_phone_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_bill_email_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_first_name_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_last_name_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_company_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_address_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_city_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_state_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_country_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_post_code_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_ship_method_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_payment_method_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_payment_paypal_enable');
								 		
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_shipping_charge_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_cart_discount_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_order_tax_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_shipping_tax_enable');
								 		$data.= $this->woo_order_exp_excel_get_column('woo_order_order_total_enable');
							 	
								 		//display / export in same sheet
										if ($excel_sheet == 1) {
											$product_export = get_option("woo_order_product_export_settings");	
											//One Column Start
											if ($product_export == 1) {
												$data.= $this->woo_order_exp_excel_get_column('woo_order_product_name_enable');
												$data.= $this->woo_order_exp_excel_get_column('woo_order_sku_enable');
												$data.= $this->woo_order_exp_excel_get_column('woo_order_price_enable');
												$data.= $this->woo_order_exp_excel_get_column('woo_order_quantity_enable');
												$data.= $this->woo_order_exp_excel_get_column('woo_order_total_enable');
												$data.= $this->woo_order_exp_excel_get_column('woo_order_fee_enable');
												$data.= $this->woo_order_exp_excel_get_column('woo_order_order_total_enable');
											}else{
												foreach ( $items as $key => $item ) {	
													$prod_list[] =  $item['product_id'];
													$product =get_product($item['product_id']);
													 
													$attributes = $product->get_attributes();
													$product_name = $item['name'];
													$data.= "<Cell  ss:StyleID='s21' ><Data ss:Type='String'>Product :- ".$product_name."</Data></Cell>
													<Cell  ss:StyleID='s21'><Data ss:Type='String'>Price :-".$product_name."</Data></Cell>";
													$data.= (get_option('woo_order_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".get_option('woo_order_sku')." :-".$product_name."</Data></Cell>" : '' ;
													if (!empty($attributes)) {
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$key."</Data></Cell>";
														}
													}
													$data.= $this->woo_order_exp_excel_get_column('woo_order_total_enable');
												}	
												$data.= $this->woo_order_exp_excel_get_column('woo_order_order_total_enable');
											}
											//One Column End
											
										}	//display / export in same sheet
									
									$data .= "</Row>";
									
									$data .= "<Row>";
										
										$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='Number'>".$counter."</Data></Cell>";
												
											$data.= (get_option('woo_order_id_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->id."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_status_title_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->status."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_date_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".date('Y-m-d' , strtotime($order->order_date) )."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_modified_date_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".date('Y-m-d' , strtotime($order->modified_date) )."</Data></Cell>" : '' ;
											
											$customer_user = get_post_meta( $post_id, '_customer_user', true );
											
											if (!empty($customer_user) && $customer_user != 0 ) {
												$author_data     = get_user_by( 'id', $customer_user );
												$user_name = $author_data->user_login;																										
											}else{
												$user_name = '';
											}
												
											$data.= (get_option('woo_order_user_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$user_name."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_first_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_first_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_last_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_last_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_company_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_company'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_address_1'][0]."</Data></Cell>" : '' ; 
											$data.= (get_option('woo_order_second_bill_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_address_2'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_city_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_city'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_state_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_state'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_country_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".WC()->countries->countries[$meta_values['_billing_country'][0]]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_post_code_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_postcode'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_phone_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_phone'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_bill_email_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_email'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_first_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_first_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_last_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_last_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_company_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_company'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_address_1'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_city_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_city'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_state_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_state'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_country_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".WC()->countries->countries[$meta_values['_shipping_country'][0]]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_ship_post_code_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_postcode'][0]."</Data></Cell>" : '' ;
											$shipping_method = $order->get_shipping_methods();
											if (!empty($shipping_method)) {
												foreach ($shipping_method as $s_key => $s_value) {
													$ship_method = $s_value['name'];
												}
											}
											$ship_method = (!empty($ship_method)) ? $ship_method : '';
											
											$data.= (get_option('woo_order_ship_method_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$ship_method."</Data></Cell>" : '' ;
											
											
											$data.= (get_option('woo_order_payment_method_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_payment_method'][0]."</Data></Cell>" : '' ;
											if (isset($meta_values['_paypal_email'][0])) {
												$PayPal_Id = (!empty($meta_values['_paypal_email'][0])) ? $meta_values['_paypal_email'][0] : '';
											}else $PayPal_Id = '';
											
											
											$data.= (get_option('woo_order_payment_paypal_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$PayPal_Id."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_shipping_charge_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_shipping'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_cart_discount_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_cart_discount'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_order_tax_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_tax'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_shipping_tax_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_shipping_tax'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_total'][0]."</Data></Cell>" : '' ;
										//display / export in same sheet	
										if ($excel_sheet == 1) {
											
											$product_export = get_option("woo_order_product_export_settings");	
											if ($product_export == 1) {
												$data.= $this->woo_order_send_excel_get_data_sheet($post_id);
											}else{
												$order = new WC_Order($post_id);
												$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));			
												$grand_total = 0; 		
												foreach ( $items as $key => $item ) {	
													$product = get_product($item['product_id']);
													 
													$attributes = $product->get_attributes();
													$product_name = $item['name'];
												    $product_info = $item['item_meta'];
												    $total=$product_info['_line_total'][0];
												    $quantity=$product_info['_qty'][0];
												   	$price = $total / $quantity;
												   	$product =get_product($item['product_id']);	
			    									$sku = $product->get_sku();
			    									$SKU = ( !empty($sku) ) ? $sku : '';
													
													$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>Quantity :-".$quantity."</Data></Cell>
													<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>	";
													$data.= (get_option('woo_order_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$SKU."</Data></Cell>" : '' ;
													if (!empty($attributes)) {
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$value['value']."</Data></Cell>";
														}
													}
													$data.= (get_option('woo_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$total."</Data></Cell>" : '' ;
													$grand_total =  $grand_total + $total;			 
												}
												$order_total_enable = get_option('woo_order_order_total_enable');
												if ($order_total_enable == 1) {
													$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$grand_total."</Data></Cell>"; 
												}						
											}												
										}	//display / export in same sheet
									$data.= "</Row>";
								$data .= "</Table></Worksheet>";

								//display / export Different Sheet	
								if ($excel_sheet == 2) {
										$data.= "<Worksheet ss:Name='Order Details' ><Table>";
										$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"120\"/>
												 <Column ss:AutoFitWidth=\"0\" ss:Width=\"130\"/>";
										$data.= "<Row>";
										$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='5'><Data ss:Type='String'>Order Details</Data></Cell>	</Row>";
										$data.= "<Row>";
										$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>";
										$data.= $this->woo_order_exp_excel_get_column('woo_order_id_enable');
										$data.= $this->woo_order_exp_excel_get_column('woo_order_product_name_enable');
										$data.= $this->woo_order_exp_excel_get_column('woo_order_sku_enable');
										$data.= $this->woo_order_exp_excel_get_column('woo_order_price_enable');
										$data.= $this->woo_order_exp_excel_get_column('woo_order_quantity_enable');			
															
										$id_skip = (get_option('woo_order_id_enable')==1) ? 1 : 0;
										$pn_skip = (get_option('woo_order_product_name_enable')==1) ? 1 : 0;
										$sku_skip = (get_option('woo_order_sku_enable')==1) ? 1 : 0;
										$pr_skip = (get_option('woo_order_price_enable')==1) ? 1 : 0;
										$qty_skip = (get_option('woo_order_quantity_enable')==1) ? 1 : 0;
										$skip = $id_skip + $pn_skip + $sku_skip + $pr_skip + $qty_skip;
										foreach ( $items as $key => $item ) {	
											$count_att = 0;
											$product =get_product($item['product_id']);		
											if (is_object($product)) {
											  $attributes = $product->get_attributes();
											}									 
											
											if (!empty($attributes)) {
												
												foreach ($attributes as $key => $value) {														
													$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$key."</Data></Cell>";
													$count_att++;
												}
											}										
										}	
										$data.= $this->woo_order_exp_excel_get_column('woo_order_total_enable');
										$data .= "</Row>";
										$counter = 1;
										$grand_total =0;						    	
										foreach ( $items as $item ) {
											    $product_name = $item['name'];
											    $product_info = $item['item_meta'];
											    $total=$product_info['_line_total'][0];
										       	$product =get_product($item['product_id']);	
		    									//$sku = $product->get_sku();
		    									$SKU = ( !empty($sku) ) ? $sku : '';
											    $type = $item['type'];
											    if ($type == 'line_item') {
											    	$quantity=$product_info['_qty'][0];
											    	$price = $total / $quantity;
											    	$data .= "<Row>"; 
												    $data.= "<Cell  ss:StyleID='s22'><Data ss:Type='Number'>".$counter."</Data></Cell>";
													$data.= (get_option('woo_order_id_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->id."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_product_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$product_name."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$SKU."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_price_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_quantity_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$quantity."</Data></Cell>" : '' ;
													
													$id_skip = (get_option('woo_order_id_enable')==1) ? 1 : 0;
													$pn_skip = (get_option('woo_order_product_name_enable')==1) ? 1 : 0;
													$sku_skip = (get_option('woo_order_sku_enable')==1) ? 1 : 0;
													$pr_skip = (get_option('woo_order_price_enable')==1) ? 1 : 0;
													$qty_skip = (get_option('woo_order_quantity_enable')==1) ? 1 : 0;
													$skip = $id_skip + $pn_skip + $sku_skip + $pr_skip + $qty_skip;		
													
													$counter++;
													$product =get_product($item['product_id']);											 
													//$attributes = $product->get_attributes();
													if (!empty($attributes)) {
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$value['value']."</Data></Cell>";
														}
													}else{
														for ($i=0; $i < $count_att; $i++) { 
															$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'></Data></Cell>";
															
														}
													}										
													$data.= (get_option('woo_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$total."</Data></Cell>" : '' ;		
													$data.= "</Row>";
											    }else{
											    	$price = $total;
											    	$merge = $count_att + $skip;
											    	$fee_enable = get_option('woo_order_fee_enable');
											    	if ($fee_enable == 1) {
											    		$data .= "<Row >"; 													    
														$data.= "<Cell  ss:StyleID='s22' ss:MergeAcross='".$merge."'><Data ss:Type='String'>".$product_name."</Data></Cell>																										
														<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>
														</Row>"; 
											    	}												    	
											    }	 
											    $grand_total =  $grand_total + $total;
											    
											    
												if (end($item) ) {	}
											    
											}
												$merge = $count_att + $skip;
												$order_total_enable = get_option('woo_order_order_total_enable');
												if ($order_total_enable == 1) {
													$data .= "<Row>";
													$data.= "<Cell  ss:StyleID='s21' ss:MergeAcross='".$merge."'><Data ss:Type='String'>".get_option('woo_order_order_total')."</Data></Cell>																										
													<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$symbol.$grand_total."</Data></Cell>
													</Row>";
												}
												$data .= "<Row></Row>"; 
																					
										$data .= "</Table></Worksheet>";		
								}//display / export Different Sheet	
										
					$data.="</Workbook>";	

		ob_get_clean();
		file_put_contents(WOO_ORDER_EXP_SAVE_FILE.'/'.$post_id.'-order.xls', $data);		
		
		$file = $post_id.'-order.xls';
		return $file;
	}//Send Excel Invoice End

	

	/**
	*
	* Send CSV Invoice To Customer and Admin
	* 
	* @package WooCommerce - Order Export
	* @since 2.1.3
	* 
	*/	
	public function woo_order_exp_send_invoice_csv($post_id='')
	{
		$fp = fopen(WOO_ORDER_EXP_SAVE_FILE.'/'.$post_id.'-order.csv', 'w');

		$meta_values = get_post_meta( $post_id );	
		global $woocommerce;

		$symbol = $this -> woo_order_exp_currency_symbol();
		$symbol = utf8_decode($symbol);
		
		$order = new WC_Order($post_id);
		$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
		$excel_sheet = get_option("woo_order_excel_sheet_settings");
		
			$data_ary = ' No ,';
			$data_ary.=$this->woo_order_exp_csv_get_column('woo_order_id_enable');
	 		$data_ary.=$this->woo_order_exp_csv_get_column('woo_order_status_title_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_date_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_modified_date_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_user_name_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_first_name_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_last_name_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_company_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_address_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_second_bill_address_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_city_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_state_enable'); 		
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_country_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_post_code_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_phone_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_bill_email_enable');
	 		
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_first_name_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_last_name_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_company_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_address_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_city_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_state_enable');	 		
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_country_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_post_code_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_ship_method_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_payment_method_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_payment_paypal_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_shipping_charge_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_cart_discount_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_order_tax_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_shipping_tax_enable');
	 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_order_total_enable');
	 		
	 		
		 
			$product_export = get_option("woo_order_product_export_settings");
		 	if ($product_export == 1) {
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_product_name_enable');
			 	$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_sku_enable');
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_price_enable');
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_quantity_enable');
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_total_enable');
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_fee_enable');
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_order_total_enable');
			}else{	
				foreach ( $items as $key => $item ) {	
					$prod_list[] =  $item['product_id'];
					$product =get_product($item['product_id']);
					 
					$attributes = $product->get_attributes();
					$product_name = $item['name'];
					$data_ary.= (get_option('woo_order_product_name_enable') == 1 ) ? get_option('woo_order_product_name')." :-".$product_name." ," : '' ;
					$data_ary.= (get_option('woo_order_sku_enable') == 1 ) ?  get_option('woo_order_sku')." :-".$product_name." ," : '' ;
					$data_ary.= (get_option('woo_order_price_enable') == 1 ) ? get_option('woo_order_price').":-".$product_name." ," : '' ;
					
					if (!empty($attributes)) {
						foreach ($attributes as $key => $value) {														
							$data_ary.= " ".$key." ,";
						}
					}
					$data_ary.= (get_option('woo_order_total_enable') == 1 ) ? get_option('woo_order_total')." :-".$product_name." ," : '' ;
				
				$type = $item['type'];
				if ($type != 'line_item') {
					$data_ary.= $product_name.",";
				}
				}		
		 		$data_ary.= $this->woo_order_exp_csv_get_column('woo_order_order_total_enable');
			}
			
				
		$data_array[] = explode(',', $data_ary);	
		foreach ($data_array as $row) {
	        fputcsv($fp, $row); // here you can change delimiter/enclosure
	    }
		
		$counter = 1;
		
		$data = '';
		$get_data_array = '';
		$meta_values = get_post_meta( $post_id );	
		$order = new WC_Order($post_id);
			$data .= $counter.",";
			$data.= (get_option('woo_order_id_enable') == 1 ) ? $order->id." ," : '' ;
			$data.= (get_option('woo_order_status_title_enable') == 1 ) ? $order->status." ," : '' ;
			$data.= (get_option('woo_order_date_enable') == 1 ) ? date('Y-m-d' , strtotime($order->order_date) )." ," : '' ;
			$data.= (get_option('woo_order_modified_date_enable') == 1 ) ? date('Y-m-d' , strtotime($order->modified_date) )." ," : '' ;
			
			$customer_user = get_post_meta( $post_id, '_customer_user', true );
			if (!empty($customer_user) && $customer_user != 0 ) {
				$author_data     = get_user_by( 'id', $customer_user );
				$user_name = $author_data->user_login;																										
			}else{
				$user_name = '';
			}
				
			$data.= (get_option('woo_order_user_name_enable') == 1 ) ? $user_name." ," : '' ;
			$data.= (get_option('woo_order_bill_first_name_enable') == 1 ) ? $meta_values['_billing_first_name'][0]." ," : '' ;
			$data.= (get_option('woo_order_bill_last_name_enable') == 1 ) ? $meta_values['_billing_last_name'][0]." ," : '' ;
			$data.= (get_option('woo_order_bill_company_enable') == 1 ) ? $meta_values['_billing_company'][0]." ," : '' ;
			$search =  ',' ;
			$search = str_split($search);
			$bill_first_address =str_replace($search, "", $meta_values['_billing_address_1'][0]);
			$bill_second_address =str_replace($search, "", $meta_values['_billing_address_2'][0]);		
			$data.= (get_option('woo_order_bill_address_enable') == 1 ) ? $bill_first_address." ," : '' ; 
			$data.= (get_option('woo_order_second_bill_address_enable') == 1 ) ? $bill_second_address." ," : '' ;
			$data.= (get_option('woo_order_bill_city_enable') == 1 ) ? $meta_values['_billing_city'][0]." ," : '' ;
			$data.= (get_option('woo_order_bill_state_enable') == 1 ) ? $meta_values['_billing_state'][0]." ," : '' ;
			$data.= (get_option('woo_order_bill_country_enable') == 1 ) ? WC()->countries->countries[$meta_values['_billing_country'][0]]." ," : '' ;
			$data.= (get_option('woo_order_bill_post_code_enable') == 1 ) ? $meta_values['_billing_postcode'][0]." ," : '' ;
			$data.= (get_option('woo_order_bill_phone_enable') == 1 ) ? $meta_values['_billing_phone'][0]." ," : '' ;
			$data.= (get_option('woo_order_bill_email_enable') == 1 ) ? $meta_values['_billing_email'][0]." ," : '' ;
			$data.= (get_option('woo_order_ship_first_name_enable') == 1 ) ? $meta_values['_shipping_first_name'][0]." ," : '' ;
			$data.= (get_option('woo_order_ship_last_name_enable') == 1 ) ? $meta_values['_shipping_last_name'][0]." ," : '' ;
			$data.= (get_option('woo_order_ship_company_enable') == 1 ) ? $meta_values['_shipping_company'][0]." ," : '' ;
			$ship_first_address =str_replace($search, "", $meta_values['_shipping_address_1'][0]);
			$data.= (get_option('woo_order_ship_address_enable') == 1 ) ? $ship_first_address." ," : '' ;
			$data.= (get_option('woo_order_ship_city_enable') == 1 ) ? $meta_values['_shipping_city'][0]." ," : '' ;
			$data.= (get_option('woo_order_ship_state_enable') == 1 ) ? $meta_values['_shipping_state'][0]." ," : '' ;
			$data.= (get_option('woo_order_ship_country_enable') == 1 ) ? WC()->countries->countries[$meta_values['_shipping_country'][0]]." ," : '' ;
			$data.= (get_option('woo_order_ship_post_code_enable') == 1 ) ? $meta_values['_shipping_postcode'][0]." ," : '' ;
			
			$shipping_method = $order->get_shipping_methods();
			if (!empty($shipping_method)) {
				foreach ($shipping_method as $s_key => $s_value) {
					$ship_method = $s_value['name'];
				}
			}
			$ship_method = (!empty($ship_method)) ? $ship_method : '';
			$data.= (get_option('woo_order_ship_method_enable') == 1 ) ? $ship_method." ," : '' ;
			$data.= (get_option('woo_order_payment_method_enable') == 1 ) ? $meta_values['_payment_method'][0]." ," : '' ;
			if (isset($meta_values['_paypal_email'][0])) {
				$PayPal_Id = (!empty($meta_values['_paypal_email'][0])) ? $meta_values['_paypal_email'][0].',' : ',';
			}else $PayPal_Id = ',';
			
			$data.= (get_option('woo_order_payment_paypal_enable') == 1 ) ? $PayPal_Id : '' ;
			$data.= (get_option('woo_order_shipping_charge_enable') == 1 ) ? $symbol.$meta_values['_order_shipping'][0]." ," : '' ;
			$data.= (get_option('woo_order_cart_discount_enable') == 1 ) ? $symbol.$meta_values['_cart_discount'][0]." ," : '' ;
			$data.= (get_option('woo_order_order_tax_enable') == 1 ) ? $symbol.$meta_values['_order_tax'][0]." ," : '' ;
			$data.= (get_option('woo_order_shipping_tax_enable') == 1 ) ? $symbol.$meta_values['_order_shipping_tax'][0]." ," : '' ;
			$data.= (get_option('woo_order_order_total_enable') == 1 ) ? $symbol.$meta_values['_order_total'][0]." ," : '' ;
			
			$data.= $this->woo_order_exp_csv_get_data_sheet($post_id);
			
			$get_data_array[] = explode(',', $data);
		
		foreach ($get_data_array as $row) {
	        fputcsv($fp, $row); // here you can change delimiter/enclosure
	    }
		fclose($fp); 
		$file = $post_id.'-order.csv';
		return $file;
	}//CSV Invoice End


	
	/**
	 *
	 * Get default data settings for Excell or csv  
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	public function woo_order_exp_excel_default()
	{	

		//pdf title
		update_option('woo_order_export_pdf_title','Invoice Details');

		update_option('woo_order_excel_sheet_settings',2);		
		
		update_option('woo_order_id_enable',1);
		update_option('woo_order_status_title_enable',1);		
		update_option('woo_order_date_enable','');
		update_option('woo_order_modified_date_enable',1);
		update_option('woo_order_payment_method_enable',1);
		update_option('woo_order_payment_paypal_enable',1);
		update_option('woo_order_shipping_charge_enable',1);
		update_option('woo_order_cart_discount_enable',1);
		update_option('woo_order_order_tax_enable',1);
		update_option('woo_order_shipping_tax_enable',1);
		update_option('woo_order_sku_enable',1);
		update_option('woo_order_product_name_enable',1);
		update_option('woo_order_price_enable',1);
		update_option('woo_order_quantity_enable',1);
		update_option('woo_order_total_enable',1);
		update_option('woo_order_fee_enable',1);
		update_option('woo_order_order_total_enable',1);

		//customer default excel/csv
		update_option('woo_order_user_name_enable',1);
		update_option('woo_order_bill_first_name_enable',1);
		update_option('woo_order_bill_last_name_enable',1);
		update_option('woo_order_bill_company_enable',1);
		update_option('woo_order_bill_address_enable',1);
		update_option('woo_order_second_bill_address_enable',1);
		update_option('woo_order_bill_city_enable',1);
		update_option('woo_order_bill_state_enable',1);
		update_option('woo_order_bill_country_enable',1);
		update_option('woo_order_bill_post_code_enable',1);
		update_option('woo_order_bill_phone_enable',1);
		update_option('woo_order_bill_email_enable',1);

		update_option('woo_order_ship_first_name_enable',1);
		update_option('woo_order_ship_last_name_enable',1);
		update_option('woo_order_ship_company_enable',1);
		update_option('woo_order_ship_address_enable',1);
		update_option('woo_order_ship_city_enable',1);
		update_option('woo_order_ship_state_enable',1);
		update_option('woo_order_ship_country_enable',1);
		update_option('woo_order_ship_post_code_enable',1);
		update_option('woo_order_ship_method_enable',1);
		
		//order default excel/csv
		update_option('woo_order_id','Order ID');
		update_option('woo_order_status_title','Order Status');
		update_option('woo_order_date','Order Date');
		update_option('woo_order_modified_date','Order Modified Date');
		update_option('woo_order_payment_method','Payment Method');
		update_option('woo_order_payment_paypal','Paypal ID');
		update_option('woo_order_shipping_charge','Shipping Charge');
		update_option('woo_order_cart_discount','Cart Discount');
		update_option('woo_order_order_tax','Order Tax');
		update_option('woo_order_shipping_tax','Order Tax');
		update_option('woo_order_sku','SKU');
		update_option('woo_order_product_name','Product Name');
		update_option('woo_order_price','Product Price');
		update_option('woo_order_quantity','Quantity');
		update_option('woo_order_total','Total');
		update_option('woo_order_order_total','Order Total');

		//customer default excel/csv
		update_option('woo_order_user_name','User Name');
		update_option('woo_order_bill_first_name','Bill First Name');
		update_option('woo_order_bill_last_name','Bill Last Name');
		update_option('woo_order_bill_company','Bill Company');
		update_option('woo_order_bill_address','Bill Address');
		update_option('woo_order_second_bill_address','Bill Second Address');
		update_option('woo_order_bill_city','Bill City');
		update_option('woo_order_bill_state','Bill State');
		update_option('woo_order_bill_country','Bill Country');
		update_option('woo_order_bill_post_code','Bill Zip');
		update_option('woo_order_bill_phone','Bill Phone');
		update_option('woo_order_bill_email','Bill Email');

		update_option('woo_order_ship_first_name','Shipping First Name');
		update_option('woo_order_ship_last_name','Shipping Last Name');
		update_option('woo_order_ship_company','Shipping Company');
		update_option('woo_order_ship_address','Shipping Address');
		update_option('woo_order_ship_city','Shipping City');
		update_option('woo_order_ship_state','Shipping State');
		update_option('woo_order_ship_country','Shipping Country');
		update_option('woo_order_ship_post_code','Shipping Zip');
		update_option('woo_order_ship_method','Shipping Method');		
		

		//Mail Function
		update_option('woo_order_exp_attach_invoice',0);
		update_option('woo_order_excel_attached_file_type',1);

		//Invoice for buyer
		update_option('woo_order_exp_buyer_invoice',0);
		update_option('woo_order_excel_buyer_invoice_file_type',1);
		update_option('woo_order_excel_buyer_invoice_file_title','Download Order Report');

	}		
}
?>