<?php 
ob_start();
$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once($root .'/wp-load.php');

if ( current_user_can( 'manage_options' ) ) {

require(WOO_ORDER_EXP_DIR.'/includes/fpdf/fpdf.php');

global $woocommerce,$wpdb,$woo_order_exp_model;
		
$model = $woo_order_exp_model;

$post_id = $_GET['post_id'];

$meta_values = get_post_meta( $post_id );
$order = new WC_Order($post_id);
$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	

		$pdf = new FPDF();
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(true, 15);

		$pdf->AddPage();				

			$pdf_title = get_option('woo_order_export_pdf_title');

			$pdf->SetFont('Arial','B',16);	//title
			$pdf->Cell(60);	
			$pdf->Cell(50,10,$pdf_title,1,1,'C');

			$woo_order_export_pdf_enable = get_option('woo_order_export_pdf_enable');

			$billing_title_enable = get_option('woo_order_export_buyer_bill_title_enable');

			$billing_title = get_option('woo_order_export_buyer_bill_title');

			$shipping_title_enable = get_option('woo_order_export_buyer_shipp_title_enable');

			$shipping_title = get_option('woo_order_export_buyer_shipp_title');



			if ($woo_order_export_pdf_enable == 1) {

					$woo_order_export_pdf_logo = get_option('woo_order_export_pdf_logo');

					$woo_order_export_pdf_shopname = get_option('woo_order_export_pdf_shopname');

					$woo_order_export_pdf_address = get_option('woo_order_export_pdf_address');

					$store_title = get_option('woo_order_export_store_title');		
					
					$width = get_option('woo_order_export_pdf_logo_width');

					$height = get_option('woo_order_export_pdf_logo_height');

					$pdf->SetY(8);//logo

					if ($woo_order_export_pdf_logo != '' ) {
								
						$pdf->Image($woo_order_export_pdf_logo,95,30,$width,$height);	
					}
					

					$pdf->SetFont('Arial', 'U',12);
					$pdf->Cell(150,33,$store_title, 0, 1,'R');

					$pdf->SetY(13);
					$pdf->SetFont('Arial', '', 8);		
					$pdf->Cell(150,38, $woo_order_export_pdf_shopname, 0, 1,'R');

					$address = explode(",", $woo_order_export_pdf_address);

					$setya = 13;


					foreach ($address as $key => $value) {

						$pdf->SetY($setya);

						$pdf->SetFont('Arial', '', 8);

						$pdf->Cell(150,45, $value, 0, 1,'R');						

						$setya = $setya + 4;
					}
						

					$second_title=50;

					$second_details=55;

					$shipping=80;

			}else{

				$second_title=30;

				$second_details=35;

				$shipping=60;

			}
				
				
				
				if ($billing_title_enable == 1) {

					$pdf->SetY($second_title);
					$pdf->Cell(15);
					$pdf->SetFont('Arial', 'U',12);
					$pdf->Cell(195,10, $billing_title, 0, 1);
					$pdf->SetY($second_details);
					
					$pdf->Cell(13);
					$pdf->SetFont('Arial', '', 8);
					$pdf->Cell(40, 15, $meta_values['_billing_first_name'][0].' '.$meta_values['_billing_last_name'][0]);
					$pdf->Cell(100,15,'Order Date : '.date('Y-m-d' , strtotime($order->order_date) ) );
					$pdf->Ln(4);
					
					$pdf->Cell(13);
					$pdf->Cell(40, 15, $meta_values['_billing_address_1'][0]);
					$pdf->Cell(100,15,'Payment Method : '.$meta_values['_payment_method'][0]);
					$pdf->Ln(4);
					
					$pdf->Cell(13);
					$pdf->Cell(40, 15, $meta_values['_billing_city'][0].', '.$meta_values['_billing_postcode'][0]);
					$pdf->Ln(4);
					$pdf->Cell(13);
					$pdf->Cell(40, 15, $meta_values['_billing_state'][0].', '.WC()->countries->countries[ $meta_values['_billing_country'][0] ]);

				}else{
					$pdf->SetY($second_details);
					$pdf->Cell(13);
					$pdf->SetFont('Arial', '', 8);
					
					$pdf->Cell(100,15,'Order Date : '.$order->order_date);
					$pdf->Ln(4);
					$pdf->Cell(13);
					
					$pdf->Cell(100,15,'Payment Method : '.$meta_values['_payment_method'][0]);
				}


				if ($shipping_title_enable == 1) {

					$pdf->SetY(20);
					$pdf->Cell(15);
					$pdf->SetFont('Arial', 'U',12);
					$pdf->Cell(195,10, $shipping_title, 0, 1);

					$pdf->SetY(25);
					$pdf->Cell(13);
					$pdf->SetFont('Arial', '', 8);
					$pdf->Cell(40, 15, $meta_values['_shipping_first_name'][0].' '.$meta_values['_shipping_last_name'][0]);
					
					$pdf->Ln(4);

					$pdf->Cell(13);
					$pdf->Cell(40, 15, $meta_values['_shipping_address_1'][0]);
					
					$pdf->Ln(4);

					$pdf->Cell(13);
					$pdf->Cell(40, 15, $meta_values['_shipping_city'][0].', '.$meta_values['_shipping_postcode'][0]);
					$pdf->Ln(4);
					$pdf->Cell(13);
					$pdf->Cell(40, 15, $meta_values['_shipping_state'][0].', '.WC()->countries->countries[ $meta_values['_shipping_country'][0] ]);

				}
				
				
				$pdf->Ln(20);
				$pdf->SetY(87);				
				$pdf->Cell(13);			
				$pdf->SetFont('Arial', 'B', 12);
				$pdf->SetTextColor(0);				
				$pdf->SetFillColor(94, 188, 225);
				$pdf->SetLineWidth(0.6);
				$pdf->Cell(13, 12, "No", 'LTR', 0, 'C', true);
				$pdf->Cell(35, 12, "Product", 'LTR', 1, 'C', true);

				$pdf->SetY(87);
				$pdf->Cell(61);
				$pdf->Cell(35, 12, "Price", 'LTR', 2, 'C', true);

				$pdf->SetY(87);
				$pdf->Cell(96);
				$pdf->Cell(35, 12, "Quantity", 'LTR', 3, 'C', true);

				$pdf->SetY(87);
				$pdf->Cell(131);
				$pdf->Cell(35, 12, "Total", 'LTR', 4, 'C', true);
	 
				$pdf->SetFont('Arial', '');
				$pdf->SetFillColor(238);
				$pdf->SetLineWidth(0.2);
				$fill = false;

				

				$counter=1; $grand_total=0;$total_quantity=0;$price=0;$fee_total = 0;
				
				$symbol = $model -> woo_order_exp_currency_symbol();

				$symbol = utf8_decode($symbol);
				
				$yset = 99;
				foreach ( $items as $item ) {

					$product_info = $item['item_meta'];

					$product_info = $item['item_meta'];

				    $total=$product_info['_line_total'];

				    
					$type = $item['type'];
				    if ($type == 'line_item') {

				    	$quantity=$product_info['_qty'];
				    	
				    	$price = $total[0] / $quantity[0];
				    	$price = $symbol.$price;
				    	$grand_total =  $grand_total + $total[0];	    
					   	$total_quantity = 	$total_quantity + $quantity[0];				   	

				    				    
				  
					    $pdf->SetY($yset);	
						$pdf->Cell(13);
						$pdf->Cell(13,10, $counter, 1, 0, 'C', $fill);
						$pdf->Cell(35, 10,  $item['name'], 1, 1, 'C', $fill);
						
						
						
							
						$qty = $quantity[0];
						$pdf->SetY($yset);	
						$pdf->Cell(61);
						$pdf->Cell(35, 10, $price, 1, 0, 'C', $fill);
						$pdf->Cell(35, 10, $qty , 1, 1, 'C', $fill);
						

						$pdf->SetY($yset);	
						$pdf->Cell(131);
						$pdf->Cell(35, 10,  $symbol.$total[0], 1, 1, 'C', $fill);
						
						
						
						
						$fill = !$fill;
						$pdf->Ln(0);
						$counter++;
						$yset = $yset + 10;
						
						$product =get_product($item['product_id']);				 
						$attributes = $product->get_attributes();
						if (!empty($attributes)) {
								
							$cn = 1;
							foreach ($attributes as $key => $value) {	
								$name = $key;
								$value = $value['value'];
								
								$pdf->SetY($yset);	
								$pdf->Cell(13);
								$pdf->Cell(13,10," " , 1, 0, 'C', $fill);
								$pdf->Cell(35, 10,  $cn , 1, 1, 'C', $fill);
						
								$pdf->SetY($yset);
								$pdf->Cell(61);
								$pdf->Cell(35,10 ,$name, 1, 0, 'C', $fill);
								$pdf->Cell(35 ,10, $value, 1, 0, 'C', $fill);
								
								$pdf->SetY($yset);	
								$pdf->Cell(131);
								$pdf->Cell(35, 10, "-", 1, 1, 'C', $fill);
								
								$fill = !$fill;
								$pdf->Ln(0);
								$cn++;
								$yset = $yset + 10;
							}
							
						}
									
						
						
					}
					
				}
				
				$pdf->SetY($yset);	
				$pdf->Cell(96);
				$pdf->Cell(35, 10,'SubTotal', 1, 0, 'C', $fill);
				$pdf->Cell(35, 10,  $symbol.$grand_total, 1, 1, 'C', $fill);
				$pdf->Ln(0);

				foreach ( $items as $item ) {

				    $product_name = $item['name'];

				    $product_info = $item['item_meta'];

				    $total=$product_info['_line_total'];

				   

				    $type = $item['type'];
				    if ($type == 'fee') {
				    	$pdf->Cell(96);
						$pdf->Cell(35, 10, $product_name, 1, 0, 'C', $fill);
						$pdf->Cell(35, 10,  $symbol.$total[0], 1, 1, 'C', $fill);
						$fill = !$fill;
						$pdf->Ln(0);
						$yset = $yset + 10;
						$fee_total = $fee_total + $total[0];
				    }
				}

				$order_total = $grand_total + $fee_total;
				$pdf->Cell(96);
				$pdf->SetLineWidth(0.5);
				$pdf->Cell(35, 10,'Order Total','T' , 0, 'C', true);
				$pdf->Cell(35, 10,  $symbol.$order_total,'T',  1, 'C',true);
				$pdf->Ln(0);

				$woo_order_export_footer_display = get_option('woo_order_export_footer_display');
				if(isset($woo_order_export_footer_display) && $woo_order_export_footer_display == 1){

					$woo_order_export_pdf_footer = get_option('woo_order_export_pdf_footer');
					$pdf->SetY(-25);	
					$pdf->Cell(1);
					$pdf->SetTextColor(0);
					$pdf->SetFillColor(169,3,41);
					$pdf->SetLineWidth(0.2);					
					$pdf->Cell(180,10,  $woo_order_export_pdf_footer,'T',1,'C');

					$pdf->SetY(-25);	
					$pdf->Cell(1);
					$woo_order_export_page_enable = get_option('woo_order_export_page_enable');
					if(isset($woo_order_export_page_enable) && $woo_order_export_page_enable == 'L'){
						$pdf->Cell(180,10,$pdf->PageNo(),'T',1,'L');
					}elseif (isset($woo_order_export_page_enable) && $woo_order_export_page_enable == 'R') {
						$pdf->Cell(180,10,$pdf->PageNo(),'T',1,'R');
					}
				}
ob_end_clean();
	$pdf->Output($meta_values['_billing_first_name'][0].'-order.pdf','D');
	ob_end_flush();


}else{
	$my_error = new WP_Error( 'error', 'Please login with administrator role.', 'my best' );
	$error = $my_error->get_error_messages();
	echo $error[0];exit;
}
?>