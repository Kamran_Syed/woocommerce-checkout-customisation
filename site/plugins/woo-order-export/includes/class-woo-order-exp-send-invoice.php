<?php 
public function get_save_excel($post_id='')
	{
		global $product,$woocommerce;	
		$model= $this;
		ob_start();			

		$meta_values = get_post_meta( $post_id );	
		$order = new WC_Order($post_id);
	
   
		$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));	
		
		$symbol = $this -> woo_order_invoice_send_currency_symbol();
		
		$excel_sheet = get_option("woo_order_invoice_excel_sheet_settings");//Display In Settings
		$counter = 1;
	 	
		$data = "<?xml version='1.0'?>
				<?mso-application progid='Excel.Sheet'?>
				<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>";
				$data.="<Styles>
				  	<Style ss:ID='Default' ss:Name='Normal'>
				   	<Alignment ss:Vertical='Bottom'/>
				   	<Borders/>
				   	<Font/>
				   	<Interior/>
				   	<NumberFormat/>
				   	<Protection/>
				  	</Style>
				  	<Style ss:ID='s21'>
				   	<Alignment ss:Horizontal='Center' ss:Vertical='Bottom'/>
				   	<Font x:Family='Swiss' ss:Bold='1'/>
				    </Style>
				   	<Style ss:ID='s22'>
				   	<Alignment ss:Horizontal='Center' ss:Vertical='Bottom'/>
				   	</Style>
				 	</Styles>";
			 				$data.= "<Worksheet ss:Name='Order Information'><Table>";
									$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"150\"/>
											 <Column ss:AutoFitWidth=\"0\" ss:Width=\"160\"/>";
									$data.= "<Row>";
									$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='10'><Data ss:Type='String'>Order Information </Data></Cell>	</Row>";
									$data.= "<Row>";
									$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>	";
										$data.=$model->woo_order_invoice_send_excel_get_column('woo_order_invoice_id_enable');
								 		$data.=$model->woo_order_invoice_send_excel_get_column('woo_order_invoice_status_title_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_date_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_modified_date_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_user_name_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_first_name_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_last_name_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_company_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_address_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_second_bill_address_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_city_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_state_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_country_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_post_code_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_phone_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_bill_email_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_first_name_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_last_name_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_company_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_address_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_city_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_state_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_country_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_post_code_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_ship_method_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_payment_method_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_payment_paypal_enable');
								 		
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_shipping_charge_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_cart_discount_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_order_tax_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_shipping_tax_enable');
								 		$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_order_total_enable');
							 	
								 		//display / export in same sheet
										if ($excel_sheet == 1) {
											$product_export = get_option("woo_order_invoice_product_export_settings");	
											//One Column Start
											if ($product_export == 1) {
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_product_name_enable');
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_sku_enable');
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_price_enable');
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_quantity_enable');
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_total_enable');
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_fee_enable');
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_order_total_enable');
											}else{
												foreach ( $items as $key => $item ) {	
													$prod_list[] =  $item['product_id'];
													$product =get_product($item['product_id']);
													 
													$attributes = $product->get_attributes();
													$product_name = $item['name'];
													$data.= "<Cell  ss:StyleID='s21' ><Data ss:Type='String'>Product :- ".$product_name."</Data></Cell>
													<Cell  ss:StyleID='s21'><Data ss:Type='String'>Price :-".$product_name."</Data></Cell>";
													$data.= (get_option('woo_order_invoice_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".get_option('woo_order_invoice_sku')." :-".$product_name."</Data></Cell>" : '' ;
													if (!empty($attributes)) {
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$key."</Data></Cell>";
														}
													}
													$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_total_enable');
												}	
												$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_order_total_enable');
											}
											//One Column End
											
										}	//display / export in same sheet
									
									$data .= "</Row>";
									
									$data .= "<Row>";
										
										$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='Number'>".$counter."</Data></Cell>";
												
											$data.= (get_option('woo_order_invoice_id_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->id."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_status_title_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->status."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_date_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".date('Y-m-d' , strtotime($order->order_date) )."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_modified_date_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".date('Y-m-d' , strtotime($order->modified_date) )."</Data></Cell>" : '' ;
											
											$customer_user = get_post_meta( $post_id, '_customer_user', true );
											
											if (!empty($customer_user) && $customer_user != 0 ) {
												$author_data     = get_user_by( 'id', $customer_user );
												$user_name = $author_data->user_login;																										
											}else{
												$user_name = '';
											}
												
											$data.= (get_option('woo_order_invoice_user_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$user_name."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_first_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_first_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_last_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_last_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_company_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_company'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_address_1'][0]."</Data></Cell>" : '' ; 
											$data.= (get_option('woo_order_invoice_second_bill_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_address_2'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_city_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_city'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_state_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_state'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_country_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".WC()->countries->countries[$meta_values['_billing_country'][0]]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_post_code_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_postcode'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_phone_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_phone'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_bill_email_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_billing_email'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_first_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_first_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_last_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_last_name'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_company_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_company'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_address_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_address_1'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_city_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_city'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_state_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_state'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_country_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".WC()->countries->countries[$meta_values['_shipping_country'][0]]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_ship_post_code_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_shipping_postcode'][0]."</Data></Cell>" : '' ;
											$shipping_method = $order->get_shipping_methods();
											if (!empty($shipping_method)) {
												foreach ($shipping_method as $s_key => $s_value) {
													$ship_method = $s_value['name'];
												}
											}
											$ship_method = (!empty($ship_method)) ? $ship_method : '';
											
											$data.= (get_option('woo_order_invoice_ship_method_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$ship_method."</Data></Cell>" : '' ;
											
											
											$data.= (get_option('woo_order_invoice_payment_method_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$meta_values['_payment_method'][0]."</Data></Cell>" : '' ;
											if (isset($meta_values['_paypal_email'][0])) {
												$PayPal_Id = (!empty($meta_values['_paypal_email'][0])) ? $meta_values['_paypal_email'][0] : '';
											}else $PayPal_Id = '';
											
											
											$data.= (get_option('woo_order_invoice_payment_paypal_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$PayPal_Id."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_shipping_charge_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_shipping'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_cart_discount_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_cart_discount'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_order_tax_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_tax'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_shipping_tax_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_shipping_tax'][0]."</Data></Cell>" : '' ;
											$data.= (get_option('woo_order_invoice_order_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$meta_values['_order_total'][0]."</Data></Cell>" : '' ;
										//display / export in same sheet	
										if ($excel_sheet == 1) {
											
											$product_export = get_option("woo_order_invoice_product_export_settings");	
											if ($product_export == 1) {
												$data.= $model->woo_order_invoice_send_excel_get_data_sheet($post_id);
											}else{
												$order = new WC_Order($post_id);
												$items = $order->get_items(apply_filters( 'woocommerce_admin_order_item_types', array( 'line_item', 'fee' ) ));			
												$grand_total = 0; 		
												foreach ( $items as $key => $item ) {	
													$product = get_product($item['product_id']);
													 
													$attributes = $product->get_attributes();
													$product_name = $item['name'];
												    $product_info = $item['item_meta'];
												    $total=$product_info['_line_total'][0];
												    $quantity=$product_info['_qty'][0];
												   	$price = $total / $quantity;
												   	$product =get_product($item['product_id']);	
			    									$sku = $product->get_sku();
			    									$SKU = ( !empty($sku) ) ? $sku : '';
													
													$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>Quantity :-".$quantity."</Data></Cell>
													<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>	";
													$data.= (get_option('woo_order_invoice_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$SKU."</Data></Cell>" : '' ;
													if (!empty($attributes)) {
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$value['value']."</Data></Cell>";
														}
													}
													$data.= (get_option('woo_order_invoice_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$total."</Data></Cell>" : '' ;
													$grand_total =  $grand_total + $total;			 
												}
												$order_total_enable = get_option('woo_order_invoice_order_total_enable');
												if ($order_total_enable == 1) {
													$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$grand_total."</Data></Cell>"; 
												}						
											}												
										}	//display / export in same sheet
									$data.= "</Row>";
								$data .= "</Table></Worksheet>";

								//display / export Different Sheet	
								if ($excel_sheet == 2) {
										$data.= "<Worksheet ss:Name='Order Details' ><Table>";
										$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"120\"/>
												 <Column ss:AutoFitWidth=\"0\" ss:Width=\"130\"/>";
										$data.= "<Row>";
										$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='5'><Data ss:Type='String'>Order Details</Data></Cell>	</Row>";
										$data.= "<Row>";
										$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>";
										$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_id_enable');
										$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_product_name_enable');
										$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_sku_enable');
										$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_price_enable');
										$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_quantity_enable');			
															
										$id_skip = (get_option('woo_order_invoice_id_enable')==1) ? 1 : 0;
										$pn_skip = (get_option('woo_order_invoice_product_name_enable')==1) ? 1 : 0;
										$sku_skip = (get_option('woo_order_invoice_sku_enable')==1) ? 1 : 0;
										$pr_skip = (get_option('woo_order_invoice_price_enable')==1) ? 1 : 0;
										$qty_skip = (get_option('woo_order_invoice_quantity_enable')==1) ? 1 : 0;
										$skip = $id_skip + $pn_skip + $sku_skip + $pr_skip + $qty_skip;
										foreach ( $items as $key => $item ) {	
											$count_att = 0;
											$product =get_product($item['product_id']);		
											if (is_object($product)) {
											  $attributes = $product->get_attributes();
											}									 
											
											if (!empty($attributes)) {
												
												foreach ($attributes as $key => $value) {														
													$data.= "<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$key."</Data></Cell>";
													$count_att++;
												}
											}										
										}	
										$data.= $model->woo_order_invoice_send_excel_get_column('woo_order_invoice_total_enable');
										$data .= "</Row>";
										$counter = 1;
										$grand_total =0;						    	
										foreach ( $items as $item ) {
											    $product_name = $item['name'];
											    $product_info = $item['item_meta'];
											    $total=$product_info['_line_total'][0];
										       	$product =get_product($item['product_id']);	
		    									//$sku = $product->get_sku();
		    									$SKU = ( !empty($sku) ) ? $sku : '';
											    $type = $item['type'];
											    if ($type == 'line_item') {
											    	$quantity=$product_info['_qty'][0];
											    	$price = $total / $quantity;
											    	$data .= "<Row>"; 
												    $data.= "<Cell  ss:StyleID='s22'><Data ss:Type='Number'>".$counter."</Data></Cell>";
													$data.= (get_option('woo_order_invoice_id_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$order->id."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_invoice_product_name_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$product_name."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_invoice_sku_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$SKU."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_invoice_price_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>" : '' ;
													$data.= (get_option('woo_order_invoice_quantity_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$quantity."</Data></Cell>" : '' ;
													
													$id_skip = (get_option('woo_order_invoice_id_enable')==1) ? 1 : 0;
													$pn_skip = (get_option('woo_order_invoice_product_name_enable')==1) ? 1 : 0;
													$sku_skip = (get_option('woo_order_invoice_sku_enable')==1) ? 1 : 0;
													$pr_skip = (get_option('woo_order_invoice_price_enable')==1) ? 1 : 0;
													$qty_skip = (get_option('woo_order_invoice_quantity_enable')==1) ? 1 : 0;
													$skip = $id_skip + $pn_skip + $sku_skip + $pr_skip + $qty_skip;		
													
													$counter++;
													$product =get_product($item['product_id']);											 
													//$attributes = $product->get_attributes();
													if (!empty($attributes)) {
														foreach ($attributes as $key => $value) {														
															$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$value['value']."</Data></Cell>";
														}
													}else{
														for ($i=0; $i < $count_att; $i++) { 
															$data.= "<Cell  ss:StyleID='s22'><Data ss:Type='String'></Data></Cell>";
															
														}
													}										
													$data.= (get_option('woo_order_invoice_total_enable') == 1 ) ? "<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$total."</Data></Cell>" : '' ;		
													$data.= "</Row>";
											    }else{
											    	$price = $total;
											    	$merge = $count_att + $skip;
											    	$fee_enable = get_option('woo_order_invoice_fee_enable');
											    	if ($fee_enable == 1) {
											    		$data .= "<Row >"; 													    
														$data.= "<Cell  ss:StyleID='s22' ss:MergeAcross='".$merge."'><Data ss:Type='String'>".$product_name."</Data></Cell>																										
														<Cell  ss:StyleID='s22'><Data ss:Type='String'>".$symbol.$price."</Data></Cell>
														</Row>"; 
											    	}												    	
											    }	 
											    $grand_total =  $grand_total + $total;
											    
											    
												if (end($item) ) {	}
											    
											}
												$merge = $count_att + $skip;
												$order_total_enable = get_option('woo_order_invoice_order_total_enable');
												if ($order_total_enable == 1) {
													$data .= "<Row>";
													$data.= "<Cell  ss:StyleID='s21' ss:MergeAcross='".$merge."'><Data ss:Type='String'>".get_option('woo_order_invoice_order_total')."</Data></Cell>																										
													<Cell  ss:StyleID='s21'><Data ss:Type='String'>".$symbol.$grand_total."</Data></Cell>
													</Row>";
												}
												$data .= "<Row></Row>"; 
																					
										$data .= "</Table></Worksheet>";		
								}//display / export Different Sheet	
					

					$data.= "<Worksheet ss:Name='Custom Order Items' ><Table>";
					$data.= "<Column ss:Index='2' ss:AutoFitWidth='0' ss:Width=\"120\"/>
							 <Column ss:AutoFitWidth=\"0\" ss:Width=\"130\"/>";
					$data.= "<Row>";
					$data.= "<Cell ss:StyleID='s21' ss:MergeAcross='5'><Data ss:Type='String'>Custom Order Items</Data></Cell>	</Row>";
					$data.= "<Row>";
					$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>No</Data></Cell>";
					$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>Order ID</Data></Cell>";
					$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>Description</Data></Cell>";
					$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>Packing</Data></Cell>";
					$data.= "<Cell ss:StyleID='s21'><Data ss:Type='String'>Quantity</Data></Cell>";
					$data.= "</Row>";

					$meta_values = get_post_meta( $post_id );
					if (!empty($meta_values['cvnse-custom-order-item'][0])) {
						$meta = unserialize($meta_values['cvnse-custom-order-item'][0]);
						if (!empty($meta) && count($meta) > 0) {
							$i = 1;
							foreach ($meta as $key => $value) {
								$data.= "<Row>";
								$data.= "<Cell ss:StyleID='s22'><Data ss:Type='String'>".$i."</Data></Cell>";
								$data.= "<Cell ss:StyleID='s22'><Data ss:Type='String'>".$post_id."</Data></Cell>";
								$data.= "<Cell ss:StyleID='s23'><Data ss:Type='String'>".$value['description']."</Data></Cell>";
								$data.= "<Cell ss:StyleID='s22'><Data ss:Type='String'>".$value['packing']."</Data></Cell>";
								$data.= "<Cell ss:StyleID='s22'><Data ss:Type='String'>".$value['quantity']."</Data></Cell>";
								$data.= "</Row>";
								$i++;
							}
						}else{
								$data .= "<Row>"; 
								$data.= "<Cell ss:StyleID='s22' ss:MergeAcross='5'><Data ss:Type='String'>No Custom Order Items</Data></Cell>	</Row>";
							}	
					}
					$data .= "</Table></Worksheet>";	
					$data.="</Workbook>";	

		ob_get_clean();
		file_put_contents(WOO_ORDER_EXCEL.'/'.$post_id.'-order.xls', $data);		
		
		$file = $post_id.'-order.xls';
		return $file;
	}

?>