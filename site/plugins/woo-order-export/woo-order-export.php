<?php
/**
 * Plugin Name: WooCommerce - Order Export
 * Plugin URI: http://indicaneinfotech.com/
 * Description: Allow You to export order in pdf excell and csv  file.
 * Version: 2.1.3 (bug fixed)
 * Author: Apin Chaudhary
 * Author URI: http://indicaneinfotech.com/
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Basic plugin definitions 
 * 
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */

global $wpdb;

if( !defined( 'WOO_ORDER_EXP_URL' ) ) {
	define( 'WOO_ORDER_EXP_URL', plugin_dir_url( __FILE__ ) ); // plugin url
}
if( !defined( 'WOO_ORDER_EXP_DIR' ) ) {
	define( 'WOO_ORDER_EXP_DIR', dirname( __FILE__ ) ); // plugin dir
}
if( !defined( 'WOO_ORDER_EXP_ADMIN' ) ) {
	define( 'WOO_ORDER_EXP_ADMIN', WOO_ORDER_EXP_DIR . '/includes/admin' ); // plugin admin dir
}
if( !defined( 'WOO_ORDER_EXP_SAVE_FILE' ) ) {
	define( 'WOO_ORDER_EXP_SAVE_FILE', WOO_ORDER_EXP_DIR . '/includes/order-excel' ); // plugin admin dir
}
/**
 * Load Text Domain
 *
 * This gets the plugin ready for translation.
 *
 * @package WooCommerce - Order Export
 * @since 1.3.0
 */

function woo_order_exp_load_textdomain() {

  load_plugin_textdomain( 'wooorderexp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

}

add_action( 'init', 'woo_order_exp_load_textdomain' );

	/**
	 * Activation Hook
	 *
	 * Register plugin activation hook.
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	
	register_activation_hook( __FILE__, 'woo_order_exp_install' );
	
	/**
	 * Deactivation Hook
	 *
	 * Register plugin deactivation hook.
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	
	register_deactivation_hook( __FILE__, 'woo_order_exp_uninstall');
	
	
	/**
	 * Plugin Setup (On Activation)
	 *
	 * Does the initial setup,
	 * stest default values for the plugin options.
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	
	function woo_order_exp_install() {		
		global $wpdb,$woo_order_exp_model;		
		
		$woo_order_exp_model->woo_order_exp_excel_default();
	}
	
	
	
	/**
	 * Plugin Setup (On Deactivation)
	 *
	 * Delete  plugin options.
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	
	function woo_order_exp_uninstall() {
		delete_option('woo_order_export_pdf_title');

		delete_option('woo_order_excel_sheet_settings');		
		
		delete_option('woo_order_id_enable');
		delete_option('woo_order_status_title_enable');		
		delete_option('woo_order_date_enable','');
		delete_option('woo_order_modified_date_enable');
		delete_option('woo_order_payment_method_enable');
		delete_option('woo_order_payment_paypal_enable');
		delete_option('woo_order_shipping_charge_enable');
		delete_option('woo_order_cart_discount_enable');
		delete_option('woo_order_order_tax_enable');
		delete_option('woo_order_shipping_tax_enable');
		delete_option('woo_order_sku_enable');
		delete_option('woo_order_product_name_enable');
		delete_option('woo_order_price_enable');
		delete_option('woo_order_quantity_enable');
		delete_option('woo_order_total_enable');
		delete_option('woo_order_fee_enable');
		delete_option('woo_order_order_total_enable');

		//customer default excel/csv
		delete_option('woo_order_user_name_enable');
		delete_option('woo_order_bill_first_name_enable');
		delete_option('woo_order_bill_last_name_enable');
		delete_option('woo_order_bill_company_enable');
		delete_option('woo_order_bill_address_enable');
		delete_option('woo_order_second_bill_address_enable');
		delete_option('woo_order_bill_city_enable');
		delete_option('woo_order_bill_state_enable');
		delete_option('woo_order_bill_country_enable');
		delete_option('woo_order_bill_post_code_enable');
		delete_option('woo_order_bill_phone_enable');
		delete_option('woo_order_bill_email_enable');

		delete_option('woo_order_ship_first_name_enable');
		delete_option('woo_order_ship_last_name_enable');
		delete_option('woo_order_ship_company_enable');
		delete_option('woo_order_ship_address_enable');
		delete_option('woo_order_ship_city_enable');
		delete_option('woo_order_ship_state_enable');
		delete_option('woo_order_ship_country_enable');
		delete_option('woo_order_ship_post_code_enable');
		delete_option('woo_order_ship_method_enable');
		
		//order default excel/csv
		delete_option('woo_order_id');
		delete_option('woo_order_status_title');
		delete_option('woo_order_date');
		delete_option('woo_order_modified_date');
		delete_option('woo_order_payment_method');
		delete_option('woo_order_payment_paypal');
		delete_option('woo_order_shipping_charge');
		delete_option('woo_order_cart_discount');
		delete_option('woo_order_order_tax');
		delete_option('woo_order_shipping_tax');
		delete_option('woo_order_sku');
		delete_option('woo_order_product_name');
		delete_option('woo_order_price');
		delete_option('woo_order_quantity');
		delete_option('woo_order_total');
		delete_option('woo_order_order_total');

		//customer default excel/csv
		delete_option('woo_order_user_name');
		delete_option('woo_order_bill_first_name');
		delete_option('woo_order_bill_last_name');
		delete_option('woo_order_bill_company');
		delete_option('woo_order_bill_address');
		delete_option('woo_order_second_bill_address');
		delete_option('woo_order_bill_city');
		delete_option('woo_order_bill_state');
		delete_option('woo_order_bill_country');
		delete_option('woo_order_bill_post_code');
		delete_option('woo_order_bill_phone');
		delete_option('woo_order_bill_email');

		delete_option('woo_order_ship_first_name');
		delete_option('woo_order_ship_last_name');
		delete_option('woo_order_ship_company');
		delete_option('woo_order_ship_address');
		delete_option('woo_order_ship_city');
		delete_option('woo_order_ship_state');
		delete_option('woo_order_ship_country');
		delete_option('woo_order_ship_post_code');
		delete_option('woo_order_ship_method');	

		//Mail Function
		delete_option('woo_order_exp_attach_invoice');	
		delete_option('woo_order_excel_attached_file_type');	

		//Invoice for buyer
		delete_option('woo_order_exp_buyer_invoice');
		delete_option('woo_order_excel_buyer_invoice_file_type');
		delete_option('woo_order_excel_buyer_invoice_file_title');
	}
		
	/**
	 * Start Session
	 * 
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 */
	
	function woo_order_exp_start_session() {
		
		if( !session_id() ) {
			session_start();
		}
	}
	

	/**
	 *Paypal Mail
	 * 
	 * Save PayPal Mail ID save in order.
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	add_action('woocommerce_checkout_update_order_meta', 'woo_order_exp_paypal_mail_id', 10, 2);

	function woo_order_exp_paypal_mail_id($order_id,$posted ){
		 $woocommerce_paypal_settings = get_option('woocommerce_paypal_settings');
		 $paypal_email = $woocommerce_paypal_settings['email'];
		 
		 if($posted['payment_method'] == "paypal"){
		  update_post_meta( $order_id, '_paypal_email', $paypal_email);
		 }
	}
		

	/**
	 * Includes Files
	 * 
	 * Includes some required files for plugin
	 *
	 * @package WooCommerce - Order Export
	 * @since 1.3.0
	 * 
	 */
	
	global $woo_order_exp_model, $woo_order_exp_scripts,
		$woo_order_exp_render, $woo_order_exp_admin;
	


	//Model Class for generic functions
	require_once( WOO_ORDER_EXP_DIR . '/includes/class-woo-order-exp-model.php' );
	$woo_order_exp_model = new Woo_Order_Exp_Model();
	
	//Scripts Class for scripts / styles
	require_once( WOO_ORDER_EXP_DIR . '/includes/class-woo-order-exp-scripts.php' );
	$woo_order_exp_scripts = new Woo_Order_Exp_Scripts();
	$woo_order_exp_scripts->add_hooks();
	
	//Renderer Class for HTML
	require_once( WOO_ORDER_EXP_DIR . '/includes/class-woo-order-exp-renderer.php' );
	$woo_order_exp_render = new Woo_Order_Exp_Renderer();	
	

	//Admin Pages Class for admin site
	require_once( WOO_ORDER_EXP_ADMIN . '/class-woo-order-exp-admin.php' );
	$woo_order_exp_admin = new Woo_Order_Exp_Admin();
	$woo_order_exp_admin->add_hooks();		
?>